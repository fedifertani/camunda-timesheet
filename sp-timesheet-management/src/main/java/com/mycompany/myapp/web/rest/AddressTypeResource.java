package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.AddressType;

import com.mycompany.myapp.repository.AddressTypeRepository;
import com.mycompany.myapp.repository.search.AddressTypeSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing AddressType.
 */
@RestController
@RequestMapping("/api")
public class AddressTypeResource {

    private final Logger log = LoggerFactory.getLogger(AddressTypeResource.class);

    private static final String ENTITY_NAME = "addressType";

    private final AddressTypeRepository addressTypeRepository;

    private final AddressTypeSearchRepository addressTypeSearchRepository;

    public AddressTypeResource(AddressTypeRepository addressTypeRepository, AddressTypeSearchRepository addressTypeSearchRepository) {
        this.addressTypeRepository = addressTypeRepository;
        this.addressTypeSearchRepository = addressTypeSearchRepository;
    }

    /**
     * POST  /address-types : Create a new addressType.
     *
     * @param addressType the addressType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new addressType, or with status 400 (Bad Request) if the addressType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/address-types")
    @Timed
    public ResponseEntity<AddressType> createAddressType(@RequestBody AddressType addressType) throws URISyntaxException {
        log.debug("REST request to save AddressType : {}", addressType);
        if (addressType.getId() != null) {
            throw new BadRequestAlertException("A new addressType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AddressType result = addressTypeRepository.save(addressType);
        addressTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/address-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /address-types : Updates an existing addressType.
     *
     * @param addressType the addressType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated addressType,
     * or with status 400 (Bad Request) if the addressType is not valid,
     * or with status 500 (Internal Server Error) if the addressType couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/address-types")
    @Timed
    public ResponseEntity<AddressType> updateAddressType(@RequestBody AddressType addressType) throws URISyntaxException {
        log.debug("REST request to update AddressType : {}", addressType);
        if (addressType.getId() == null) {
            return createAddressType(addressType);
        }
        AddressType result = addressTypeRepository.save(addressType);
        addressTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, addressType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /address-types : get all the addressTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of addressTypes in body
     */
    @GetMapping("/address-types")
    @Timed
    public List<AddressType> getAllAddressTypes() {
        log.debug("REST request to get all AddressTypes");
        return addressTypeRepository.findAll();
        }

    /**
     * GET  /address-types/:id : get the "id" addressType.
     *
     * @param id the id of the addressType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the addressType, or with status 404 (Not Found)
     */
    @GetMapping("/address-types/{id}")
    @Timed
    public ResponseEntity<AddressType> getAddressType(@PathVariable String id) {
        log.debug("REST request to get AddressType : {}", id);
        AddressType addressType = addressTypeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(addressType));
    }

    /**
     * DELETE  /address-types/:id : delete the "id" addressType.
     *
     * @param id the id of the addressType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/address-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteAddressType(@PathVariable String id) {
        log.debug("REST request to delete AddressType : {}", id);
        addressTypeRepository.delete(id);
        addressTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/address-types?query=:query : search for the addressType corresponding
     * to the query.
     *
     * @param query the query of the addressType search
     * @return the result of the search
     */
    @GetMapping("/_search/address-types")
    @Timed
    public List<AddressType> searchAddressTypes(@RequestParam String query) {
        log.debug("REST request to search AddressTypes for query {}", query);
        return StreamSupport
            .stream(addressTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
