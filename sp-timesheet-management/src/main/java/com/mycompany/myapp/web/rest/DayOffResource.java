package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.DayOff;

import com.mycompany.myapp.repository.DayOffRepository;
import com.mycompany.myapp.repository.search.DayOffSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing DayOff.
 */
@RestController
@RequestMapping("/api")
public class DayOffResource {

    private final Logger log = LoggerFactory.getLogger(DayOffResource.class);

    private static final String ENTITY_NAME = "dayOff";

    private final DayOffRepository dayOffRepository;

    private final DayOffSearchRepository dayOffSearchRepository;

    public DayOffResource(DayOffRepository dayOffRepository, DayOffSearchRepository dayOffSearchRepository) {
        this.dayOffRepository = dayOffRepository;
        this.dayOffSearchRepository = dayOffSearchRepository;
    }

    /**
     * POST  /day-offs : Create a new dayOff.
     *
     * @param dayOff the dayOff to create
     * @return the ResponseEntity with status 201 (Created) and with body the new dayOff, or with status 400 (Bad Request) if the dayOff has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/day-offs")
    @Timed
    public ResponseEntity<DayOff> createDayOff(@RequestBody DayOff dayOff) throws URISyntaxException {
        log.debug("REST request to save DayOff : {}", dayOff);
        if (dayOff.getId() != null) {
            throw new BadRequestAlertException("A new dayOff cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DayOff result = dayOffRepository.save(dayOff);
        dayOffSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/day-offs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /day-offs : Updates an existing dayOff.
     *
     * @param dayOff the dayOff to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated dayOff,
     * or with status 400 (Bad Request) if the dayOff is not valid,
     * or with status 500 (Internal Server Error) if the dayOff couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/day-offs")
    @Timed
    public ResponseEntity<DayOff> updateDayOff(@RequestBody DayOff dayOff) throws URISyntaxException {
        log.debug("REST request to update DayOff : {}", dayOff);
        if (dayOff.getId() == null) {
            return createDayOff(dayOff);
        }
        DayOff result = dayOffRepository.save(dayOff);
        dayOffSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, dayOff.getId().toString()))
            .body(result);
    }

    /**
     * GET  /day-offs : get all the dayOffs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of dayOffs in body
     */
    @GetMapping("/day-offs")
    @Timed
    public List<DayOff> getAllDayOffs() {
        log.debug("REST request to get all DayOffs");
        return dayOffRepository.findAll();
        }

    /**
     * GET  /day-offs/:id : get the "id" dayOff.
     *
     * @param id the id of the dayOff to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the dayOff, or with status 404 (Not Found)
     */
    @GetMapping("/day-offs/{id}")
    @Timed
    public ResponseEntity<DayOff> getDayOff(@PathVariable String id) {
        log.debug("REST request to get DayOff : {}", id);
        DayOff dayOff = dayOffRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(dayOff));
    }

    /**
     * DELETE  /day-offs/:id : delete the "id" dayOff.
     *
     * @param id the id of the dayOff to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/day-offs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDayOff(@PathVariable String id) {
        log.debug("REST request to delete DayOff : {}", id);
        dayOffRepository.delete(id);
        dayOffSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/day-offs?query=:query : search for the dayOff corresponding
     * to the query.
     *
     * @param query the query of the dayOff search
     * @return the result of the search
     */
    @GetMapping("/_search/day-offs")
    @Timed
    public List<DayOff> searchDayOffs(@RequestParam String query) {
        log.debug("REST request to search DayOffs for query {}", query);
        return StreamSupport
            .stream(dayOffSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
