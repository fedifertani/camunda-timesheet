package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.SickLeaveRequest;

import com.mycompany.myapp.repository.SickLeaveRequestRepository;
import com.mycompany.myapp.repository.search.SickLeaveRequestSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing SickLeaveRequest.
 */
@RestController
@RequestMapping("/api")
public class SickLeaveRequestResource {

    private final Logger log = LoggerFactory.getLogger(SickLeaveRequestResource.class);

    private static final String ENTITY_NAME = "sickLeaveRequest";

    private final SickLeaveRequestRepository sickLeaveRequestRepository;

    private final SickLeaveRequestSearchRepository sickLeaveRequestSearchRepository;

    public SickLeaveRequestResource(SickLeaveRequestRepository sickLeaveRequestRepository, SickLeaveRequestSearchRepository sickLeaveRequestSearchRepository) {
        this.sickLeaveRequestRepository = sickLeaveRequestRepository;
        this.sickLeaveRequestSearchRepository = sickLeaveRequestSearchRepository;
    }

    /**
     * POST  /sick-leave-requests : Create a new sickLeaveRequest.
     *
     * @param sickLeaveRequest the sickLeaveRequest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new sickLeaveRequest, or with status 400 (Bad Request) if the sickLeaveRequest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/sick-leave-requests")
    @Timed
    public ResponseEntity<SickLeaveRequest> createSickLeaveRequest(@RequestBody SickLeaveRequest sickLeaveRequest) throws URISyntaxException {
        log.debug("REST request to save SickLeaveRequest : {}", sickLeaveRequest);
        if (sickLeaveRequest.getId() != null) {
            throw new BadRequestAlertException("A new sickLeaveRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SickLeaveRequest result = sickLeaveRequestRepository.save(sickLeaveRequest);
        sickLeaveRequestSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/sick-leave-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /sick-leave-requests : Updates an existing sickLeaveRequest.
     *
     * @param sickLeaveRequest the sickLeaveRequest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated sickLeaveRequest,
     * or with status 400 (Bad Request) if the sickLeaveRequest is not valid,
     * or with status 500 (Internal Server Error) if the sickLeaveRequest couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/sick-leave-requests")
    @Timed
    public ResponseEntity<SickLeaveRequest> updateSickLeaveRequest(@RequestBody SickLeaveRequest sickLeaveRequest) throws URISyntaxException {
        log.debug("REST request to update SickLeaveRequest : {}", sickLeaveRequest);
        if (sickLeaveRequest.getId() == null) {
            return createSickLeaveRequest(sickLeaveRequest);
        }
        SickLeaveRequest result = sickLeaveRequestRepository.save(sickLeaveRequest);
        sickLeaveRequestSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, sickLeaveRequest.getId().toString()))
            .body(result);
    }

    /**
     * GET  /sick-leave-requests : get all the sickLeaveRequests.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of sickLeaveRequests in body
     */
    @GetMapping("/sick-leave-requests")
    @Timed
    public List<SickLeaveRequest> getAllSickLeaveRequests() {
        log.debug("REST request to get all SickLeaveRequests");
        return sickLeaveRequestRepository.findAll();
        }

    /**
     * GET  /sick-leave-requests/:id : get the "id" sickLeaveRequest.
     *
     * @param id the id of the sickLeaveRequest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the sickLeaveRequest, or with status 404 (Not Found)
     */
    @GetMapping("/sick-leave-requests/{id}")
    @Timed
    public ResponseEntity<SickLeaveRequest> getSickLeaveRequest(@PathVariable String id) {
        log.debug("REST request to get SickLeaveRequest : {}", id);
        SickLeaveRequest sickLeaveRequest = sickLeaveRequestRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(sickLeaveRequest));
    }

    /**
     * DELETE  /sick-leave-requests/:id : delete the "id" sickLeaveRequest.
     *
     * @param id the id of the sickLeaveRequest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/sick-leave-requests/{id}")
    @Timed
    public ResponseEntity<Void> deleteSickLeaveRequest(@PathVariable String id) {
        log.debug("REST request to delete SickLeaveRequest : {}", id);
        sickLeaveRequestRepository.delete(id);
        sickLeaveRequestSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/sick-leave-requests?query=:query : search for the sickLeaveRequest corresponding
     * to the query.
     *
     * @param query the query of the sickLeaveRequest search
     * @return the result of the search
     */
    @GetMapping("/_search/sick-leave-requests")
    @Timed
    public List<SickLeaveRequest> searchSickLeaveRequests(@RequestParam String query) {
        log.debug("REST request to search SickLeaveRequests for query {}", query);
        return StreamSupport
            .stream(sickLeaveRequestSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
