package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.Mission;

import com.mycompany.myapp.repository.MissionRepository;
import com.mycompany.myapp.repository.search.MissionSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Mission.
 */
@RestController
@RequestMapping("/api")
public class MissionResource {

    private final Logger log = LoggerFactory.getLogger(MissionResource.class);

    private static final String ENTITY_NAME = "mission";

    private final MissionRepository missionRepository;

    private final MissionSearchRepository missionSearchRepository;

    public MissionResource(MissionRepository missionRepository, MissionSearchRepository missionSearchRepository) {
        this.missionRepository = missionRepository;
        this.missionSearchRepository = missionSearchRepository;
    }

    /**
     * POST  /missions : Create a new mission.
     *
     * @param mission the mission to create
     * @return the ResponseEntity with status 201 (Created) and with body the new mission, or with status 400 (Bad Request) if the mission has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/missions")
    @Timed
    public ResponseEntity<Mission> createMission(@RequestBody Mission mission) throws URISyntaxException {
        log.debug("REST request to save Mission : {}", mission);
        if (mission.getId() != null) {
            throw new BadRequestAlertException("A new mission cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Mission result = missionRepository.save(mission);
        missionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/missions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /missions : Updates an existing mission.
     *
     * @param mission the mission to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated mission,
     * or with status 400 (Bad Request) if the mission is not valid,
     * or with status 500 (Internal Server Error) if the mission couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/missions")
    @Timed
    public ResponseEntity<Mission> updateMission(@RequestBody Mission mission) throws URISyntaxException {
        log.debug("REST request to update Mission : {}", mission);
        if (mission.getId() == null) {
            return createMission(mission);
        }
        Mission result = missionRepository.save(mission);
        missionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, mission.getId().toString()))
            .body(result);
    }

    /**
     * GET  /missions : get all the missions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of missions in body
     */
    @GetMapping("/missions")
    @Timed
    public List<Mission> getAllMissions() {
        log.debug("REST request to get all Missions");
        return missionRepository.findAll();
        }

    /**
     * GET  /missions/:id : get the "id" mission.
     *
     * @param id the id of the mission to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the mission, or with status 404 (Not Found)
     */
    @GetMapping("/missions/{id}")
    @Timed
    public ResponseEntity<Mission> getMission(@PathVariable String id) {
        log.debug("REST request to get Mission : {}", id);
        Mission mission = missionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(mission));
    }

    /**
     * DELETE  /missions/:id : delete the "id" mission.
     *
     * @param id the id of the mission to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/missions/{id}")
    @Timed
    public ResponseEntity<Void> deleteMission(@PathVariable String id) {
        log.debug("REST request to delete Mission : {}", id);
        missionRepository.delete(id);
        missionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/missions?query=:query : search for the mission corresponding
     * to the query.
     *
     * @param query the query of the mission search
     * @return the result of the search
     */
    @GetMapping("/_search/missions")
    @Timed
    public List<Mission> searchMissions(@RequestParam String query) {
        log.debug("REST request to search Missions for query {}", query);
        return StreamSupport
            .stream(missionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
