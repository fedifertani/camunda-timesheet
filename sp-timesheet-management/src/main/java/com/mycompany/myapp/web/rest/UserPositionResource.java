package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.UserPosition;

import com.mycompany.myapp.repository.UserPositionRepository;
import com.mycompany.myapp.repository.search.UserPositionSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing UserPosition.
 */
@RestController
@RequestMapping("/api")
public class UserPositionResource {

    private final Logger log = LoggerFactory.getLogger(UserPositionResource.class);

    private static final String ENTITY_NAME = "userPosition";

    private final UserPositionRepository userPositionRepository;

    private final UserPositionSearchRepository userPositionSearchRepository;

    public UserPositionResource(UserPositionRepository userPositionRepository, UserPositionSearchRepository userPositionSearchRepository) {
        this.userPositionRepository = userPositionRepository;
        this.userPositionSearchRepository = userPositionSearchRepository;
    }

    /**
     * POST  /user-positions : Create a new userPosition.
     *
     * @param userPosition the userPosition to create
     * @return the ResponseEntity with status 201 (Created) and with body the new userPosition, or with status 400 (Bad Request) if the userPosition has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/user-positions")
    @Timed
    public ResponseEntity<UserPosition> createUserPosition(@RequestBody UserPosition userPosition) throws URISyntaxException {
        log.debug("REST request to save UserPosition : {}", userPosition);
        if (userPosition.getId() != null) {
            throw new BadRequestAlertException("A new userPosition cannot already have an ID", ENTITY_NAME, "idexists");
        }
        UserPosition result = userPositionRepository.save(userPosition);
        userPositionSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/user-positions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /user-positions : Updates an existing userPosition.
     *
     * @param userPosition the userPosition to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated userPosition,
     * or with status 400 (Bad Request) if the userPosition is not valid,
     * or with status 500 (Internal Server Error) if the userPosition couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/user-positions")
    @Timed
    public ResponseEntity<UserPosition> updateUserPosition(@RequestBody UserPosition userPosition) throws URISyntaxException {
        log.debug("REST request to update UserPosition : {}", userPosition);
        if (userPosition.getId() == null) {
            return createUserPosition(userPosition);
        }
        UserPosition result = userPositionRepository.save(userPosition);
        userPositionSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, userPosition.getId().toString()))
            .body(result);
    }

    /**
     * GET  /user-positions : get all the userPositions.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of userPositions in body
     */
    @GetMapping("/user-positions")
    @Timed
    public List<UserPosition> getAllUserPositions() {
        log.debug("REST request to get all UserPositions");
        return userPositionRepository.findAll();
        }

    /**
     * GET  /user-positions/:id : get the "id" userPosition.
     *
     * @param id the id of the userPosition to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the userPosition, or with status 404 (Not Found)
     */
    @GetMapping("/user-positions/{id}")
    @Timed
    public ResponseEntity<UserPosition> getUserPosition(@PathVariable String id) {
        log.debug("REST request to get UserPosition : {}", id);
        UserPosition userPosition = userPositionRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(userPosition));
    }

    @GetMapping("/user-positions/name/{name}")
    @Timed
    public UserPosition getUserPositionByName(@PathVariable String name) {
        return userPositionRepository.findByName(name);
       
    }
    /**
     * DELETE  /user-positions/:id : delete the "id" userPosition.
     *
     * @param id the id of the userPosition to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/user-positions/{id}")
    @Timed
    public ResponseEntity<Void> deleteUserPosition(@PathVariable String id) {
        log.debug("REST request to delete UserPosition : {}", id);
        userPositionRepository.delete(id);
        userPositionSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/user-positions?query=:query : search for the userPosition corresponding
     * to the query.
     *
     * @param query the query of the userPosition search
     * @return the result of the search
     */
    @GetMapping("/_search/user-positions")
    @Timed
    public List<UserPosition> searchUserPositions(@RequestParam String query) {
        log.debug("REST request to search UserPositions for query {}", query);
        return StreamSupport
            .stream(userPositionSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
