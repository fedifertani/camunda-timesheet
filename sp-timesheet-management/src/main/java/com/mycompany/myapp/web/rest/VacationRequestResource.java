package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.VacationRequest;
import com.mycompany.myapp.domain.VacationToUpdate;
import com.mycompany.myapp.repository.VacationRequestRepository;
import com.mycompany.myapp.repository.search.VacationRequestSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VacationRequest.
 */
@RestController
@RequestMapping("/api")
public class VacationRequestResource {

    private final Logger log = LoggerFactory.getLogger(VacationRequestResource.class);

    private static final String ENTITY_NAME = "vacationRequest";

    private final VacationRequestRepository vacationRequestRepository;

    private final VacationRequestSearchRepository vacationRequestSearchRepository;

    public VacationRequestResource(VacationRequestRepository vacationRequestRepository, VacationRequestSearchRepository vacationRequestSearchRepository) {
        this.vacationRequestRepository = vacationRequestRepository;
        this.vacationRequestSearchRepository = vacationRequestSearchRepository;
    }

    /**
     * POST  /vacation-requests : Create a new vacationRequest.
     *
     * @param vacationRequest the vacationRequest to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vacationRequest, or with status 400 (Bad Request) if the vacationRequest has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vacation-requests")
    @Timed
    public ResponseEntity<VacationRequest> createVacationRequest(@RequestBody VacationRequest vacationRequest) throws URISyntaxException {
        log.debug("REST request to save VacationRequest : {}", vacationRequest);
        if (vacationRequest.getId() != null) {
            throw new BadRequestAlertException("A new vacationRequest cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VacationRequest result = vacationRequestRepository.save(vacationRequest);
        vacationRequestSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/vacation-requests/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vacation-requests : Updates an existing vacationRequest.
     *
     * @param vacationRequest the vacationRequest to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vacationRequest,
     * or with status 400 (Bad Request) if the vacationRequest is not valid,
     * or with status 500 (Internal Server Error) if the vacationRequest couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vacation-requests")
    @Timed
    public ResponseEntity<VacationRequest> updateVacationRequest(@RequestBody VacationRequest vacationRequest) throws URISyntaxException {
        log.debug("REST request to update VacationRequest : {}", vacationRequest);
        if (vacationRequest.getId() == null) {
            return createVacationRequest(vacationRequest);
        }
        VacationRequest result = vacationRequestRepository.save(vacationRequest);
        vacationRequestSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vacationRequest.getId().toString()))
            .body(result);
    }
    @PutMapping("/vacation-request/{code}/status")
    @Timed
    public VacationRequest updateStatusByCode(@RequestBody VacationToUpdate vacationUpdate,@PathVariable("code") String code) {
        log.debug("REST request to get all Vacations");
        VacationRequest vacation =vacationRequestRepository.findByCode(code);
        vacation.setStatus(vacationUpdate.getStatus());
        
        return vacationRequestRepository.save(vacation);
    }
    /**
     * GET  /vacation-requests : get all the vacationRequests.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of vacationRequests in body
     */
    @GetMapping("/vacation-requests")
    @Timed
    public List<VacationRequest> getAllVacationRequests() {
        log.debug("REST request to get all VacationRequests");
        return vacationRequestRepository.findAll();
        }

    /**
     * GET  /vacation-requests/:id : get the "id" vacationRequest.
     *
     * @param id the id of the vacationRequest to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vacationRequest, or with status 404 (Not Found)
     */
    @GetMapping("/vacation-requests/{id}")
    @Timed
    public ResponseEntity<VacationRequest> getVacationRequest(@PathVariable String id) {
        log.debug("REST request to get VacationRequest : {}", id);
        VacationRequest vacationRequest = vacationRequestRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vacationRequest));
    }

    /**
     * DELETE  /vacation-requests/:id : delete the "id" vacationRequest.
     *
     * @param id the id of the vacationRequest to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vacation-requests/{id}")
    @Timed
    public ResponseEntity<Void> deleteVacationRequest(@PathVariable String id) {
        log.debug("REST request to delete VacationRequest : {}", id);
        vacationRequestRepository.delete(id);
        vacationRequestSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/vacation-requests?query=:query : search for the vacationRequest corresponding
     * to the query.
     *
     * @param query the query of the vacationRequest search
     * @return the result of the search
     */
    @GetMapping("/_search/vacation-requests")
    @Timed
    public List<VacationRequest> searchVacationRequests(@RequestParam String query) {
        log.debug("REST request to search VacationRequests for query {}", query);
        return StreamSupport
            .stream(vacationRequestSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }
    @GetMapping("/vacation-requests/code/{code}")
    @Timed
    public VacationRequest  getByCode(@PathVariable("code") String code) {
    return vacationRequestRepository.findByCode(code);
    }   
}
