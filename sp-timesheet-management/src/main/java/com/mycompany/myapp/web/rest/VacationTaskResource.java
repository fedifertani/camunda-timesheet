package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.VacationTask;

import com.mycompany.myapp.repository.VacationTaskRepository;
import com.mycompany.myapp.repository.search.VacationTaskSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import com.mycompany.myapp.web.rest.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VacationTask.
 */
@RestController
@RequestMapping("/api")
public class VacationTaskResource {

    private final Logger log = LoggerFactory.getLogger(VacationTaskResource.class);

    private static final String ENTITY_NAME = "vacationTask";

    private final VacationTaskRepository vacationTaskRepository;

    private final VacationTaskSearchRepository vacationTaskSearchRepository;

    public VacationTaskResource(VacationTaskRepository vacationTaskRepository, VacationTaskSearchRepository vacationTaskSearchRepository) {
        this.vacationTaskRepository = vacationTaskRepository;
        this.vacationTaskSearchRepository = vacationTaskSearchRepository;
    }

    /**
     * POST  /vacation-tasks : Create a new vacationTask.
     *
     * @param vacationTask the vacationTask to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vacationTask, or with status 400 (Bad Request) if the vacationTask has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vacation-tasks")
    @Timed
    public ResponseEntity<VacationTask> createVacationTask(@RequestBody VacationTask vacationTask) throws URISyntaxException {
        log.debug("REST request to save VacationTask : {}", vacationTask);
        if (vacationTask.getId() != null) {
            throw new BadRequestAlertException("A new vacationTask cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VacationTask result = vacationTaskRepository.save(vacationTask);
        vacationTaskSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/vacation-tasks/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vacation-tasks : Updates an existing vacationTask.
     *
     * @param vacationTask the vacationTask to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vacationTask,
     * or with status 400 (Bad Request) if the vacationTask is not valid,
     * or with status 500 (Internal Server Error) if the vacationTask couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vacation-tasks")
    @Timed
    public ResponseEntity<VacationTask> updateVacationTask(@RequestBody VacationTask vacationTask) throws URISyntaxException {
        log.debug("REST request to update VacationTask : {}", vacationTask);
        if (vacationTask.getId() == null) {
            return createVacationTask(vacationTask);
        }
        VacationTask result = vacationTaskRepository.save(vacationTask);
        vacationTaskSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vacationTask.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vacation-tasks : get all the vacationTasks.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of vacationTasks in body
     */
    @GetMapping("/vacation-tasks")
    @Timed
    public ResponseEntity<List<VacationTask>> getAllVacationTasks(Pageable pageable) {
        log.debug("REST request to get a page of VacationTasks");
        Page<VacationTask> page = vacationTaskRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/vacation-tasks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /vacation-tasks/:id : get the "id" vacationTask.
     *
     * @param id the id of the vacationTask to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vacationTask, or with status 404 (Not Found)
     */
    @GetMapping("/vacation-tasks/{id}")
    @Timed
    public ResponseEntity<VacationTask> getVacationTask(@PathVariable String id) {
        log.debug("REST request to get VacationTask : {}", id);
        VacationTask vacationTask = vacationTaskRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vacationTask));
    }

    /**
     * DELETE  /vacation-tasks/:id : delete the "id" vacationTask.
     *
     * @param id the id of the vacationTask to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vacation-tasks/{id}")
    @Timed
    public ResponseEntity<Void> deleteVacationTask(@PathVariable String id) {
        log.debug("REST request to delete VacationTask : {}", id);
        vacationTaskRepository.delete(id);
        vacationTaskSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/vacation-tasks?query=:query : search for the vacationTask corresponding
     * to the query.
     *
     * @param query the query of the vacationTask search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/vacation-tasks")
    @Timed
    public ResponseEntity<List<VacationTask>> searchVacationTasks(@RequestParam String query, Pageable pageable) {
        log.debug("REST request to search for a page of VacationTasks for query {}", query);
        Page<VacationTask> page = vacationTaskSearchRepository.search(queryStringQuery(query), pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/vacation-tasks");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
