package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.VacationType;

import com.mycompany.myapp.repository.VacationTypeRepository;
import com.mycompany.myapp.repository.search.VacationTypeSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing VacationType.
 */
@RestController
@RequestMapping("/api")
public class VacationTypeResource {

    private final Logger log = LoggerFactory.getLogger(VacationTypeResource.class);

    private static final String ENTITY_NAME = "vacationType";

    private final VacationTypeRepository vacationTypeRepository;

    private final VacationTypeSearchRepository vacationTypeSearchRepository;

    public VacationTypeResource(VacationTypeRepository vacationTypeRepository, VacationTypeSearchRepository vacationTypeSearchRepository) {
        this.vacationTypeRepository = vacationTypeRepository;
        this.vacationTypeSearchRepository = vacationTypeSearchRepository;
    }

    /**
     * POST  /vacation-types : Create a new vacationType.
     *
     * @param vacationType the vacationType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new vacationType, or with status 400 (Bad Request) if the vacationType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/vacation-types")
    @Timed
    public ResponseEntity<VacationType> createVacationType(@RequestBody VacationType vacationType) throws URISyntaxException {
        log.debug("REST request to save VacationType : {}", vacationType);
        if (vacationType.getId() != null) {
            throw new BadRequestAlertException("A new vacationType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        VacationType result = vacationTypeRepository.save(vacationType);
        vacationTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/vacation-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /vacation-types : Updates an existing vacationType.
     *
     * @param vacationType the vacationType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated vacationType,
     * or with status 400 (Bad Request) if the vacationType is not valid,
     * or with status 500 (Internal Server Error) if the vacationType couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/vacation-types")
    @Timed
    public ResponseEntity<VacationType> updateVacationType(@RequestBody VacationType vacationType) throws URISyntaxException {
        log.debug("REST request to update VacationType : {}", vacationType);
        if (vacationType.getId() == null) {
            return createVacationType(vacationType);
        }
        VacationType result = vacationTypeRepository.save(vacationType);
        vacationTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, vacationType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /vacation-types : get all the vacationTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of vacationTypes in body
     */
    @GetMapping("/vacation-types")
    @Timed
    public List<VacationType> getAllVacationTypes() {
        log.debug("REST request to get all VacationTypes");
        return vacationTypeRepository.findAll();
        }

    /**
     * GET  /vacation-types/:id : get the "id" vacationType.
     *
     * @param id the id of the vacationType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the vacationType, or with status 404 (Not Found)
     */
    @GetMapping("/vacation-types/{id}")
    @Timed
    public ResponseEntity<VacationType> getVacationType(@PathVariable String id) {
        log.debug("REST request to get VacationType : {}", id);
        VacationType vacationType = vacationTypeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(vacationType));
    }

    /**
     * DELETE  /vacation-types/:id : delete the "id" vacationType.
     *
     * @param id the id of the vacationType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/vacation-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteVacationType(@PathVariable String id) {
        log.debug("REST request to delete VacationType : {}", id);
        vacationTypeRepository.delete(id);
        vacationTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/vacation-types?query=:query : search for the vacationType corresponding
     * to the query.
     *
     * @param query the query of the vacationType search
     * @return the result of the search
     */
    @GetMapping("/_search/vacation-types")
    @Timed
    public List<VacationType> searchVacationTypes(@RequestParam String query) {
        log.debug("REST request to search VacationTypes for query {}", query);
        return StreamSupport
            .stream(vacationTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
