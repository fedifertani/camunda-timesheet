package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.CustomerContact;

import com.mycompany.myapp.repository.CustomerContactRepository;
import com.mycompany.myapp.repository.search.CustomerContactSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CustomerContact.
 */
@RestController
@RequestMapping("/api")
public class CustomerContactResource {

    private final Logger log = LoggerFactory.getLogger(CustomerContactResource.class);

    private static final String ENTITY_NAME = "customerContact";

    private final CustomerContactRepository customerContactRepository;

    private final CustomerContactSearchRepository customerContactSearchRepository;

    public CustomerContactResource(CustomerContactRepository customerContactRepository, CustomerContactSearchRepository customerContactSearchRepository) {
        this.customerContactRepository = customerContactRepository;
        this.customerContactSearchRepository = customerContactSearchRepository;
    }

    /**
     * POST  /customer-contacts : Create a new customerContact.
     *
     * @param customerContact the customerContact to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerContact, or with status 400 (Bad Request) if the customerContact has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-contacts")
    @Timed
    public ResponseEntity<CustomerContact> createCustomerContact(@RequestBody CustomerContact customerContact) throws URISyntaxException {
        log.debug("REST request to save CustomerContact : {}", customerContact);
        if (customerContact.getId() != null) {
            throw new BadRequestAlertException("A new customerContact cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerContact result = customerContactRepository.save(customerContact);
        customerContactSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/customer-contacts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /customer-contacts : Updates an existing customerContact.
     *
     * @param customerContact the customerContact to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerContact,
     * or with status 400 (Bad Request) if the customerContact is not valid,
     * or with status 500 (Internal Server Error) if the customerContact couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customer-contacts")
    @Timed
    public ResponseEntity<CustomerContact> updateCustomerContact(@RequestBody CustomerContact customerContact) throws URISyntaxException {
        log.debug("REST request to update CustomerContact : {}", customerContact);
        if (customerContact.getId() == null) {
            return createCustomerContact(customerContact);
        }
        CustomerContact result = customerContactRepository.save(customerContact);
        customerContactSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerContact.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customer-contacts : get all the customerContacts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of customerContacts in body
     */
    @GetMapping("/customer-contacts")
    @Timed
    public List<CustomerContact> getAllCustomerContacts() {
        log.debug("REST request to get all CustomerContacts");
        return customerContactRepository.findAll();
        }

    /**
     * GET  /customer-contacts/:id : get the "id" customerContact.
     *
     * @param id the id of the customerContact to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerContact, or with status 404 (Not Found)
     */
    @GetMapping("/customer-contacts/{id}")
    @Timed
    public ResponseEntity<CustomerContact> getCustomerContact(@PathVariable String id) {
        log.debug("REST request to get CustomerContact : {}", id);
        CustomerContact customerContact = customerContactRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customerContact));
    }

    /**
     * DELETE  /customer-contacts/:id : delete the "id" customerContact.
     *
     * @param id the id of the customerContact to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customer-contacts/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomerContact(@PathVariable String id) {
        log.debug("REST request to delete CustomerContact : {}", id);
        customerContactRepository.delete(id);
        customerContactSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/customer-contacts?query=:query : search for the customerContact corresponding
     * to the query.
     *
     * @param query the query of the customerContact search
     * @return the result of the search
     */
    @GetMapping("/_search/customer-contacts")
    @Timed
    public List<CustomerContact> searchCustomerContacts(@RequestParam String query) {
        log.debug("REST request to search CustomerContacts for query {}", query);
        return StreamSupport
            .stream(customerContactSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
