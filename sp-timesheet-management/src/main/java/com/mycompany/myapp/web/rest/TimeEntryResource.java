package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.TimeEntry;

import com.mycompany.myapp.repository.TimeEntryRepository;
import com.mycompany.myapp.repository.search.TimeEntrySearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing TimeEntry.
 */
@RestController
@RequestMapping("/api")
public class TimeEntryResource {

    private final Logger log = LoggerFactory.getLogger(TimeEntryResource.class);

    private static final String ENTITY_NAME = "timeEntry";

    private final TimeEntryRepository timeEntryRepository;

    private final TimeEntrySearchRepository timeEntrySearchRepository;

    public TimeEntryResource(TimeEntryRepository timeEntryRepository, TimeEntrySearchRepository timeEntrySearchRepository) {
        this.timeEntryRepository = timeEntryRepository;
        this.timeEntrySearchRepository = timeEntrySearchRepository;
    }

    /**
     * POST  /time-entries : Create a new timeEntry.
     *
     * @param timeEntry the timeEntry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new timeEntry, or with status 400 (Bad Request) if the timeEntry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/time-entries")
    @Timed
    public ResponseEntity<TimeEntry> createTimeEntry(@RequestBody TimeEntry timeEntry) throws URISyntaxException {
        log.debug("REST request to save TimeEntry : {}", timeEntry);
        if (timeEntry.getId() != null) {
            throw new BadRequestAlertException("A new timeEntry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        TimeEntry result = timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/time-entries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /time-entries : Updates an existing timeEntry.
     *
     * @param timeEntry the timeEntry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated timeEntry,
     * or with status 400 (Bad Request) if the timeEntry is not valid,
     * or with status 500 (Internal Server Error) if the timeEntry couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/time-entries")
    @Timed
    public ResponseEntity<TimeEntry> updateTimeEntry(@RequestBody TimeEntry timeEntry) throws URISyntaxException {
        log.debug("REST request to update TimeEntry : {}", timeEntry);
        if (timeEntry.getId() == null) {
            return createTimeEntry(timeEntry);
        }
        TimeEntry result = timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, timeEntry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /time-entries : get all the timeEntries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of timeEntries in body
     */
    @GetMapping("/time-entries")
    @Timed
    public List<TimeEntry> getAllTimeEntries() {
        log.debug("REST request to get all TimeEntries");
        return timeEntryRepository.findAll();
        }

    /**
     * GET  /time-entries/:id : get the "id" timeEntry.
     *
     * @param id the id of the timeEntry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the timeEntry, or with status 404 (Not Found)
     */
    @GetMapping("/time-entries/{id}")
    @Timed
    public ResponseEntity<TimeEntry> getTimeEntry(@PathVariable String id) {
        log.debug("REST request to get TimeEntry : {}", id);
        TimeEntry timeEntry = timeEntryRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(timeEntry));
    }

    /**
     * DELETE  /time-entries/:id : delete the "id" timeEntry.
     *
     * @param id the id of the timeEntry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/time-entries/{id}")
    @Timed
    public ResponseEntity<Void> deleteTimeEntry(@PathVariable String id) {
        log.debug("REST request to delete TimeEntry : {}", id);
        timeEntryRepository.delete(id);
        timeEntrySearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/time-entries?query=:query : search for the timeEntry corresponding
     * to the query.
     *
     * @param query the query of the timeEntry search
     * @return the result of the search
     */
    @GetMapping("/_search/time-entries")
    @Timed
    public List<TimeEntry> searchTimeEntries(@RequestParam String query) {
        log.debug("REST request to search TimeEntries for query {}", query);
        return StreamSupport
            .stream(timeEntrySearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
