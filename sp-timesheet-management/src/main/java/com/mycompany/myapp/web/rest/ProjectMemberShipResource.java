package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.ProjectMemberShip;

import com.mycompany.myapp.repository.ProjectMemberShipRepository;
import com.mycompany.myapp.repository.search.ProjectMemberShipSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing ProjectMemberShip.
 */
@RestController
@RequestMapping("/api")
public class ProjectMemberShipResource {

    private final Logger log = LoggerFactory.getLogger(ProjectMemberShipResource.class);

    private static final String ENTITY_NAME = "projectMemberShip";

    private final ProjectMemberShipRepository projectMemberShipRepository;

    private final ProjectMemberShipSearchRepository projectMemberShipSearchRepository;

    public ProjectMemberShipResource(ProjectMemberShipRepository projectMemberShipRepository, ProjectMemberShipSearchRepository projectMemberShipSearchRepository) {
        this.projectMemberShipRepository = projectMemberShipRepository;
        this.projectMemberShipSearchRepository = projectMemberShipSearchRepository;
    }

    /**
     * POST  /project-member-ships : Create a new projectMemberShip.
     *
     * @param projectMemberShip the projectMemberShip to create
     * @return the ResponseEntity with status 201 (Created) and with body the new projectMemberShip, or with status 400 (Bad Request) if the projectMemberShip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/project-member-ships")
    @Timed
    public ResponseEntity<ProjectMemberShip> createProjectMemberShip(@RequestBody ProjectMemberShip projectMemberShip) throws URISyntaxException {
        log.debug("REST request to save ProjectMemberShip : {}", projectMemberShip);
        if (projectMemberShip.getId() != null) {
            throw new BadRequestAlertException("A new projectMemberShip cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ProjectMemberShip result = projectMemberShipRepository.save(projectMemberShip);
        projectMemberShipSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/project-member-ships/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /project-member-ships : Updates an existing projectMemberShip.
     *
     * @param projectMemberShip the projectMemberShip to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated projectMemberShip,
     * or with status 400 (Bad Request) if the projectMemberShip is not valid,
     * or with status 500 (Internal Server Error) if the projectMemberShip couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/project-member-ships")
    @Timed
    public ResponseEntity<ProjectMemberShip> updateProjectMemberShip(@RequestBody ProjectMemberShip projectMemberShip) throws URISyntaxException {
        log.debug("REST request to update ProjectMemberShip : {}", projectMemberShip);
        if (projectMemberShip.getId() == null) {
            return createProjectMemberShip(projectMemberShip);
        }
        ProjectMemberShip result = projectMemberShipRepository.save(projectMemberShip);
        projectMemberShipSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, projectMemberShip.getId().toString()))
            .body(result);
    }

    /**
     * GET  /project-member-ships : get all the projectMemberShips.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of projectMemberShips in body
     */
    @GetMapping("/project-member-ships")
    @Timed
    public List<ProjectMemberShip> getAllProjectMemberShips() {
        log.debug("REST request to get all ProjectMemberShips");
        return projectMemberShipRepository.findAll();
        }

    /**
     * GET  /project-member-ships/:id : get the "id" projectMemberShip.
     *
     * @param id the id of the projectMemberShip to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the projectMemberShip, or with status 404 (Not Found)
     */
    @GetMapping("/project-member-ships/{id}")
    @Timed
    public ResponseEntity<ProjectMemberShip> getProjectMemberShip(@PathVariable String id) {
        log.debug("REST request to get ProjectMemberShip : {}", id);
        ProjectMemberShip projectMemberShip = projectMemberShipRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(projectMemberShip));
    }

    /**
     * DELETE  /project-member-ships/:id : delete the "id" projectMemberShip.
     *
     * @param id the id of the projectMemberShip to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/project-member-ships/{id}")
    @Timed
    public ResponseEntity<Void> deleteProjectMemberShip(@PathVariable String id) {
        log.debug("REST request to delete ProjectMemberShip : {}", id);
        projectMemberShipRepository.delete(id);
        projectMemberShipSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/project-member-ships?query=:query : search for the projectMemberShip corresponding
     * to the query.
     *
     * @param query the query of the projectMemberShip search
     * @return the result of the search
     */
    @GetMapping("/_search/project-member-ships")
    @Timed
    public List<ProjectMemberShip> searchProjectMemberShips(@RequestParam String query) {
        log.debug("REST request to search ProjectMemberShips for query {}", query);
        return StreamSupport
            .stream(projectMemberShipSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
