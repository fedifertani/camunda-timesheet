package com.mycompany.myapp.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.mycompany.myapp.domain.CustomerContactType;

import com.mycompany.myapp.repository.CustomerContactTypeRepository;
import com.mycompany.myapp.repository.search.CustomerContactTypeSearchRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import com.mycompany.myapp.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing CustomerContactType.
 */
@RestController
@RequestMapping("/api")
public class CustomerContactTypeResource {

    private final Logger log = LoggerFactory.getLogger(CustomerContactTypeResource.class);

    private static final String ENTITY_NAME = "customerContactType";

    private final CustomerContactTypeRepository customerContactTypeRepository;

    private final CustomerContactTypeSearchRepository customerContactTypeSearchRepository;

    public CustomerContactTypeResource(CustomerContactTypeRepository customerContactTypeRepository, CustomerContactTypeSearchRepository customerContactTypeSearchRepository) {
        this.customerContactTypeRepository = customerContactTypeRepository;
        this.customerContactTypeSearchRepository = customerContactTypeSearchRepository;
    }

    /**
     * POST  /customer-contact-types : Create a new customerContactType.
     *
     * @param customerContactType the customerContactType to create
     * @return the ResponseEntity with status 201 (Created) and with body the new customerContactType, or with status 400 (Bad Request) if the customerContactType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/customer-contact-types")
    @Timed
    public ResponseEntity<CustomerContactType> createCustomerContactType(@RequestBody CustomerContactType customerContactType) throws URISyntaxException {
        log.debug("REST request to save CustomerContactType : {}", customerContactType);
        if (customerContactType.getId() != null) {
            throw new BadRequestAlertException("A new customerContactType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CustomerContactType result = customerContactTypeRepository.save(customerContactType);
        customerContactTypeSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/customer-contact-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /customer-contact-types : Updates an existing customerContactType.
     *
     * @param customerContactType the customerContactType to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated customerContactType,
     * or with status 400 (Bad Request) if the customerContactType is not valid,
     * or with status 500 (Internal Server Error) if the customerContactType couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/customer-contact-types")
    @Timed
    public ResponseEntity<CustomerContactType> updateCustomerContactType(@RequestBody CustomerContactType customerContactType) throws URISyntaxException {
        log.debug("REST request to update CustomerContactType : {}", customerContactType);
        if (customerContactType.getId() == null) {
            return createCustomerContactType(customerContactType);
        }
        CustomerContactType result = customerContactTypeRepository.save(customerContactType);
        customerContactTypeSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, customerContactType.getId().toString()))
            .body(result);
    }

    /**
     * GET  /customer-contact-types : get all the customerContactTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of customerContactTypes in body
     */
    @GetMapping("/customer-contact-types")
    @Timed
    public List<CustomerContactType> getAllCustomerContactTypes() {
        log.debug("REST request to get all CustomerContactTypes");
        return customerContactTypeRepository.findAll();
        }

    /**
     * GET  /customer-contact-types/:id : get the "id" customerContactType.
     *
     * @param id the id of the customerContactType to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the customerContactType, or with status 404 (Not Found)
     */
    @GetMapping("/customer-contact-types/{id}")
    @Timed
    public ResponseEntity<CustomerContactType> getCustomerContactType(@PathVariable String id) {
        log.debug("REST request to get CustomerContactType : {}", id);
        CustomerContactType customerContactType = customerContactTypeRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(customerContactType));
    }

    /**
     * DELETE  /customer-contact-types/:id : delete the "id" customerContactType.
     *
     * @param id the id of the customerContactType to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/customer-contact-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCustomerContactType(@PathVariable String id) {
        log.debug("REST request to delete CustomerContactType : {}", id);
        customerContactTypeRepository.delete(id);
        customerContactTypeSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }

    /**
     * SEARCH  /_search/customer-contact-types?query=:query : search for the customerContactType corresponding
     * to the query.
     *
     * @param query the query of the customerContactType search
     * @return the result of the search
     */
    @GetMapping("/_search/customer-contact-types")
    @Timed
    public List<CustomerContactType> searchCustomerContactTypes(@RequestParam String query) {
        log.debug("REST request to search CustomerContactTypes for query {}", query);
        return StreamSupport
            .stream(customerContactTypeSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
