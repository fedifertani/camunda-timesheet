package com.mycompany.myapp.domain;


import java.io.Serializable;

public class VacationToUpdate implements Serializable{

    private String status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
