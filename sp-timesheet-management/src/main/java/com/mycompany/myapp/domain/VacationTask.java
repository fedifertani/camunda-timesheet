package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

/**
 * A VacationTask.
 */
@Document(collection = "vacation_task")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "vacationtask")
public class VacationTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("task_id")
    private String taskId;

    @Field("vacation_request_id")
    private String vacationRequestId;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTaskId() {
        return taskId;
    }

    public VacationTask taskId(String taskId) {
        this.taskId = taskId;
        return this;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getVacationRequestId() {
        return vacationRequestId;
    }

    public VacationTask vacationRequestId(String vacationRequestId) {
        this.vacationRequestId = vacationRequestId;
        return this;
    }

    public void setVacationRequestId(String vacationRequestId) {
        this.vacationRequestId = vacationRequestId;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        VacationTask vacationTask = (VacationTask) o;
        if (vacationTask.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), vacationTask.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "VacationTask{" +
            "id=" + getId() +
            ", taskId='" + getTaskId() + "'" +
            ", vacationRequestId='" + getVacationRequestId() + "'" +
            "}";
    }
}
