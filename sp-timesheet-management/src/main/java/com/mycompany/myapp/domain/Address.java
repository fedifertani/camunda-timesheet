package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Address.
 */
@Document(collection = "address")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("code")
    private String code;

    @Field("created_date")
    private LocalDate createdDate;

    @Field("updated_date")
    private LocalDate updatedDate;

    @Field("apartment")
    private String apartment;

    @Field("buildng")
    private String buildng;

    @Field("department")
    private String department;

    @Field("pobox")
    private String pobox;

    @Field("posta_code")
    private String postaCode;

    @Field("street_name")
    private String streetName;

    @Field("street_number")
    private String streetNumber;

    @Field("town")
    private String town;

    @DBRef
    @Field("country")
    private Country country;

    @DBRef
    @Field("type")
    private AddressType type;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public Address code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public Address createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public Address updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public String getApartment() {
        return apartment;
    }

    public Address apartment(String apartment) {
        this.apartment = apartment;
        return this;
    }

    public void setApartment(String apartment) {
        this.apartment = apartment;
    }

    public String getBuildng() {
        return buildng;
    }

    public Address buildng(String buildng) {
        this.buildng = buildng;
        return this;
    }

    public void setBuildng(String buildng) {
        this.buildng = buildng;
    }

    public String getDepartment() {
        return department;
    }

    public Address department(String department) {
        this.department = department;
        return this;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPobox() {
        return pobox;
    }

    public Address pobox(String pobox) {
        this.pobox = pobox;
        return this;
    }

    public void setPobox(String pobox) {
        this.pobox = pobox;
    }

    public String getPostaCode() {
        return postaCode;
    }

    public Address postaCode(String postaCode) {
        this.postaCode = postaCode;
        return this;
    }

    public void setPostaCode(String postaCode) {
        this.postaCode = postaCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public Address streetName(String streetName) {
        this.streetName = streetName;
        return this;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public Address streetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
        return this;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getTown() {
        return town;
    }

    public Address town(String town) {
        this.town = town;
        return this;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public Country getCountry() {
        return country;
    }

    public Address country(Country country) {
        this.country = country;
        return this;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public AddressType getType() {
        return type;
    }

    public Address type(AddressType type) {
        this.type = type;
        return this;
    }

    public void setType(AddressType type) {
        this.type = type;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address = (Address) o;
        if (address.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), address.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Address{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", apartment='" + getApartment() + "'" +
            ", buildng='" + getBuildng() + "'" +
            ", department='" + getDepartment() + "'" +
            ", pobox='" + getPobox() + "'" +
            ", postaCode='" + getPostaCode() + "'" +
            ", streetName='" + getStreetName() + "'" +
            ", streetNumber='" + getStreetNumber() + "'" +
            ", town='" + getTown() + "'" +
            ", country='" + getCountry() + "'" +
            ", type='" + getType() + "'" +
            "}";
    }
}
