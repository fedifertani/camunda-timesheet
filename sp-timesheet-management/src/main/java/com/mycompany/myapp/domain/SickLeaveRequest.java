package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Objects;

/**
 * A SickLeaveRequest.
 */
@Document(collection = "sick_leave_request")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "sickleaverequest")
public class SickLeaveRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("media")
    private String media;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMedia() {
        return media;
    }

    public SickLeaveRequest media(String media) {
        this.media = media;
        return this;
    }

    public void setMedia(String media) {
        this.media = media;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SickLeaveRequest sickLeaveRequest = (SickLeaveRequest) o;
        if (sickLeaveRequest.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), sickLeaveRequest.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "SickLeaveRequest{" +
            "id=" + getId() +
            ", media='" + getMedia() + "'" +
            "}";
    }
}
