package com.mycompany.myapp.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A ProjectMemberShip.
 */
@Document(collection = "project_member_ship")
@org.springframework.data.elasticsearch.annotations.Document(indexName = "projectmembership")
public class ProjectMemberShip implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @Field("code")
    private String code;

    @Field("created_date")
    private LocalDate createdDate;

    @Field("updated_date")
    private LocalDate updatedDate;

    @Field("rate")
    private BigDecimal rate;

    @DBRef
    @Field("user")
    private User user;

    @DBRef
    @Field("user_position")
    private UserPosition userPosition;

    @DBRef
    @Field("project")
    private Project project;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public ProjectMemberShip code(String code) {
        this.code = code;
        return this;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public LocalDate getCreatedDate() {
        return createdDate;
    }

    public ProjectMemberShip createdDate(LocalDate createdDate) {
        this.createdDate = createdDate;
        return this;
    }

    public void setCreatedDate(LocalDate createdDate) {
        this.createdDate = createdDate;
    }

    public LocalDate getUpdatedDate() {
        return updatedDate;
    }

    public ProjectMemberShip updatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
        return this;
    }

    public void setUpdatedDate(LocalDate updatedDate) {
        this.updatedDate = updatedDate;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public ProjectMemberShip rate(BigDecimal rate) {
        this.rate = rate;
        return this;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public User getUser() {
        return user;
    }

    public ProjectMemberShip user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserPosition getUserPosition() {
        return userPosition;
    }

    public ProjectMemberShip userPosition(UserPosition userPosition) {
        this.userPosition = userPosition;
        return this;
    }

    public void setUserPosition(UserPosition userPosition) {
        this.userPosition = userPosition;
    }

    public Project getProject() {
        return project;
    }

    public ProjectMemberShip project(Project project) {
        this.project = project;
        return this;
    }

    public void setProject(Project project) {
        this.project = project;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ProjectMemberShip projectMemberShip = (ProjectMemberShip) o;
        if (projectMemberShip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), projectMemberShip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ProjectMemberShip{" +
            "id=" + getId() +
            ", code='" + getCode() + "'" +
            ", createdDate='" + getCreatedDate() + "'" +
            ", updatedDate='" + getUpdatedDate() + "'" +
            ", rate=" + getRate() +
            ", user='" + getUser() + "'" +
            ", userPosition='" + getUserPosition() + "'" +
            ", project='" + getProject() + "'" +
            "}";
    }
}
