package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.ProjectMemberShip;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the ProjectMemberShip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProjectMemberShipRepository extends MongoRepository<ProjectMemberShip, String> {

}
