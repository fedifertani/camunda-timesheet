package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.SickLeaveRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the SickLeaveRequest entity.
 */
public interface SickLeaveRequestSearchRepository extends ElasticsearchRepository<SickLeaveRequest, String> {
}
