package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.VacationType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VacationType entity.
 */
public interface VacationTypeSearchRepository extends ElasticsearchRepository<VacationType, String> {
}
