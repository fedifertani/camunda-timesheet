package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.VacationTask;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VacationTask entity.
 */
public interface VacationTaskSearchRepository extends ElasticsearchRepository<VacationTask, String> {
}
