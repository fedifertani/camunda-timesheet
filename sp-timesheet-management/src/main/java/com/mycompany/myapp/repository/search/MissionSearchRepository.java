package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Mission;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Mission entity.
 */
public interface MissionSearchRepository extends ElasticsearchRepository<Mission, String> {
}
