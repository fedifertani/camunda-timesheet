package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Module;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the Module entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ModuleRepository extends MongoRepository<Module, String> {

}
