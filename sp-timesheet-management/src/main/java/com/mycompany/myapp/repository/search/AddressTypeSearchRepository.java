package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.AddressType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the AddressType entity.
 */
public interface AddressTypeSearchRepository extends ElasticsearchRepository<AddressType, String> {
}
