package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.UserPosition;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the UserPosition entity.
 */
public interface UserPositionSearchRepository extends ElasticsearchRepository<UserPosition, String> {
}
