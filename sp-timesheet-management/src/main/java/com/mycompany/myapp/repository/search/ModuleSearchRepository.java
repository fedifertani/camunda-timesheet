package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.Module;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Module entity.
 */
public interface ModuleSearchRepository extends ElasticsearchRepository<Module, String> {
}
