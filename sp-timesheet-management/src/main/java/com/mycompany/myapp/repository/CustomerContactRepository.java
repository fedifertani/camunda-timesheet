package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CustomerContact;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the CustomerContact entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerContactRepository extends MongoRepository<CustomerContact, String> {

}
