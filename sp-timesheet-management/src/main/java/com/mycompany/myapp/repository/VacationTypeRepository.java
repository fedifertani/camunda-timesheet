package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.VacationType;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the VacationType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VacationTypeRepository extends MongoRepository<VacationType, String> {

}
