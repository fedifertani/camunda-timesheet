package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.TimeEntry;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the TimeEntry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TimeEntryRepository extends MongoRepository<TimeEntry, String> {

}
