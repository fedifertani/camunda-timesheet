package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.SickLeaveRequest;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the SickLeaveRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface SickLeaveRequestRepository extends MongoRepository<SickLeaveRequest, String> {

}
