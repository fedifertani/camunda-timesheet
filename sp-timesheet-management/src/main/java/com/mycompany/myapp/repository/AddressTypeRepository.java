package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.AddressType;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the AddressType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AddressTypeRepository extends MongoRepository<AddressType, String> {

}
