package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.CustomerContactType;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the CustomerContactType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CustomerContactTypeRepository extends MongoRepository<CustomerContactType, String> {

}
