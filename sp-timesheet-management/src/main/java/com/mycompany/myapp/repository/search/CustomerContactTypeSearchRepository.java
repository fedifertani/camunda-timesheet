package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.CustomerContactType;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CustomerContactType entity.
 */
public interface CustomerContactTypeSearchRepository extends ElasticsearchRepository<CustomerContactType, String> {
}
