package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.CustomerContact;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the CustomerContact entity.
 */
public interface CustomerContactSearchRepository extends ElasticsearchRepository<CustomerContact, String> {
}
