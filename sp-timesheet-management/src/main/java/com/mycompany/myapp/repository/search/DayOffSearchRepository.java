package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.DayOff;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the DayOff entity.
 */
public interface DayOffSearchRepository extends ElasticsearchRepository<DayOff, String> {
}
