package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.VacationRequest;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the VacationRequest entity.
 */
public interface VacationRequestSearchRepository extends ElasticsearchRepository<VacationRequest, String> {
}
