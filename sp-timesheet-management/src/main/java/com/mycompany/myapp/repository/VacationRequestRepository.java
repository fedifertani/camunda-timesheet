package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.VacationRequest;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the VacationRequest entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VacationRequestRepository extends MongoRepository<VacationRequest, String> {

    VacationRequest findByCode(String code);
}
