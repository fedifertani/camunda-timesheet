package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.DayOff;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the DayOff entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayOffRepository extends MongoRepository<DayOff, String> {

}
