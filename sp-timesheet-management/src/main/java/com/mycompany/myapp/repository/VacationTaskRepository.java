package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.VacationTask;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the VacationTask entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VacationTaskRepository extends MongoRepository<VacationTask, String> {

}
