package com.mycompany.myapp.repository.search;

import com.mycompany.myapp.domain.TimeEntry;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the TimeEntry entity.
 */
public interface TimeEntrySearchRepository extends ElasticsearchRepository<TimeEntry, String> {
}
