package com.mycompany.myapp.repository;

import java.util.List;

import com.mycompany.myapp.domain.UserPosition;
import org.springframework.stereotype.Repository;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the UserPosition entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserPositionRepository extends MongoRepository<UserPosition, String> {

    UserPosition  findByName(String name);
}
