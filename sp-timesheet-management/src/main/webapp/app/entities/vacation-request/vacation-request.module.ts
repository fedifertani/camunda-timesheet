import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    VacationRequestService,
    VacationRequestPopupService,
    VacationRequestComponent,
    VacationRequestDetailComponent,
    VacationRequestDialogComponent,
    VacationRequestPopupComponent,
    VacationRequestDeletePopupComponent,
    VacationRequestDeleteDialogComponent,
    vacationRequestRoute,
    vacationRequestPopupRoute,
} from './';

const ENTITY_STATES = [
    ...vacationRequestRoute,
    ...vacationRequestPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VacationRequestComponent,
        VacationRequestDetailComponent,
        VacationRequestDialogComponent,
        VacationRequestDeleteDialogComponent,
        VacationRequestPopupComponent,
        VacationRequestDeletePopupComponent,
    ],
    entryComponents: [
        VacationRequestComponent,
        VacationRequestDialogComponent,
        VacationRequestPopupComponent,
        VacationRequestDeleteDialogComponent,
        VacationRequestDeletePopupComponent,
    ],
    providers: [
        VacationRequestService,
        VacationRequestPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetVacationRequestModule {}
