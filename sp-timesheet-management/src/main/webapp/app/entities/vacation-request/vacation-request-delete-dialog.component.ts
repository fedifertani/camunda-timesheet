import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationRequest } from './vacation-request.model';
import { VacationRequestPopupService } from './vacation-request-popup.service';
import { VacationRequestService } from './vacation-request.service';

@Component({
    selector: 'jhi-vacation-request-delete-dialog',
    templateUrl: './vacation-request-delete-dialog.component.html'
})
export class VacationRequestDeleteDialogComponent {

    vacationRequest: VacationRequest;

    constructor(
        private vacationRequestService: VacationRequestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.vacationRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'vacationRequestListModification',
                content: 'Deleted an vacationRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-vacation-request-delete-popup',
    template: ''
})
export class VacationRequestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationRequestPopupService: VacationRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.vacationRequestPopupService
                .open(VacationRequestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
