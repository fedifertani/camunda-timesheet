import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationRequest } from './vacation-request.model';
import { VacationRequestPopupService } from './vacation-request-popup.service';
import { VacationRequestService } from './vacation-request.service';
import { Principal, UserService } from '../../shared';
import { VacationType, VacationTypeService } from '../vacation-type';
import { UserPosition, UserPositionService } from '../user-position';
let moment = require('moment');
@Component({
    selector: 'jhi-vacation-request-dialog',
    templateUrl: './vacation-request-dialog.component.html'
})
export class VacationRequestDialogComponent implements OnInit {

    vacationRequest: VacationRequest;
    isSaving: boolean;
    startDateDp: any;
    endDateDp: any;
    createdDateDp: any;
    updatedDateDp: any;
    currentAccount:any;
    currentSearch: string;
    status: string[];
    types: VacationType[];

    constructor(
        public activeModal: NgbActiveModal,
        private vacationRequestService: VacationRequestService,
        private eventManager: JhiEventManager,
        private principale:Principal,
        private userService:UserService,
        private vacationTypeService: VacationTypeService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.principale.identity().then(account => {
            this.currentAccount = account;
        });
        this.vacationTypeService.query().subscribe(
            (res: HttpResponse<VacationType[]>) => {
                this.types = res.body;
                this.currentSearch = '';
            },
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vacationRequest.id !== undefined) {
            this.vacationRequest.updatedDate=moment().toDate();

            this.subscribeToSaveResponse(
                
                this.vacationRequestService.update(this.vacationRequest));
        } else {
            this.vacationRequest.user=this.currentAccount;
            this.vacationRequest.status='Pending';
            this.vacationRequest.code=new Date().getTime()+"";
            this.vacationRequest.createdDate=moment().toDate();
            this.vacationRequest.updatedDate=moment().toDate();
            if(moment(this.vacationRequest.endDate)-moment(this.vacationRequest.startDate) >0 ) {

            this.subscribeToSaveResponse(
                this.vacationRequestService.create(this.vacationRequest));
                

                this.userService.findByPosition('RH').subscribe((pos) =>
                 {
                     for(let i=0;i<pos.body.length;i++)
                     {
                        this.subscribeToSaveResponse(this.vacationRequestService.sendSignal(this.vacationRequest,pos.body[i].login));
                     }
                }
                );

            }
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<VacationRequest>>) {
        result.subscribe((res: HttpResponse<VacationRequest>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: VacationRequest) {
        this.eventManager.broadcast({ name: 'vacationRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-vacation-request-popup',
    template: ''
})
export class VacationRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationRequestPopupService: VacationRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vacationRequestPopupService
                    .open(VacationRequestDialogComponent as Component, params['id']);
            } else {
                this.vacationRequestPopupService
                    .open(VacationRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
