import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { VacationRequest } from './vacation-request.model';
import { VacationRequestService } from './vacation-request.service';

@Injectable()
export class VacationRequestPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private vacationRequestService: VacationRequestService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.vacationRequestService.find(id)
                    .subscribe((vacationRequestResponse: HttpResponse<VacationRequest>) => {
                        const vacationRequest: VacationRequest = vacationRequestResponse.body;
                        if (vacationRequest.startDate) {
                            vacationRequest.startDate = {
                                year: vacationRequest.startDate.getFullYear(),
                                month: vacationRequest.startDate.getMonth() + 1,
                                day: vacationRequest.startDate.getDate()
                            };
                        }
                        if (vacationRequest.endDate) {
                            vacationRequest.endDate = {
                                year: vacationRequest.endDate.getFullYear(),
                                month: vacationRequest.endDate.getMonth() + 1,
                                day: vacationRequest.endDate.getDate()
                            };
                        }

                        this.ngbModalRef = this.vacationRequestModalRef(component, vacationRequest);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.vacationRequestModalRef(component, new VacationRequest());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    vacationRequestModalRef(component: Component, vacationRequest: VacationRequest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.vacationRequest = vacationRequest;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
