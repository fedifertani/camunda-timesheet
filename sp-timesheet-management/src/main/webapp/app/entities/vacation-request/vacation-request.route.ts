import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { VacationRequestComponent } from './vacation-request.component';
import { VacationRequestDetailComponent } from './vacation-request-detail.component';
import { VacationRequestPopupComponent } from './vacation-request-dialog.component';
import { VacationRequestDeletePopupComponent } from './vacation-request-delete-dialog.component';

export const vacationRequestRoute: Routes = [
    {
        path: 'vacation-request',
        component: VacationRequestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vacation-request/:id',
        component: VacationRequestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vacationRequestPopupRoute: Routes = [
    {
        path: 'vacation-request-new',
        component: VacationRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-request/:id/edit',
        component: VacationRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-request/:id/delete',
        component: VacationRequestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
