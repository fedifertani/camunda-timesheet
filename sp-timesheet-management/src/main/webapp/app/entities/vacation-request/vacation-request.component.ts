import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { VacationRequest } from './vacation-request.model';
import { VacationRequestService } from './vacation-request.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-vacation-request',
    templateUrl: './vacation-request.component.html'
})
export class VacationRequestComponent implements OnInit, OnDestroy {
vacationRequests: VacationRequest[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private vacationRequestService: VacationRequestService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal,

    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.vacationRequestService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<VacationRequest[]>) => this.vacationRequests = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.vacationRequestService.query().subscribe(
            (res: HttpResponse<VacationRequest[]>) => {
                this.vacationRequests = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVacationRequests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VacationRequest) {
        return item.id;
    }
    registerChangeInVacationRequests() {
        this.eventSubscriber = this.eventManager.subscribe('vacationRequestListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
