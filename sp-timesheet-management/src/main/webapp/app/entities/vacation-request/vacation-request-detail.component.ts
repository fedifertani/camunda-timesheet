import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { VacationRequest } from './vacation-request.model';
import { VacationRequestService } from './vacation-request.service';

@Component({
    selector: 'jhi-vacation-request-detail',
    templateUrl: './vacation-request-detail.component.html'
})
export class VacationRequestDetailComponent implements OnInit, OnDestroy {

    vacationRequest: VacationRequest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private vacationRequestService: VacationRequestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVacationRequests();
    }

    load(id) {
        this.vacationRequestService.find(id)
            .subscribe((vacationRequestResponse: HttpResponse<VacationRequest>) => {
                this.vacationRequest = vacationRequestResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVacationRequests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'vacationRequestListModification',
            (response) => this.load(this.vacationRequest.id)
        );
    }
}
