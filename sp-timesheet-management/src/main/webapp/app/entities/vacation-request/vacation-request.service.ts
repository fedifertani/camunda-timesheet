import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL, CAMUNDA_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { VacationRequest } from './vacation-request.model';
import { createRequestOption } from '../../shared';
import { Signal } from './signal.model';

export type EntityResponseType = HttpResponse<VacationRequest>;

@Injectable()
export class VacationRequestService {

    private resourceUrl =  SERVER_API_URL + 'api/vacation-requests';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vacation-requests';
    private taskUrl=CAMUNDA_API_URL+ 'holidaySignal';
    private signal:Signal;
    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(vacationRequest: VacationRequest): Observable<EntityResponseType> {

        const copy = this.convert(vacationRequest);
        console.log('from create : '+vacationRequest.code);

        return this.http.post<VacationRequest>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }
    sendSignal(vacation: VacationRequest,user:string): Observable<EntityResponseType> {
        /*Supposons que admin=UserRH*/
        this.signal=new Signal('holiday',user, vacation.code);
       return this.http.post(this.taskUrl,this.signal, { observe: 'response' });
    }
    update(vacationRequest: VacationRequest): Observable<EntityResponseType> {
        const copy = this.convert(vacationRequest);
        return this.http.put<VacationRequest>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<VacationRequest>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<VacationRequest[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationRequest[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationRequest[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<VacationRequest[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationRequest[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationRequest[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: VacationRequest = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<VacationRequest[]>): HttpResponse<VacationRequest[]> {
        const jsonResponse: VacationRequest[] = res.body;
        const body: VacationRequest[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to VacationRequest.
     */
    private convertItemFromServer(vacationRequest: VacationRequest): VacationRequest {
        const copy: VacationRequest = Object.assign({}, vacationRequest);
        copy.startDate = this.dateUtils
            .convertLocalDateFromServer(vacationRequest.startDate);
        copy.endDate = this.dateUtils
            .convertLocalDateFromServer(vacationRequest.endDate);
      /*  copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(vacationRequest.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(vacationRequest.updatedDate);
        */return copy;
    }

    /**
     * Convert a VacationRequest to a JSON which can be sent to the server.
     */
    private convert(vacationRequest: VacationRequest): VacationRequest {
        const copy: VacationRequest = Object.assign({}, vacationRequest);
        copy.startDate = this.dateUtils
            .convertLocalDateToServer(vacationRequest.startDate);
        copy.endDate = this.dateUtils
            .convertLocalDateToServer(vacationRequest.endDate);
        /*copy.createdDate = this.dateUtils
            .convertLocalDateToServer(vacationRequest.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(vacationRequest.updatedDate);
        */return copy;
    }

    findByCode(code:string): Observable<EntityResponseType> {
        return this.http
            .get<VacationRequest>(`${this.resourceUrl}/code/${code}`, { observe: 'response' })
         .map((res: EntityResponseType) => this.convertResponse(res));
    }
}
