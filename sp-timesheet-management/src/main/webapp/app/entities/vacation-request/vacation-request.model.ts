import { BaseEntity, User } from './../../shared';
import { VacationType } from '../vacation-type';

export class VacationRequest implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public startDate?: any,
        public endDate?: any,
        public reason?: string,
        public status?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public type?: VacationType,
        public user?: User,
    ) {
    }
}
