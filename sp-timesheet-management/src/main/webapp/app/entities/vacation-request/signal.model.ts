
export interface ISignal {
    signal?: string;
    userRH?: string;
    vacationRequestId?: string;
}

export class Signal implements ISignal {
    constructor(
        public signal?: string,
        public userRH?: string,
        public vacationRequestId?: string
    ) {}
}
