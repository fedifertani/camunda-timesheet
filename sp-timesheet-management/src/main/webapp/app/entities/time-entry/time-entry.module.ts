import { NgModule, CUSTOM_ELEMENTS_SCHEMA, Provider, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import { FlatpickrModule } from 'angularx-flatpickr';
import {

    TimeEntryService,
    TimeEntryPopupService,
    TimeEntryComponent,
    TimeEntryDetailComponent,
    TimeEntryDialogComponent,
    TimeEntryPopupComponent,
    TimeEntryDeletePopupComponent,
    TimeEntryDeleteDialogComponent,
    timeEntryRoute,
    timeEntryPopupRoute,
} from './';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {  CalendarEventTitleFormatter, CalendarDateFormatter, CalendarUtils, CalendarModuleConfig, DateAdapter, CalendarCommonModule } from 
'../calendar/common/calendar-common.module';

import { ResizableModule } from 'angular-resizable-element';
import { DragAndDropModule } from 'angular-draggable-droppable';
import { adapterFactory } from 'calendar-utils/date-adapters/date-fns';
import { CalendarModule } from '../calendar/calendar.module';
const ENTITY_STATES = [
    ...timeEntryRoute,
    ...timeEntryPopupRoute,
];
export {
    CalendarDayViewBeforeRenderEvent
  } from '../calendar/timesheet-calendar-day-view/timesheet-calendar-day-view.component';
@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES),
        CommonModule,
        TimesheetTimeEntryModule.forRoot({
            provide: DateAdapter,
            useFactory: adapterFactory
        }),
    FormsModule,
    NgbModalModule.forRoot(),
    FlatpickrModule.forRoot(),
    CalendarModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot()
    ],
    declarations: [
        TimeEntryComponent,
        TimeEntryDetailComponent,
        TimeEntryDialogComponent,
        TimeEntryDeleteDialogComponent,
        TimeEntryPopupComponent,
        TimeEntryDeletePopupComponent,
    ],
    entryComponents: [
        TimeEntryComponent,
        TimeEntryDialogComponent,
        TimeEntryPopupComponent,
        TimeEntryDeleteDialogComponent,
        TimeEntryDeletePopupComponent,
    ],
    exports: [
        CalendarCommonModule,
    ],
    providers: [
        TimeEntryService,
        TimeEntryPopupService,
        CalendarUtils,
      
        ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetTimeEntryModule {

    static forRoot(
        dateAdapter: Provider,
        config: CalendarModuleConfig = {}
      ): ModuleWithProviders {
        return {
          ngModule: CalendarModule,
          providers: [
            dateAdapter,
            config.eventTitleFormatter || CalendarEventTitleFormatter,
            config.dateFormatter || CalendarDateFormatter,
            config.utils || CalendarUtils
          ]
        };
    }
}
