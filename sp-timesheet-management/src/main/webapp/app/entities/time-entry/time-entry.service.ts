import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { TimeEntry } from './time-entry.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<TimeEntry>;

@Injectable()
export class TimeEntryService {

    private resourceUrl =  SERVER_API_URL + 'api/time-entries';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/time-entries';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(timeEntry: TimeEntry): Observable<EntityResponseType> {
        const copy = this.convert(timeEntry);
        return this.http.post<TimeEntry>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(timeEntry: TimeEntry): Observable<EntityResponseType> {
        const copy = this.convert(timeEntry);
        return this.http.put<TimeEntry>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<TimeEntry>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<TimeEntry[]>> {
        const options = createRequestOption(req);
        return this.http.get<TimeEntry[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TimeEntry[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<TimeEntry[]>> {
        const options = createRequestOption(req);
        return this.http.get<TimeEntry[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<TimeEntry[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: TimeEntry = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<TimeEntry[]>): HttpResponse<TimeEntry[]> {
        const jsonResponse: TimeEntry[] = res.body;
        const body: TimeEntry[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to TimeEntry.
     */
    private convertItemFromServer(timeEntry: TimeEntry): TimeEntry {
        const copy: TimeEntry = Object.assign({}, timeEntry);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(timeEntry.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(timeEntry.updatedDate);
        return copy;
    }

    /**
     * Convert a TimeEntry to a JSON which can be sent to the server.
     */
    private convert(timeEntry: TimeEntry): TimeEntry {
        const copy: TimeEntry = Object.assign({}, timeEntry);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(timeEntry.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(timeEntry.updatedDate);
        return copy;
    }
}
