import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TimeEntry } from './time-entry.model';
import { TimeEntryPopupService } from './time-entry-popup.service';
import { TimeEntryService } from './time-entry.service';
import { User, UserService } from '../../shared';
import { Project, ProjectService } from '../project';
import { Module, ModuleService } from '../module';

@Component({
    selector: 'jhi-time-entry-dialog',
    templateUrl: './time-entry-dialog.component.html'
})
export class TimeEntryDialogComponent implements OnInit {

    timeEntry: TimeEntry;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;
    currentSearch: string;
    status: string[];
    modules: Module[];
    projects: Project[];
    users: User[];

    constructor(
        public activeModal: NgbActiveModal,
        private timeEntryService: TimeEntryService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private moduleService: ModuleService,
        private projectService: ProjectService,
        private userService: UserService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.moduleService.query().subscribe(
            (res: HttpResponse<Module[]>) => {
                this.modules = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.projectService.query().subscribe(
            (res: HttpResponse<Project[]>) => {
                this.projects = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userService.query().subscribe(
            (res: HttpResponse<User[]>) => {
                this.users = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.timeEntry.id !== undefined) {
            this.subscribeToSaveResponse(
                this.timeEntryService.update(this.timeEntry));
        } else {
            this.subscribeToSaveResponse(
                this.timeEntryService.create(this.timeEntry));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<TimeEntry>>) {
        result.subscribe((res: HttpResponse<TimeEntry>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: TimeEntry) {
        this.eventManager.broadcast({ name: 'timeEntryListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-time-entry-popup',
    template: ''
})
export class TimeEntryPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private timeEntryPopupService: TimeEntryPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.timeEntryPopupService
                    .open(TimeEntryDialogComponent as Component, params['id']);
            } else {
                this.timeEntryPopupService
                    .open(TimeEntryDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
