import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { TimeEntry } from './time-entry.model';
import { TimeEntryService } from './time-entry.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-time-entry',
    templateUrl: './time-entry.component.html'
})
export class TimeEntryComponent implements OnInit, OnDestroy {
timeEntries: TimeEntry[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private timeEntryService: TimeEntryService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.timeEntryService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<TimeEntry[]>) => this.timeEntries = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.timeEntryService.query().subscribe(
            (res: HttpResponse<TimeEntry[]>) => {
                this.timeEntries = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInTimeEntries();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: TimeEntry) {
        return item.id;
    }
    registerChangeInTimeEntries() {
        this.eventSubscriber = this.eventManager.subscribe('timeEntryListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
