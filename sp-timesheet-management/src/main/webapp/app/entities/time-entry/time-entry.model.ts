import { BaseEntity, User } from './../../shared';
import { Project } from '../project';
import { Module } from '../module';

export class TimeEntry implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public description?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public duration?: number,
        public status?: string,
        public module?: Module,
        public project?: Project,
        public user?: User,
    ) {
    }
}
