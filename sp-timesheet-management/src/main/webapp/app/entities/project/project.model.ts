import { BaseEntity } from './../../shared';
import { Customer } from '../customer';
import { Currency } from '../currency';

export class Project implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public name?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public summury?: string,
        public description?: string,
        public startDate?: any,
        public endDate?: any,
        public status?: string,
        public currency?: Currency,
        public customer?: Customer,
    ) {
    }
}
