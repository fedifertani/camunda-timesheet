import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { VacationTaskComponent } from './vacation-task.component';
import { VacationTaskDetailComponent } from './vacation-task-detail.component';
import { VacationTaskPopupComponent } from './vacation-task-dialog.component';
import { VacationTaskDeletePopupComponent } from './vacation-task-delete-dialog.component';

@Injectable()
export class VacationTaskResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const vacationTaskRoute: Routes = [
    {
        path: 'vacation-task',
        component: VacationTaskComponent,
        resolve: {
            'pagingParams': VacationTaskResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationTask.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vacation-task/:id',
        component: VacationTaskDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationTask.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vacationTaskPopupRoute: Routes = [
    {
        path: 'vacation-task-new',
        component: VacationTaskPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationTask.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-task/:id/edit',
        component: VacationTaskPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationTask.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-task/:id/delete',
        component: VacationTaskDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationTask.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
