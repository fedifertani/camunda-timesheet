import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationTask } from './vacation-task.model';
import { VacationTaskPopupService } from './vacation-task-popup.service';
import { VacationTaskService } from './vacation-task.service';

@Component({
    selector: 'jhi-vacation-task-dialog',
    templateUrl: './vacation-task-dialog.component.html'
})
export class VacationTaskDialogComponent implements OnInit {

    vacationTask: VacationTask;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private vacationTaskService: VacationTaskService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vacationTask.id !== undefined) {
            this.subscribeToSaveResponse(
                this.vacationTaskService.update(this.vacationTask));
        } else {
            this.subscribeToSaveResponse(
                this.vacationTaskService.create(this.vacationTask));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<VacationTask>>) {
        result.subscribe((res: HttpResponse<VacationTask>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: VacationTask) {
        this.eventManager.broadcast({ name: 'vacationTaskListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-vacation-task-popup',
    template: ''
})
export class VacationTaskPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationTaskPopupService: VacationTaskPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vacationTaskPopupService
                    .open(VacationTaskDialogComponent as Component, params['id']);
            } else {
                this.vacationTaskPopupService
                    .open(VacationTaskDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
