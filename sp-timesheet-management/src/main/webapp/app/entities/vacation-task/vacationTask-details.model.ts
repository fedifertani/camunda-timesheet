import { Moment } from 'moment';

export interface IVacationTaskDetail {
    taskId?: string;
    user?: string;
    from?: Moment;
    to?: Moment;
    reason?: string;
    type? : string;
    duree?:string;


}

export class VacationTaskDetail implements IVacationTaskDetail {
    constructor( public taskId?: string,
        public user?: string,
        public from?: Moment,
        public to?: Moment,
        public reason?: string,
        public type?: string,
        public  duree?:string

        ) {}
}
