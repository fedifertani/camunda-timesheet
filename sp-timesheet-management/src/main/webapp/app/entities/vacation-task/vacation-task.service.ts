import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL, CAMUNDA_API_URL } from '../../app.constants';

import { VacationTask } from './vacation-task.model';
import { createRequestOption } from '../../shared';
import { SubmitFormVariables } from './submitFormVaraibles.model';

export type EntityResponseType = HttpResponse<VacationTask>;
type EntityArrayResponseType = HttpResponse<VacationTask[]>;

@Injectable()
export class VacationTaskService {
    private taskUrl= CAMUNDA_API_URL+ '/holidayTasks';

    private resourceUrl =  SERVER_API_URL + 'api/vacation-tasks';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vacation-tasks';
    private approvalUrl=CAMUNDA_API_URL+ 'submitform/';

    private form:SubmitFormVariables;
    constructor(private http: HttpClient) { }

    create(vacationTask: VacationTask): Observable<EntityResponseType> {
        const copy = this.convert(vacationTask);
        return this.http.post<VacationTask>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(vacationTask: VacationTask): Observable<EntityResponseType> {
        const copy = this.convert(vacationTask);
        return this.http.put<VacationTask>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<VacationTask>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<VacationTask[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationTask[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationTask[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<VacationTask[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationTask[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationTask[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: VacationTask = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<VacationTask[]>): HttpResponse<VacationTask[]> {
        const jsonResponse: VacationTask[] = res.body;
        const body: VacationTask[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to VacationTask.
     */
    private convertItemFromServer(vacationTask: VacationTask): VacationTask {
        const copy: VacationTask = Object.assign({}, vacationTask);
        return copy;
    }

    /**
     * Convert a VacationTask to a JSON which can be sent to the server.
     */
    private convert(vacationTask: VacationTask): VacationTask {
        const copy: VacationTask = Object.assign({}, vacationTask);
        return copy;
    }


    findByAssignee(assignee: string): Observable<EntityArrayResponseType> {
        return this.http.get<VacationTask[]>(this.taskUrl+'/'+assignee,  { observe: 'response' });
    }
    actionForm(reviewer:string,id:string,approve:string): Observable<any>  {
        this.form=new SubmitFormVariables(reviewer,approve);
        console.log(this.form);
        console.log(this.approvalUrl+id);
        let headers = new HttpHeaders();
        headers = headers.append("Authorization", "Basic " + btoa("admin:admin"));
        let options = { headers: headers };
       return this.http.post(this.approvalUrl+id+'/'+approve,this.form,options);
    }
    
}
