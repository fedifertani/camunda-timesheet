import { BaseEntity } from './../../shared';

export class VacationTask implements BaseEntity {
    constructor(
        public id?: string,
        public taskId?: string,
        public vacationRequestId?: string,
    ) {
    }
}
