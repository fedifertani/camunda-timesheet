import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    VacationTaskService,
    VacationTaskPopupService,
    VacationTaskComponent,
    VacationTaskDetailComponent,
    VacationTaskDialogComponent,
    VacationTaskPopupComponent,
    VacationTaskDeletePopupComponent,
    VacationTaskDeleteDialogComponent,
    vacationTaskRoute,
    vacationTaskPopupRoute,
    VacationTaskResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...vacationTaskRoute,
    ...vacationTaskPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VacationTaskComponent,
        VacationTaskDetailComponent,
        VacationTaskDialogComponent,
        VacationTaskDeleteDialogComponent,
        VacationTaskPopupComponent,
        VacationTaskDeletePopupComponent,
    ],
    entryComponents: [
        VacationTaskComponent,
        VacationTaskDialogComponent,
        VacationTaskPopupComponent,
        VacationTaskDeleteDialogComponent,
        VacationTaskDeletePopupComponent,
    ],
    providers: [
        VacationTaskService,
        VacationTaskPopupService,
        VacationTaskResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetVacationTaskModule {}
