import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { VacationTask } from './vacation-task.model';
import { VacationTaskService } from './vacation-task.service';

@Component({
    selector: 'jhi-vacation-task-detail',
    templateUrl: './vacation-task-detail.component.html'
})
export class VacationTaskDetailComponent implements OnInit, OnDestroy {

    vacationTask: VacationTask;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private vacationTaskService: VacationTaskService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVacationTasks();
    }

    load(id) {
        this.vacationTaskService.find(id)
            .subscribe((vacationTaskResponse: HttpResponse<VacationTask>) => {
                this.vacationTask = vacationTaskResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVacationTasks() {
        this.eventSubscriber = this.eventManager.subscribe(
            'vacationTaskListModification',
            (response) => this.load(this.vacationTask.id)
        );
    }
}
