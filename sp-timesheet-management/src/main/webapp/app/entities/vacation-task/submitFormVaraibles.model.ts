export interface ISubmitFormVariables {
    assignee?: string;
    isapproved?: string;
 
}

export class SubmitFormVariables implements ISubmitFormVariables {
    constructor(
        public assignee?: string,
        public isapproved?: string
    ) {}
}
