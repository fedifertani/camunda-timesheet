import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse, HttpClient, HttpHandler } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiParseLinks, JhiAlertService, JhiDateUtils } from 'ng-jhipster';

import { VacationTask } from './vacation-task.model';
import { VacationTaskService } from './vacation-task.service';
import { ITEMS_PER_PAGE, Principal, UserService, User } from '../../shared';
import { VacationRequest, VacationRequestService } from '../vacation-request';
import { VacationTaskDetail } from './vacationTask-details.model';
import { SERVER_API_URL } from '../../app.constants';
import { Moment } from 'moment';

@Component({
    selector: 'jhi-vacation-task',
    templateUrl: './vacation-task.component.html'
})
export class VacationTaskComponent implements OnInit, OnDestroy {

    currentAccount: any;
    private resourceUrl = SERVER_API_URL + 'api/vacation-requests';

    vacationTasks: VacationTask[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    currentSearch: string;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    tasks: VacationTaskDetail[];
    assignee: any;
    duree: any;
    reviewer:string;
    vacationRequestService: VacationRequestService;
    vacationTaskDetail: VacationTaskDetail;
    managers: User[];
    constructor(
        private vacationTaskService: VacationTaskService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private http: HttpClient,
        private userService:UserService,
        private eventManager: JhiEventManager
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = data.pagingParams.page;
            this.previousPage = data.pagingParams.page;
            this.reverse = data.pagingParams.ascending;
            this.predicate = data.pagingParams.predicate;
        });
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        let moment = require('moment');

        if (this.currentSearch) {
            this.vacationTaskService.search({
                page: this.page - 1,
                query: this.currentSearch,
                size: this.itemsPerPage,
                sort: this.sort()
            }).subscribe(
                (res: HttpResponse<VacationTask[]>) => this.onSuccess(res.body, res.headers),
                (res: HttpErrorResponse) => this.onError(res.message)
            );
            return;
        }
        this.principal.identity().then(account => {
            this.currentAccount = account;
            this.assignee = this.currentAccount.login;

            this.vacationTaskService.findByAssignee(this.assignee).subscribe(
                (res: HttpResponse<VacationTask[]>) => {

                    console.log("Number : " + res.body.length);
                    for (let i = 0; i < res.body.length; i++) {
                        console.log("Vacation request id " + res.body[i].vacationRequestId);
                        this.vacationRequestService.findByCode(res.body[i].vacationRequestId).subscribe(
                            (vac: HttpResponse<VacationRequest>) => {

                                this.tasks[i] = new VacationTaskDetail();
                                this.tasks[i].taskId = res.body[i].taskId;
                                this.tasks[i].user = vac.body.user.login;
                                this.tasks[i].from = moment(vac.body.startDate);
                                this.tasks[i].to = moment(vac.body.endDate);
                                this.tasks[i].reason = vac.body.reason;
                                this.tasks[i].type = vac.body.type.name;
                                this.tasks[i].duree = moment(this.tasks[i].to).diff(this.tasks[i].from, 'd') + " Jours";
                            }
                        );

                    }

                },
                (res: HttpErrorResponse) => this.onError(res.message)

            );
        });
    }
    approve(id:string) {
        console.log(id);
        console.log(this.reviewer);
        if(this.reviewer==null)
        {
            this.reviewer="No reviewer";
        }
         this.vacationTaskService.actionForm(this.reviewer,id,"true").subscribe((res: HttpResponse<VacationTask>) =>  
         this.onsuccess());

    }
    onsuccess() {
        this.success = true;
         this.loadAll()
    }
    disapprove(id:string) {
        console.log(id);
         this.vacationTaskService.actionForm("",id,"false").subscribe((res: HttpResponse<VacationTask>) => 
         this.onsuccess());
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/vacation-task'], {
            queryParams:
            {
                page: this.page,
                size: this.itemsPerPage,
                search: this.currentSearch,
                sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
            }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.currentSearch = '';
        this.router.navigate(['/vacation-task', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    search(query) {
        if (!query) {
            return this.clear();
        }
        this.page = 0;
        this.currentSearch = query;
        this.router.navigate(['/vacation-task', {
            search: this.currentSearch,
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.userService.findByPosition('Manager').subscribe((pos) =>
        {
               this.managers=pos.body;
       }
       );
       
        this.vacationRequestService = new VacationRequestService(this.http, new JhiDateUtils());
        this.tasks = new Array<VacationTaskDetail>();
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInVacationTasks();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: VacationTask) {
        return item.id;
    }
    registerChangeInVacationTasks() {
        this.eventSubscriber = this.eventManager.subscribe('vacationTaskListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.vacationTasks = data;
    }
    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
