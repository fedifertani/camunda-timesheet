import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationTask } from './vacation-task.model';
import { VacationTaskPopupService } from './vacation-task-popup.service';
import { VacationTaskService } from './vacation-task.service';

@Component({
    selector: 'jhi-vacation-task-delete-dialog',
    templateUrl: './vacation-task-delete-dialog.component.html'
})
export class VacationTaskDeleteDialogComponent {

    vacationTask: VacationTask;

    constructor(
        private vacationTaskService: VacationTaskService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.vacationTaskService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'vacationTaskListModification',
                content: 'Deleted an vacationTask'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-vacation-task-delete-popup',
    template: ''
})
export class VacationTaskDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationTaskPopupService: VacationTaskPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.vacationTaskPopupService
                .open(VacationTaskDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
