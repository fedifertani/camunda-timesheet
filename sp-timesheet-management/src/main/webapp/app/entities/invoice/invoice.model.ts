import { BaseEntity } from './../../shared';
import { Project } from '../project';

export class Invoice implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public status?: string,
        public project?: Project,
    ) {
    }
}
