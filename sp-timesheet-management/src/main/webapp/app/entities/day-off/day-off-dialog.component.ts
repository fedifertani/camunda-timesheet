import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DayOff } from './day-off.model';
import { DayOffPopupService } from './day-off-popup.service';
import { DayOffService } from './day-off.service';

@Component({
    selector: 'jhi-day-off-dialog',
    templateUrl: './day-off-dialog.component.html'
})
export class DayOffDialogComponent implements OnInit {

    dayOff: DayOff;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;
    dateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private dayOffService: DayOffService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.dayOff.id !== undefined) {
            this.subscribeToSaveResponse(
                this.dayOffService.update(this.dayOff));
        } else {
            this.subscribeToSaveResponse(
                this.dayOffService.create(this.dayOff));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<DayOff>>) {
        result.subscribe((res: HttpResponse<DayOff>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: DayOff) {
        this.eventManager.broadcast({ name: 'dayOffListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-day-off-popup',
    template: ''
})
export class DayOffPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dayOffPopupService: DayOffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.dayOffPopupService
                    .open(DayOffDialogComponent as Component, params['id']);
            } else {
                this.dayOffPopupService
                    .open(DayOffDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
