import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { DayOff } from './day-off.model';
import { DayOffService } from './day-off.service';

@Component({
    selector: 'jhi-day-off-detail',
    templateUrl: './day-off-detail.component.html'
})
export class DayOffDetailComponent implements OnInit, OnDestroy {

    dayOff: DayOff;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private dayOffService: DayOffService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInDayOffs();
    }

    load(id) {
        this.dayOffService.find(id)
            .subscribe((dayOffResponse: HttpResponse<DayOff>) => {
                this.dayOff = dayOffResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInDayOffs() {
        this.eventSubscriber = this.eventManager.subscribe(
            'dayOffListModification',
            (response) => this.load(this.dayOff.id)
        );
    }
}
