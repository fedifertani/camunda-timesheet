import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DayOff } from './day-off.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<DayOff>;

@Injectable()
export class DayOffService {

    private resourceUrl =  SERVER_API_URL + 'api/day-offs';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/day-offs';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dayOff: DayOff): Observable<EntityResponseType> {
        const copy = this.convert(dayOff);
        return this.http.post<DayOff>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(dayOff: DayOff): Observable<EntityResponseType> {
        const copy = this.convert(dayOff);
        return this.http.put<DayOff>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<DayOff>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<DayOff[]>> {
        const options = createRequestOption(req);
        return this.http.get<DayOff[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DayOff[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<DayOff[]>> {
        const options = createRequestOption(req);
        return this.http.get<DayOff[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DayOff[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: DayOff = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<DayOff[]>): HttpResponse<DayOff[]> {
        const jsonResponse: DayOff[] = res.body;
        const body: DayOff[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to DayOff.
     */
    private convertItemFromServer(dayOff: DayOff): DayOff {
        const copy: DayOff = Object.assign({}, dayOff);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(dayOff.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(dayOff.updatedDate);
        copy.date = this.dateUtils
            .convertLocalDateFromServer(dayOff.date);
        return copy;
    }

    /**
     * Convert a DayOff to a JSON which can be sent to the server.
     */
    private convert(dayOff: DayOff): DayOff {
        const copy: DayOff = Object.assign({}, dayOff);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(dayOff.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(dayOff.updatedDate);
        copy.date = this.dateUtils
            .convertLocalDateToServer(dayOff.date);
        return copy;
    }
}
