import { BaseEntity } from './../../shared';

export class DayOff implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public date?: any,
        public name?: string,
        public description?: string,
    ) {
    }
}
