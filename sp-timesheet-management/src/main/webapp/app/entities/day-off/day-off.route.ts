import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { DayOffComponent } from './day-off.component';
import { DayOffDetailComponent } from './day-off-detail.component';
import { DayOffPopupComponent } from './day-off-dialog.component';
import { DayOffDeletePopupComponent } from './day-off-delete-dialog.component';

export const dayOffRoute: Routes = [
    {
        path: 'day-off',
        component: DayOffComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.dayOff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'day-off/:id',
        component: DayOffDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.dayOff.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const dayOffPopupRoute: Routes = [
    {
        path: 'day-off-new',
        component: DayOffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.dayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'day-off/:id/edit',
        component: DayOffPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.dayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'day-off/:id/delete',
        component: DayOffDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.dayOff.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
