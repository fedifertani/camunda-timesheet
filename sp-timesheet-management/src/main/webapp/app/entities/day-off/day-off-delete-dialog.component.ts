import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { DayOff } from './day-off.model';
import { DayOffPopupService } from './day-off-popup.service';
import { DayOffService } from './day-off.service';

@Component({
    selector: 'jhi-day-off-delete-dialog',
    templateUrl: './day-off-delete-dialog.component.html'
})
export class DayOffDeleteDialogComponent {

    dayOff: DayOff;

    constructor(
        private dayOffService: DayOffService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.dayOffService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'dayOffListModification',
                content: 'Deleted an dayOff'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-day-off-delete-popup',
    template: ''
})
export class DayOffDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private dayOffPopupService: DayOffPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.dayOffPopupService
                .open(DayOffDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
