import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    DayOffService,
    DayOffPopupService,
    DayOffComponent,
    DayOffDetailComponent,
    DayOffDialogComponent,
    DayOffPopupComponent,
    DayOffDeletePopupComponent,
    DayOffDeleteDialogComponent,
    dayOffRoute,
    dayOffPopupRoute,
} from './';

const ENTITY_STATES = [
    ...dayOffRoute,
    ...dayOffPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DayOffComponent,
        DayOffDetailComponent,
        DayOffDialogComponent,
        DayOffDeleteDialogComponent,
        DayOffPopupComponent,
        DayOffDeletePopupComponent,
    ],
    entryComponents: [
        DayOffComponent,
        DayOffDialogComponent,
        DayOffPopupComponent,
        DayOffDeleteDialogComponent,
        DayOffDeletePopupComponent,
    ],
    providers: [
        DayOffService,
        DayOffPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetDayOffModule {}
