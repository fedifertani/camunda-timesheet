import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DayOff } from './day-off.model';
import { DayOffService } from './day-off.service';

@Injectable()
export class DayOffPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private dayOffService: DayOffService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.dayOffService.find(id)
                    .subscribe((dayOffResponse: HttpResponse<DayOff>) => {
                        const dayOff: DayOff = dayOffResponse.body;
                        if (dayOff.createdDate) {
                            dayOff.createdDate = {
                                year: dayOff.createdDate.getFullYear(),
                                month: dayOff.createdDate.getMonth() + 1,
                                day: dayOff.createdDate.getDate()
                            };
                        }
                        if (dayOff.updatedDate) {
                            dayOff.updatedDate = {
                                year: dayOff.updatedDate.getFullYear(),
                                month: dayOff.updatedDate.getMonth() + 1,
                                day: dayOff.updatedDate.getDate()
                            };
                        }
                        if (dayOff.date) {
                            dayOff.date = {
                                year: dayOff.date.getFullYear(),
                                month: dayOff.date.getMonth() + 1,
                                day: dayOff.date.getDate()
                            };
                        }
                        this.ngbModalRef = this.dayOffModalRef(component, dayOff);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.dayOffModalRef(component, new DayOff());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    dayOffModalRef(component: Component, dayOff: DayOff): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.dayOff = dayOff;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
