import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { VacationTypeComponent } from './vacation-type.component';
import { VacationTypeDetailComponent } from './vacation-type-detail.component';
import { VacationTypePopupComponent } from './vacation-type-dialog.component';
import { VacationTypeDeletePopupComponent } from './vacation-type-delete-dialog.component';

export const vacationTypeRoute: Routes = [
    {
        path: 'vacation-type',
        component: VacationTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'vacation-type/:id',
        component: VacationTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const vacationTypePopupRoute: Routes = [
    {
        path: 'vacation-type-new',
        component: VacationTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-type/:id/edit',
        component: VacationTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'vacation-type/:id/delete',
        component: VacationTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.vacationType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
