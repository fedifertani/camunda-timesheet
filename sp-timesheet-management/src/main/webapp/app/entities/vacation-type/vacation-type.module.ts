import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    VacationTypeService,
    VacationTypePopupService,
    VacationTypeComponent,
    VacationTypeDetailComponent,
    VacationTypeDialogComponent,
    VacationTypePopupComponent,
    VacationTypeDeletePopupComponent,
    VacationTypeDeleteDialogComponent,
    vacationTypeRoute,
    vacationTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...vacationTypeRoute,
    ...vacationTypePopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        VacationTypeComponent,
        VacationTypeDetailComponent,
        VacationTypeDialogComponent,
        VacationTypeDeleteDialogComponent,
        VacationTypePopupComponent,
        VacationTypeDeletePopupComponent,
    ],
    entryComponents: [
        VacationTypeComponent,
        VacationTypeDialogComponent,
        VacationTypePopupComponent,
        VacationTypeDeleteDialogComponent,
        VacationTypeDeletePopupComponent,
    ],
    providers: [
        VacationTypeService,
        VacationTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetVacationTypeModule {}
