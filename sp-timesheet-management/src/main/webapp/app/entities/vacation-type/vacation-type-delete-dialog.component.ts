import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationType } from './vacation-type.model';
import { VacationTypePopupService } from './vacation-type-popup.service';
import { VacationTypeService } from './vacation-type.service';

@Component({
    selector: 'jhi-vacation-type-delete-dialog',
    templateUrl: './vacation-type-delete-dialog.component.html'
})
export class VacationTypeDeleteDialogComponent {

    vacationType: VacationType;

    constructor(
        private vacationTypeService: VacationTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.vacationTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'vacationTypeListModification',
                content: 'Deleted an vacationType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-vacation-type-delete-popup',
    template: ''
})
export class VacationTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationTypePopupService: VacationTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.vacationTypePopupService
                .open(VacationTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
