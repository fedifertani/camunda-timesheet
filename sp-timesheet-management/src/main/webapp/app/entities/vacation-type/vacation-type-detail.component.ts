import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { VacationType } from './vacation-type.model';
import { VacationTypeService } from './vacation-type.service';

@Component({
    selector: 'jhi-vacation-type-detail',
    templateUrl: './vacation-type-detail.component.html'
})
export class VacationTypeDetailComponent implements OnInit, OnDestroy {

    vacationType: VacationType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private vacationTypeService: VacationTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInVacationTypes();
    }

    load(id) {
        this.vacationTypeService.find(id)
            .subscribe((vacationTypeResponse: HttpResponse<VacationType>) => {
                this.vacationType = vacationTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInVacationTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'vacationTypeListModification',
            (response) => this.load(this.vacationType.id)
        );
    }
}
