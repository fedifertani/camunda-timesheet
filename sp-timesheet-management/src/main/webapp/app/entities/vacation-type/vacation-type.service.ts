import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { VacationType } from './vacation-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<VacationType>;

@Injectable()
export class VacationTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/vacation-types';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/vacation-types';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(vacationType: VacationType): Observable<EntityResponseType> {
        const copy = this.convert(vacationType);
        return this.http.post<VacationType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(vacationType: VacationType): Observable<EntityResponseType> {
        const copy = this.convert(vacationType);
        return this.http.put<VacationType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<VacationType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<VacationType[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationType[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<VacationType[]>> {
        const options = createRequestOption(req);
        return this.http.get<VacationType[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<VacationType[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: VacationType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<VacationType[]>): HttpResponse<VacationType[]> {
        const jsonResponse: VacationType[] = res.body;
        const body: VacationType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to VacationType.
     */
    private convertItemFromServer(vacationType: VacationType): VacationType {
        const copy: VacationType = Object.assign({}, vacationType);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(vacationType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(vacationType.updatedDate);
        return copy;
    }

    /**
     * Convert a VacationType to a JSON which can be sent to the server.
     */
    private convert(vacationType: VacationType): VacationType {
        const copy: VacationType = Object.assign({}, vacationType);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(vacationType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(vacationType.updatedDate);
        return copy;
    }
}
