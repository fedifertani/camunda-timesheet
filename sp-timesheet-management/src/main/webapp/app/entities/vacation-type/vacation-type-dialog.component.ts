import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { VacationType } from './vacation-type.model';
import { VacationTypePopupService } from './vacation-type-popup.service';
import { VacationTypeService } from './vacation-type.service';

@Component({
    selector: 'jhi-vacation-type-dialog',
    templateUrl: './vacation-type-dialog.component.html'
})
export class VacationTypeDialogComponent implements OnInit {

    vacationType: VacationType;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private vacationTypeService: VacationTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.vacationType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.vacationTypeService.update(this.vacationType));
        } else {
            this.subscribeToSaveResponse(
                this.vacationTypeService.create(this.vacationType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<VacationType>>) {
        result.subscribe((res: HttpResponse<VacationType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: VacationType) {
        this.eventManager.broadcast({ name: 'vacationTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-vacation-type-popup',
    template: ''
})
export class VacationTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private vacationTypePopupService: VacationTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.vacationTypePopupService
                    .open(VacationTypeDialogComponent as Component, params['id']);
            } else {
                this.vacationTypePopupService
                    .open(VacationTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
