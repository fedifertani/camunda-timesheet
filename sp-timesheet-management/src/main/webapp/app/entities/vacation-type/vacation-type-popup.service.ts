import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { VacationType } from './vacation-type.model';
import { VacationTypeService } from './vacation-type.service';

@Injectable()
export class VacationTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private vacationTypeService: VacationTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.vacationTypeService.find(id)
                    .subscribe((vacationTypeResponse: HttpResponse<VacationType>) => {
                        const vacationType: VacationType = vacationTypeResponse.body;
                        if (vacationType.createdDate) {
                            vacationType.createdDate = {
                                year: vacationType.createdDate.getFullYear(),
                                month: vacationType.createdDate.getMonth() + 1,
                                day: vacationType.createdDate.getDate()
                            };
                        }
                        if (vacationType.updatedDate) {
                            vacationType.updatedDate = {
                                year: vacationType.updatedDate.getFullYear(),
                                month: vacationType.updatedDate.getMonth() + 1,
                                day: vacationType.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.vacationTypeModalRef(component, vacationType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.vacationTypeModalRef(component, new VacationType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    vacationTypeModalRef(component: Component, vacationType: VacationType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.vacationType = vacationType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
