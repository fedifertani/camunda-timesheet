import { BaseEntity } from './../../shared';
import { CustomerContactType } from '../customer-contact-type';

export class CustomerContact implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phone1?: string,
        public phone2?: string,
        public type?: CustomerContactType,
    ) {
    }
}
