import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CustomerContact } from './customer-contact.model';
import { CustomerContactService } from './customer-contact.service';

@Component({
    selector: 'jhi-customer-contact-detail',
    templateUrl: './customer-contact-detail.component.html'
})
export class CustomerContactDetailComponent implements OnInit, OnDestroy {

    customerContact: CustomerContact;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private customerContactService: CustomerContactService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCustomerContacts();
    }

    load(id) {
        this.customerContactService.find(id)
            .subscribe((customerContactResponse: HttpResponse<CustomerContact>) => {
                this.customerContact = customerContactResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCustomerContacts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'customerContactListModification',
            (response) => this.load(this.customerContact.id)
        );
    }
}
