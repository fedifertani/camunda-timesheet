import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CustomerContactComponent } from './customer-contact.component';
import { CustomerContactDetailComponent } from './customer-contact-detail.component';
import { CustomerContactPopupComponent } from './customer-contact-dialog.component';
import { CustomerContactDeletePopupComponent } from './customer-contact-delete-dialog.component';

export const customerContactRoute: Routes = [
    {
        path: 'customer-contact',
        component: CustomerContactComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContact.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'customer-contact/:id',
        component: CustomerContactDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContact.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const customerContactPopupRoute: Routes = [
    {
        path: 'customer-contact-new',
        component: CustomerContactPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContact.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-contact/:id/edit',
        component: CustomerContactPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContact.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-contact/:id/delete',
        component: CustomerContactDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContact.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
