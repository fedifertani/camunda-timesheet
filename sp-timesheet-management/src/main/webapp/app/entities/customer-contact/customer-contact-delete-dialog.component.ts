import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CustomerContact } from './customer-contact.model';
import { CustomerContactPopupService } from './customer-contact-popup.service';
import { CustomerContactService } from './customer-contact.service';

@Component({
    selector: 'jhi-customer-contact-delete-dialog',
    templateUrl: './customer-contact-delete-dialog.component.html'
})
export class CustomerContactDeleteDialogComponent {

    customerContact: CustomerContact;

    constructor(
        private customerContactService: CustomerContactService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.customerContactService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'customerContactListModification',
                content: 'Deleted an customerContact'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-customer-contact-delete-popup',
    template: ''
})
export class CustomerContactDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private customerContactPopupService: CustomerContactPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.customerContactPopupService
                .open(CustomerContactDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
