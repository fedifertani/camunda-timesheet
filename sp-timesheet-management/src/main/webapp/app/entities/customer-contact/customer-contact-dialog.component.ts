import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CustomerContact } from './customer-contact.model';
import { CustomerContactPopupService } from './customer-contact-popup.service';
import { CustomerContactService } from './customer-contact.service';
import { CustomerContactType, CustomerContactTypeService } from '../customer-contact-type';

@Component({
    selector: 'jhi-customer-contact-dialog',
    templateUrl: './customer-contact-dialog.component.html'
})
export class CustomerContactDialogComponent implements OnInit {

    customerContact: CustomerContact;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;
    currentSearch: string;
    types: CustomerContactType[];

    constructor(
        public activeModal: NgbActiveModal,
        private customerContactService: CustomerContactService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private customerContactTypeService: CustomerContactTypeService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.customerContactTypeService.query().subscribe(
            (res: HttpResponse<CustomerContactType[]>) => {
                this.types = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.customerContact.id !== undefined) {
            this.subscribeToSaveResponse(
                this.customerContactService.update(this.customerContact));
        } else {
            this.subscribeToSaveResponse(
                this.customerContactService.create(this.customerContact));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CustomerContact>>) {
        result.subscribe((res: HttpResponse<CustomerContact>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CustomerContact) {
        this.eventManager.broadcast({ name: 'customerContactListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-customer-contact-popup',
    template: ''
})
export class CustomerContactPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private customerContactPopupService: CustomerContactPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.customerContactPopupService
                    .open(CustomerContactDialogComponent as Component, params['id']);
            } else {
                this.customerContactPopupService
                    .open(CustomerContactDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
