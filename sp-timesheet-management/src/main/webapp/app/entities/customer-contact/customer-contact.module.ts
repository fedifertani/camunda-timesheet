import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    CustomerContactService,
    CustomerContactPopupService,
    CustomerContactComponent,
    CustomerContactDetailComponent,
    CustomerContactDialogComponent,
    CustomerContactPopupComponent,
    CustomerContactDeletePopupComponent,
    CustomerContactDeleteDialogComponent,
    customerContactRoute,
    customerContactPopupRoute,
} from './';

const ENTITY_STATES = [
    ...customerContactRoute,
    ...customerContactPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CustomerContactComponent,
        CustomerContactDetailComponent,
        CustomerContactDialogComponent,
        CustomerContactDeleteDialogComponent,
        CustomerContactPopupComponent,
        CustomerContactDeletePopupComponent,
    ],
    entryComponents: [
        CustomerContactComponent,
        CustomerContactDialogComponent,
        CustomerContactPopupComponent,
        CustomerContactDeleteDialogComponent,
        CustomerContactDeletePopupComponent,
    ],
    providers: [
        CustomerContactService,
        CustomerContactPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetCustomerContactModule {}
