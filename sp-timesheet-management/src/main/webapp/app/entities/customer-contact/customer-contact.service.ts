import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CustomerContact } from './customer-contact.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CustomerContact>;

@Injectable()
export class CustomerContactService {

    private resourceUrl =  SERVER_API_URL + 'api/customer-contacts';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/customer-contacts';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(customerContact: CustomerContact): Observable<EntityResponseType> {
        const copy = this.convert(customerContact);
        return this.http.post<CustomerContact>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(customerContact: CustomerContact): Observable<EntityResponseType> {
        const copy = this.convert(customerContact);
        return this.http.put<CustomerContact>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<CustomerContact>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CustomerContact[]>> {
        const options = createRequestOption(req);
        return this.http.get<CustomerContact[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CustomerContact[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CustomerContact[]>> {
        const options = createRequestOption(req);
        return this.http.get<CustomerContact[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CustomerContact[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CustomerContact = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CustomerContact[]>): HttpResponse<CustomerContact[]> {
        const jsonResponse: CustomerContact[] = res.body;
        const body: CustomerContact[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CustomerContact.
     */
    private convertItemFromServer(customerContact: CustomerContact): CustomerContact {
        const copy: CustomerContact = Object.assign({}, customerContact);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(customerContact.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(customerContact.updatedDate);
        return copy;
    }

    /**
     * Convert a CustomerContact to a JSON which can be sent to the server.
     */
    private convert(customerContact: CustomerContact): CustomerContact {
        const copy: CustomerContact = Object.assign({}, customerContact);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(customerContact.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(customerContact.updatedDate);
        return copy;
    }
}
