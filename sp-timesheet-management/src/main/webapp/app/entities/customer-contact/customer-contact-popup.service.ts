import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CustomerContact } from './customer-contact.model';
import { CustomerContactService } from './customer-contact.service';

@Injectable()
export class CustomerContactPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private customerContactService: CustomerContactService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.customerContactService.find(id)
                    .subscribe((customerContactResponse: HttpResponse<CustomerContact>) => {
                        const customerContact: CustomerContact = customerContactResponse.body;
                        if (customerContact.createdDate) {
                            customerContact.createdDate = {
                                year: customerContact.createdDate.getFullYear(),
                                month: customerContact.createdDate.getMonth() + 1,
                                day: customerContact.createdDate.getDate()
                            };
                        }
                        if (customerContact.updatedDate) {
                            customerContact.updatedDate = {
                                year: customerContact.updatedDate.getFullYear(),
                                month: customerContact.updatedDate.getMonth() + 1,
                                day: customerContact.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.customerContactModalRef(component, customerContact);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.customerContactModalRef(component, new CustomerContact());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    customerContactModalRef(component: Component, customerContact: CustomerContact): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.customerContact = customerContact;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
