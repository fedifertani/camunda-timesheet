import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { CustomerContactType } from './customer-contact-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<CustomerContactType>;

@Injectable()
export class CustomerContactTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/customer-contact-types';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/customer-contact-types';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(customerContactType: CustomerContactType): Observable<EntityResponseType> {
        const copy = this.convert(customerContactType);
        return this.http.post<CustomerContactType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(customerContactType: CustomerContactType): Observable<EntityResponseType> {
        const copy = this.convert(customerContactType);
        return this.http.put<CustomerContactType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<CustomerContactType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<CustomerContactType[]>> {
        const options = createRequestOption(req);
        return this.http.get<CustomerContactType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CustomerContactType[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<CustomerContactType[]>> {
        const options = createRequestOption(req);
        return this.http.get<CustomerContactType[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<CustomerContactType[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: CustomerContactType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<CustomerContactType[]>): HttpResponse<CustomerContactType[]> {
        const jsonResponse: CustomerContactType[] = res.body;
        const body: CustomerContactType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to CustomerContactType.
     */
    private convertItemFromServer(customerContactType: CustomerContactType): CustomerContactType {
        const copy: CustomerContactType = Object.assign({}, customerContactType);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(customerContactType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(customerContactType.updatedDate);
        return copy;
    }

    /**
     * Convert a CustomerContactType to a JSON which can be sent to the server.
     */
    private convert(customerContactType: CustomerContactType): CustomerContactType {
        const copy: CustomerContactType = Object.assign({}, customerContactType);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(customerContactType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(customerContactType.updatedDate);
        return copy;
    }
}
