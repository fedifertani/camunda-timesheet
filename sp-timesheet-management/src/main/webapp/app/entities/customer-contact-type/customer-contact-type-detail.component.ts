import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { CustomerContactType } from './customer-contact-type.model';
import { CustomerContactTypeService } from './customer-contact-type.service';

@Component({
    selector: 'jhi-customer-contact-type-detail',
    templateUrl: './customer-contact-type-detail.component.html'
})
export class CustomerContactTypeDetailComponent implements OnInit, OnDestroy {

    customerContactType: CustomerContactType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private customerContactTypeService: CustomerContactTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInCustomerContactTypes();
    }

    load(id) {
        this.customerContactTypeService.find(id)
            .subscribe((customerContactTypeResponse: HttpResponse<CustomerContactType>) => {
                this.customerContactType = customerContactTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInCustomerContactTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'customerContactTypeListModification',
            (response) => this.load(this.customerContactType.id)
        );
    }
}
