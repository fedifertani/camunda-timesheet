import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { CustomerContactType } from './customer-contact-type.model';
import { CustomerContactTypeService } from './customer-contact-type.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-customer-contact-type',
    templateUrl: './customer-contact-type.component.html'
})
export class CustomerContactTypeComponent implements OnInit, OnDestroy {
customerContactTypes: CustomerContactType[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private customerContactTypeService: CustomerContactTypeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.customerContactTypeService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<CustomerContactType[]>) => this.customerContactTypes = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.customerContactTypeService.query().subscribe(
            (res: HttpResponse<CustomerContactType[]>) => {
                this.customerContactTypes = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInCustomerContactTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: CustomerContactType) {
        return item.id;
    }
    registerChangeInCustomerContactTypes() {
        this.eventSubscriber = this.eventManager.subscribe('customerContactTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
