import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CustomerContactType } from './customer-contact-type.model';
import { CustomerContactTypePopupService } from './customer-contact-type-popup.service';
import { CustomerContactTypeService } from './customer-contact-type.service';

@Component({
    selector: 'jhi-customer-contact-type-dialog',
    templateUrl: './customer-contact-type-dialog.component.html'
})
export class CustomerContactTypeDialogComponent implements OnInit {

    customerContactType: CustomerContactType;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private customerContactTypeService: CustomerContactTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.customerContactType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.customerContactTypeService.update(this.customerContactType));
        } else {
            this.subscribeToSaveResponse(
                this.customerContactTypeService.create(this.customerContactType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<CustomerContactType>>) {
        result.subscribe((res: HttpResponse<CustomerContactType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: CustomerContactType) {
        this.eventManager.broadcast({ name: 'customerContactTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-customer-contact-type-popup',
    template: ''
})
export class CustomerContactTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private customerContactTypePopupService: CustomerContactTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.customerContactTypePopupService
                    .open(CustomerContactTypeDialogComponent as Component, params['id']);
            } else {
                this.customerContactTypePopupService
                    .open(CustomerContactTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
