import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    CustomerContactTypeService,
    CustomerContactTypePopupService,
    CustomerContactTypeComponent,
    CustomerContactTypeDetailComponent,
    CustomerContactTypeDialogComponent,
    CustomerContactTypePopupComponent,
    CustomerContactTypeDeletePopupComponent,
    CustomerContactTypeDeleteDialogComponent,
    customerContactTypeRoute,
    customerContactTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...customerContactTypeRoute,
    ...customerContactTypePopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CustomerContactTypeComponent,
        CustomerContactTypeDetailComponent,
        CustomerContactTypeDialogComponent,
        CustomerContactTypeDeleteDialogComponent,
        CustomerContactTypePopupComponent,
        CustomerContactTypeDeletePopupComponent,
    ],
    entryComponents: [
        CustomerContactTypeComponent,
        CustomerContactTypeDialogComponent,
        CustomerContactTypePopupComponent,
        CustomerContactTypeDeleteDialogComponent,
        CustomerContactTypeDeletePopupComponent,
    ],
    providers: [
        CustomerContactTypeService,
        CustomerContactTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetCustomerContactTypeModule {}
