import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { CustomerContactTypeComponent } from './customer-contact-type.component';
import { CustomerContactTypeDetailComponent } from './customer-contact-type-detail.component';
import { CustomerContactTypePopupComponent } from './customer-contact-type-dialog.component';
import { CustomerContactTypeDeletePopupComponent } from './customer-contact-type-delete-dialog.component';

export const customerContactTypeRoute: Routes = [
    {
        path: 'customer-contact-type',
        component: CustomerContactTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContactType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'customer-contact-type/:id',
        component: CustomerContactTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContactType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const customerContactTypePopupRoute: Routes = [
    {
        path: 'customer-contact-type-new',
        component: CustomerContactTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContactType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-contact-type/:id/edit',
        component: CustomerContactTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContactType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'customer-contact-type/:id/delete',
        component: CustomerContactTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.customerContactType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
