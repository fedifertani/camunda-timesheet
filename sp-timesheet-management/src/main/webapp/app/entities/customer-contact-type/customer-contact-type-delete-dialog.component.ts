import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { CustomerContactType } from './customer-contact-type.model';
import { CustomerContactTypePopupService } from './customer-contact-type-popup.service';
import { CustomerContactTypeService } from './customer-contact-type.service';

@Component({
    selector: 'jhi-customer-contact-type-delete-dialog',
    templateUrl: './customer-contact-type-delete-dialog.component.html'
})
export class CustomerContactTypeDeleteDialogComponent {

    customerContactType: CustomerContactType;

    constructor(
        private customerContactTypeService: CustomerContactTypeService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.customerContactTypeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'customerContactTypeListModification',
                content: 'Deleted an customerContactType'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-customer-contact-type-delete-popup',
    template: ''
})
export class CustomerContactTypeDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private customerContactTypePopupService: CustomerContactTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.customerContactTypePopupService
                .open(CustomerContactTypeDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
