import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { CustomerContactType } from './customer-contact-type.model';
import { CustomerContactTypeService } from './customer-contact-type.service';

@Injectable()
export class CustomerContactTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private customerContactTypeService: CustomerContactTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.customerContactTypeService.find(id)
                    .subscribe((customerContactTypeResponse: HttpResponse<CustomerContactType>) => {
                        const customerContactType: CustomerContactType = customerContactTypeResponse.body;
                        if (customerContactType.createdDate) {
                            customerContactType.createdDate = {
                                year: customerContactType.createdDate.getFullYear(),
                                month: customerContactType.createdDate.getMonth() + 1,
                                day: customerContactType.createdDate.getDate()
                            };
                        }
                        if (customerContactType.updatedDate) {
                            customerContactType.updatedDate = {
                                year: customerContactType.updatedDate.getFullYear(),
                                month: customerContactType.updatedDate.getMonth() + 1,
                                day: customerContactType.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.customerContactTypeModalRef(component, customerContactType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.customerContactTypeModalRef(component, new CustomerContactType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    customerContactTypeModalRef(component: Component, customerContactType: CustomerContactType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.customerContactType = customerContactType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
