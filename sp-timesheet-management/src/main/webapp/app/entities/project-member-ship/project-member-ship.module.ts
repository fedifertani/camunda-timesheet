import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    ProjectMemberShipService,
    ProjectMemberShipPopupService,
    ProjectMemberShipComponent,
    ProjectMemberShipDetailComponent,
    ProjectMemberShipDialogComponent,
    ProjectMemberShipPopupComponent,
    ProjectMemberShipDeletePopupComponent,
    ProjectMemberShipDeleteDialogComponent,
    projectMemberShipRoute,
    projectMemberShipPopupRoute,
} from './';

const ENTITY_STATES = [
    ...projectMemberShipRoute,
    ...projectMemberShipPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        ProjectMemberShipComponent,
        ProjectMemberShipDetailComponent,
        ProjectMemberShipDialogComponent,
        ProjectMemberShipDeleteDialogComponent,
        ProjectMemberShipPopupComponent,
        ProjectMemberShipDeletePopupComponent,
    ],
    entryComponents: [
        ProjectMemberShipComponent,
        ProjectMemberShipDialogComponent,
        ProjectMemberShipPopupComponent,
        ProjectMemberShipDeleteDialogComponent,
        ProjectMemberShipDeletePopupComponent,
    ],
    providers: [
        ProjectMemberShipService,
        ProjectMemberShipPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetProjectMemberShipModule {}
