import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectMemberShip } from './project-member-ship.model';
import { ProjectMemberShipPopupService } from './project-member-ship-popup.service';
import { ProjectMemberShipService } from './project-member-ship.service';

@Component({
    selector: 'jhi-project-member-ship-delete-dialog',
    templateUrl: './project-member-ship-delete-dialog.component.html'
})
export class ProjectMemberShipDeleteDialogComponent {

    projectMemberShip: ProjectMemberShip;

    constructor(
        private projectMemberShipService: ProjectMemberShipService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.projectMemberShipService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'projectMemberShipListModification',
                content: 'Deleted an projectMemberShip'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-project-member-ship-delete-popup',
    template: ''
})
export class ProjectMemberShipDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private projectMemberShipPopupService: ProjectMemberShipPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.projectMemberShipPopupService
                .open(ProjectMemberShipDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
