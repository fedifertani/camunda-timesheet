import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { ProjectMemberShip } from './project-member-ship.model';
import { ProjectMemberShipService } from './project-member-ship.service';

@Injectable()
export class ProjectMemberShipPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private projectMemberShipService: ProjectMemberShipService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.projectMemberShipService.find(id)
                    .subscribe((projectMemberShipResponse: HttpResponse<ProjectMemberShip>) => {
                        const projectMemberShip: ProjectMemberShip = projectMemberShipResponse.body;
                        if (projectMemberShip.createdDate) {
                            projectMemberShip.createdDate = {
                                year: projectMemberShip.createdDate.getFullYear(),
                                month: projectMemberShip.createdDate.getMonth() + 1,
                                day: projectMemberShip.createdDate.getDate()
                            };
                        }
                        if (projectMemberShip.updatedDate) {
                            projectMemberShip.updatedDate = {
                                year: projectMemberShip.updatedDate.getFullYear(),
                                month: projectMemberShip.updatedDate.getMonth() + 1,
                                day: projectMemberShip.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.projectMemberShipModalRef(component, projectMemberShip);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.projectMemberShipModalRef(component, new ProjectMemberShip());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    projectMemberShipModalRef(component: Component, projectMemberShip: ProjectMemberShip): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.projectMemberShip = projectMemberShip;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
