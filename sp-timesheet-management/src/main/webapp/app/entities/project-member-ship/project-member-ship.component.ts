import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProjectMemberShip } from './project-member-ship.model';
import { ProjectMemberShipService } from './project-member-ship.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-project-member-ship',
    templateUrl: './project-member-ship.component.html'
})
export class ProjectMemberShipComponent implements OnInit, OnDestroy {
projectMemberShips: ProjectMemberShip[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private projectMemberShipService: ProjectMemberShipService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal

    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.projectMemberShipService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<ProjectMemberShip[]>) => this.projectMemberShips = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.projectMemberShipService.query().subscribe(
            (res: HttpResponse<ProjectMemberShip[]>) => {
                this.projectMemberShips = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInProjectMemberShips();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: ProjectMemberShip) {
        return item.id;
    }
    registerChangeInProjectMemberShips() {
        this.eventSubscriber = this.eventManager.subscribe('projectMemberShipListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
