import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { ProjectMemberShip } from './project-member-ship.model';
import { ProjectMemberShipService } from './project-member-ship.service';

@Component({
    selector: 'jhi-project-member-ship-detail',
    templateUrl: './project-member-ship-detail.component.html'
})
export class ProjectMemberShipDetailComponent implements OnInit, OnDestroy {

    projectMemberShip: ProjectMemberShip;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private projectMemberShipService: ProjectMemberShipService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInProjectMemberShips();
    }

    load(id) {
        this.projectMemberShipService.find(id)
            .subscribe((projectMemberShipResponse: HttpResponse<ProjectMemberShip>) => {
                this.projectMemberShip = projectMemberShipResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInProjectMemberShips() {
        this.eventSubscriber = this.eventManager.subscribe(
            'projectMemberShipListModification',
            (response) => this.load(this.projectMemberShip.id)
        );
    }
}
