import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { ProjectMemberShipComponent } from './project-member-ship.component';
import { ProjectMemberShipDetailComponent } from './project-member-ship-detail.component';
import { ProjectMemberShipPopupComponent } from './project-member-ship-dialog.component';
import { ProjectMemberShipDeletePopupComponent } from './project-member-ship-delete-dialog.component';

export const projectMemberShipRoute: Routes = [
    {
        path: 'project-member-ship',
        component: ProjectMemberShipComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.projectMemberShip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'project-member-ship/:id',
        component: ProjectMemberShipDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.projectMemberShip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const projectMemberShipPopupRoute: Routes = [
    {
        path: 'project-member-ship-new',
        component: ProjectMemberShipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.projectMemberShip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'project-member-ship/:id/edit',
        component: ProjectMemberShipPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.projectMemberShip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'project-member-ship/:id/delete',
        component: ProjectMemberShipDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.projectMemberShip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
