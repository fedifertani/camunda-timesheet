import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ProjectMemberShip } from './project-member-ship.model';
import { ProjectMemberShipPopupService } from './project-member-ship-popup.service';
import { ProjectMemberShipService } from './project-member-ship.service';
import { User, UserService } from '../../shared';
import { UserPosition, UserPositionService } from '../user-position';
import { Project } from '../project/project.model';
import { ProjectService } from '../project/project.service';

@Component({
    selector: 'jhi-project-member-ship-dialog',
    templateUrl: './project-member-ship-dialog.component.html'
})
export class ProjectMemberShipDialogComponent implements OnInit {

    projectMemberShip: ProjectMemberShip;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;
    currentSearch: string;
    users: User[];
    userPositions: UserPosition[];
    projects: Project[];

    constructor(
        public activeModal: NgbActiveModal,
        private projectMemberShipService: ProjectMemberShipService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private userService: UserService,
        private userPositionService: UserPositionService,
        private projectService: ProjectService

    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.userService.query().subscribe(
            (res: HttpResponse<User[]>) => {
                this.users = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userPositionService.query().subscribe(
            (res: HttpResponse<UserPosition[]>) => {
                this.userPositions = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.projectService.query().subscribe(
            (res: HttpResponse<Project[]>) => {
                this.projects = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.projectMemberShip.id !== undefined) {
            this.subscribeToSaveResponse(
                this.projectMemberShipService.update(this.projectMemberShip));
        } else {
            this.subscribeToSaveResponse(
                this.projectMemberShipService.create(this.projectMemberShip));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ProjectMemberShip>>) {
        result.subscribe((res: HttpResponse<ProjectMemberShip>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: ProjectMemberShip) {
        this.eventManager.broadcast({ name: 'projectMemberShipListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-project-member-ship-popup',
    template: ''
})
export class ProjectMemberShipPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private projectMemberShipPopupService: ProjectMemberShipPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.projectMemberShipPopupService
                    .open(ProjectMemberShipDialogComponent as Component, params['id']);
            } else {
                this.projectMemberShipPopupService
                    .open(ProjectMemberShipDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
