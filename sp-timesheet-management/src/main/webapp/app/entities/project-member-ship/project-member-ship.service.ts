import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { ProjectMemberShip } from './project-member-ship.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<ProjectMemberShip>;

@Injectable()
export class ProjectMemberShipService {

    private resourceUrl =  SERVER_API_URL + 'api/project-member-ships';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/project-member-ships';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(projectMemberShip: ProjectMemberShip): Observable<EntityResponseType> {
        const copy = this.convert(projectMemberShip);
        return this.http.post<ProjectMemberShip>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(projectMemberShip: ProjectMemberShip): Observable<EntityResponseType> {
        const copy = this.convert(projectMemberShip);
        return this.http.put<ProjectMemberShip>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<ProjectMemberShip>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<ProjectMemberShip[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProjectMemberShip[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProjectMemberShip[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<ProjectMemberShip[]>> {
        const options = createRequestOption(req);
        return this.http.get<ProjectMemberShip[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<ProjectMemberShip[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: ProjectMemberShip = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<ProjectMemberShip[]>): HttpResponse<ProjectMemberShip[]> {
        const jsonResponse: ProjectMemberShip[] = res.body;
        const body: ProjectMemberShip[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to ProjectMemberShip.
     */
    private convertItemFromServer(projectMemberShip: ProjectMemberShip): ProjectMemberShip {
        const copy: ProjectMemberShip = Object.assign({}, projectMemberShip);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(projectMemberShip.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(projectMemberShip.updatedDate);
        return copy;
    }

    /**
     * Convert a ProjectMemberShip to a JSON which can be sent to the server.
     */
    private convert(projectMemberShip: ProjectMemberShip): ProjectMemberShip {
        const copy: ProjectMemberShip = Object.assign({}, projectMemberShip);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(projectMemberShip.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(projectMemberShip.updatedDate);
        return copy;
    }
}
