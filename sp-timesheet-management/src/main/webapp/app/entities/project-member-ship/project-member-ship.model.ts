import { BaseEntity, User } from './../../shared';
import { Project } from '../project/project.model';
import { UserPosition } from '../user-position';

export class ProjectMemberShip implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public rate?: number,
        public user?: User,
        public userPosition?: UserPosition,
        public project?: Project,
    ) {
    }
}
