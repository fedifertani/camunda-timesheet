import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { AddressType } from './address-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<AddressType>;

@Injectable()
export class AddressTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/address-types';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/address-types';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(addressType: AddressType): Observable<EntityResponseType> {
        const copy = this.convert(addressType);
        return this.http.post<AddressType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(addressType: AddressType): Observable<EntityResponseType> {
        const copy = this.convert(addressType);
        return this.http.put<AddressType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<AddressType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<AddressType[]>> {
        const options = createRequestOption(req);
        return this.http.get<AddressType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AddressType[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<AddressType[]>> {
        const options = createRequestOption(req);
        return this.http.get<AddressType[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<AddressType[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: AddressType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<AddressType[]>): HttpResponse<AddressType[]> {
        const jsonResponse: AddressType[] = res.body;
        const body: AddressType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to AddressType.
     */
    private convertItemFromServer(addressType: AddressType): AddressType {
        const copy: AddressType = Object.assign({}, addressType);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(addressType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(addressType.updatedDate);
        return copy;
    }

    /**
     * Convert a AddressType to a JSON which can be sent to the server.
     */
    private convert(addressType: AddressType): AddressType {
        const copy: AddressType = Object.assign({}, addressType);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(addressType.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(addressType.updatedDate);
        return copy;
    }
}
