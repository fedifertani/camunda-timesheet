import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { AddressType } from './address-type.model';
import { AddressTypeService } from './address-type.service';

@Injectable()
export class AddressTypePopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private addressTypeService: AddressTypeService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.addressTypeService.find(id)
                    .subscribe((addressTypeResponse: HttpResponse<AddressType>) => {
                        const addressType: AddressType = addressTypeResponse.body;
                        if (addressType.createdDate) {
                            addressType.createdDate = {
                                year: addressType.createdDate.getFullYear(),
                                month: addressType.createdDate.getMonth() + 1,
                                day: addressType.createdDate.getDate()
                            };
                        }
                        if (addressType.updatedDate) {
                            addressType.updatedDate = {
                                year: addressType.updatedDate.getFullYear(),
                                month: addressType.updatedDate.getMonth() + 1,
                                day: addressType.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.addressTypeModalRef(component, addressType);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.addressTypeModalRef(component, new AddressType());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    addressTypeModalRef(component: Component, addressType: AddressType): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.addressType = addressType;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
