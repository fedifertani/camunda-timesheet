import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { AddressType } from './address-type.model';
import { AddressTypePopupService } from './address-type-popup.service';
import { AddressTypeService } from './address-type.service';

@Component({
    selector: 'jhi-address-type-dialog',
    templateUrl: './address-type-dialog.component.html'
})
export class AddressTypeDialogComponent implements OnInit {

    addressType: AddressType;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private addressTypeService: AddressTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.addressType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.addressTypeService.update(this.addressType));
        } else {
            this.subscribeToSaveResponse(
                this.addressTypeService.create(this.addressType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<AddressType>>) {
        result.subscribe((res: HttpResponse<AddressType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: AddressType) {
        this.eventManager.broadcast({ name: 'addressTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-address-type-popup',
    template: ''
})
export class AddressTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private addressTypePopupService: AddressTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.addressTypePopupService
                    .open(AddressTypeDialogComponent as Component, params['id']);
            } else {
                this.addressTypePopupService
                    .open(AddressTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
