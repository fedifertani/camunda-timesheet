import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { AddressTypeComponent } from './address-type.component';
import { AddressTypeDetailComponent } from './address-type-detail.component';
import { AddressTypePopupComponent } from './address-type-dialog.component';
import { AddressTypeDeletePopupComponent } from './address-type-delete-dialog.component';

export const addressTypeRoute: Routes = [
    {
        path: 'address-type',
        component: AddressTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.addressType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'address-type/:id',
        component: AddressTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.addressType.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const addressTypePopupRoute: Routes = [
    {
        path: 'address-type-new',
        component: AddressTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.addressType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-type/:id/edit',
        component: AddressTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.addressType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'address-type/:id/delete',
        component: AddressTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.addressType.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
