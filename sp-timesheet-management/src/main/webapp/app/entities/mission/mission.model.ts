import { BaseEntity, User } from './../../shared';
import { Country } from '../country';
import { Address } from '../address';
import { Project } from '../project';

export class Mission implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public startDate?: any,
        public endDate?: any,
        public user?: User,
        public project?: Project,
        public addresses?: Address[],
        public country?: Country,
    ) {
    }
}
