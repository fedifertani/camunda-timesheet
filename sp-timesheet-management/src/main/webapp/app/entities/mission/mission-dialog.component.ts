import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { Mission } from './mission.model';
import { MissionPopupService } from './mission-popup.service';
import { MissionService } from './mission.service';
import { User, UserService } from '../../shared';
import { Project, ProjectService } from '../project';
import { Address, AddressService } from '../address';
import { Country, CountryService } from '../country';

@Component({
    selector: 'jhi-mission-dialog',
    templateUrl: './mission-dialog.component.html'
})
export class MissionDialogComponent implements OnInit {

    mission: Mission;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;
    startDateDp: any;
    endDateDp: any;
    currentSearch: string;
    users: User[];
    projects: Project[];
    addresses: Address[];
    countries: Country[];

    constructor(
        public activeModal: NgbActiveModal,
        private missionService: MissionService,
        private eventManager: JhiEventManager,
        private jhiAlertService: JhiAlertService,
        private projectService: ProjectService,
        private userService: UserService,
        private addressService: AddressService,
        private countryService: CountryService
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.projectService.query().subscribe(
            (res: HttpResponse<Project[]>) => {
                this.projects = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.userService.query().subscribe(
            (res: HttpResponse<User[]>) => {
                this.users = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.addressService.query().subscribe(
            (res: HttpResponse<Address[]>) => {
                this.addresses = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
        this.countryService.query().subscribe(
            (res: HttpResponse<Country[]>) => {
                this.countries = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.mission.id !== undefined) {
            this.subscribeToSaveResponse(
                this.missionService.update(this.mission));
        } else {
            this.subscribeToSaveResponse(
                this.missionService.create(this.mission));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<Mission>>) {
        result.subscribe((res: HttpResponse<Mission>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: Mission) {
        this.eventManager.broadcast({ name: 'missionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-mission-popup',
    template: ''
})
export class MissionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private missionPopupService: MissionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.missionPopupService
                    .open(MissionDialogComponent as Component, params['id']);
            } else {
                this.missionPopupService
                    .open(MissionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
