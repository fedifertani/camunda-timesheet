import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { Module } from './module.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<Module>;

@Injectable()
export class ModuleService {

    private resourceUrl =  SERVER_API_URL + 'api/modules';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/modules';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(module: Module): Observable<EntityResponseType> {
        const copy = this.convert(module);
        return this.http.post<Module>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(module: Module): Observable<EntityResponseType> {
        const copy = this.convert(module);
        return this.http.put<Module>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<Module>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<Module[]>> {
        const options = createRequestOption(req);
        return this.http.get<Module[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Module[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<Module[]>> {
        const options = createRequestOption(req);
        return this.http.get<Module[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<Module[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: Module = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<Module[]>): HttpResponse<Module[]> {
        const jsonResponse: Module[] = res.body;
        const body: Module[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to Module.
     */
    private convertItemFromServer(module: Module): Module {
        const copy: Module = Object.assign({}, module);
        copy.createdDate = this.dateUtils
            .convertLocalDateFromServer(module.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateFromServer(module.updatedDate);
        return copy;
    }

    /**
     * Convert a Module to a JSON which can be sent to the server.
     */
    private convert(module: Module): Module {
        const copy: Module = Object.assign({}, module);
        copy.createdDate = this.dateUtils
            .convertLocalDateToServer(module.createdDate);
        copy.updatedDate = this.dateUtils
            .convertLocalDateToServer(module.updatedDate);
        return copy;
    }
}
