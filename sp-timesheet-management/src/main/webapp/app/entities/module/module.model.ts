import { BaseEntity } from './../../shared';

export class Module implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public name?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public summary?: string,
        public description?: string,
    ) {
    }
}
