import { BaseEntity } from './../../shared';
import { Address } from '../address';
import { CustomerContact } from '../customer-contact';

export class Customer implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public name?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public customerContacts?: CustomerContact[],
        public addresses?: Address[],
    ) {
    }
}
