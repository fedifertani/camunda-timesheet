import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { SickLeaveRequest } from './sick-leave-request.model';
import { SickLeaveRequestService } from './sick-leave-request.service';

@Component({
    selector: 'jhi-sick-leave-request-detail',
    templateUrl: './sick-leave-request-detail.component.html'
})
export class SickLeaveRequestDetailComponent implements OnInit, OnDestroy {

    sickLeaveRequest: SickLeaveRequest;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private sickLeaveRequestService: SickLeaveRequestService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInSickLeaveRequests();
    }

    load(id) {
        this.sickLeaveRequestService.find(id)
            .subscribe((sickLeaveRequestResponse: HttpResponse<SickLeaveRequest>) => {
                this.sickLeaveRequest = sickLeaveRequestResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInSickLeaveRequests() {
        this.eventSubscriber = this.eventManager.subscribe(
            'sickLeaveRequestListModification',
            (response) => this.load(this.sickLeaveRequest.id)
        );
    }
}
