import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { SickLeaveRequest } from './sick-leave-request.model';
import { SickLeaveRequestService } from './sick-leave-request.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-sick-leave-request',
    templateUrl: './sick-leave-request.component.html'
})
export class SickLeaveRequestComponent implements OnInit, OnDestroy {
sickLeaveRequests: SickLeaveRequest[];
    currentAccount: any;
    eventSubscriber: Subscription;
    currentSearch: string;

    constructor(
        private sickLeaveRequestService: SickLeaveRequestService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private activatedRoute: ActivatedRoute,
        private principal: Principal
    ) {
        this.currentSearch = this.activatedRoute.snapshot && this.activatedRoute.snapshot.params['search'] ?
            this.activatedRoute.snapshot.params['search'] : '';
    }

    loadAll() {
        if (this.currentSearch) {
            this.sickLeaveRequestService.search({
                query: this.currentSearch,
                }).subscribe(
                    (res: HttpResponse<SickLeaveRequest[]>) => this.sickLeaveRequests = res.body,
                    (res: HttpErrorResponse) => this.onError(res.message)
                );
            return;
       }
        this.sickLeaveRequestService.query().subscribe(
            (res: HttpResponse<SickLeaveRequest[]>) => {
                this.sickLeaveRequests = res.body;
                this.currentSearch = '';
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    search(query) {
        if (!query) {
            return this.clear();
        }
        this.currentSearch = query;
        this.loadAll();
    }

    clear() {
        this.currentSearch = '';
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInSickLeaveRequests();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: SickLeaveRequest) {
        return item.id;
    }
    registerChangeInSickLeaveRequests() {
        this.eventSubscriber = this.eventManager.subscribe('sickLeaveRequestListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
