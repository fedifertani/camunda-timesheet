import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SickLeaveRequest } from './sick-leave-request.model';
import { SickLeaveRequestPopupService } from './sick-leave-request-popup.service';
import { SickLeaveRequestService } from './sick-leave-request.service';

@Component({
    selector: 'jhi-sick-leave-request-dialog',
    templateUrl: './sick-leave-request-dialog.component.html'
})
export class SickLeaveRequestDialogComponent implements OnInit {

    sickLeaveRequest: SickLeaveRequest;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private sickLeaveRequestService: SickLeaveRequestService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.sickLeaveRequest.id !== undefined) {
            this.subscribeToSaveResponse(
                this.sickLeaveRequestService.update(this.sickLeaveRequest));
        } else {
            this.subscribeToSaveResponse(
                this.sickLeaveRequestService.create(this.sickLeaveRequest));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<SickLeaveRequest>>) {
        result.subscribe((res: HttpResponse<SickLeaveRequest>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: SickLeaveRequest) {
        this.eventManager.broadcast({ name: 'sickLeaveRequestListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-sick-leave-request-popup',
    template: ''
})
export class SickLeaveRequestPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sickLeaveRequestPopupService: SickLeaveRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.sickLeaveRequestPopupService
                    .open(SickLeaveRequestDialogComponent as Component, params['id']);
            } else {
                this.sickLeaveRequestPopupService
                    .open(SickLeaveRequestDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
