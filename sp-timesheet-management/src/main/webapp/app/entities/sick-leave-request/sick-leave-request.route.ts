import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { SickLeaveRequestComponent } from './sick-leave-request.component';
import { SickLeaveRequestDetailComponent } from './sick-leave-request-detail.component';
import { SickLeaveRequestPopupComponent } from './sick-leave-request-dialog.component';
import { SickLeaveRequestDeletePopupComponent } from './sick-leave-request-delete-dialog.component';

export const sickLeaveRequestRoute: Routes = [
    {
        path: 'sick-leave-request',
        component: SickLeaveRequestComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.sickLeaveRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'sick-leave-request/:id',
        component: SickLeaveRequestDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.sickLeaveRequest.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const sickLeaveRequestPopupRoute: Routes = [
    {
        path: 'sick-leave-request-new',
        component: SickLeaveRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.sickLeaveRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sick-leave-request/:id/edit',
        component: SickLeaveRequestPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.sickLeaveRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'sick-leave-request/:id/delete',
        component: SickLeaveRequestDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.sickLeaveRequest.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
