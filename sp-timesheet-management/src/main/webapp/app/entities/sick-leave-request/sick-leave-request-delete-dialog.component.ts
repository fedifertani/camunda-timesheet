import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { SickLeaveRequest } from './sick-leave-request.model';
import { SickLeaveRequestPopupService } from './sick-leave-request-popup.service';
import { SickLeaveRequestService } from './sick-leave-request.service';

@Component({
    selector: 'jhi-sick-leave-request-delete-dialog',
    templateUrl: './sick-leave-request-delete-dialog.component.html'
})
export class SickLeaveRequestDeleteDialogComponent {

    sickLeaveRequest: SickLeaveRequest;

    constructor(
        private sickLeaveRequestService: SickLeaveRequestService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: string) {
        this.sickLeaveRequestService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'sickLeaveRequestListModification',
                content: 'Deleted an sickLeaveRequest'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-sick-leave-request-delete-popup',
    template: ''
})
export class SickLeaveRequestDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private sickLeaveRequestPopupService: SickLeaveRequestPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.sickLeaveRequestPopupService
                .open(SickLeaveRequestDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
