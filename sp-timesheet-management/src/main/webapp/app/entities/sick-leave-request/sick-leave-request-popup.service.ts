import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { SickLeaveRequest } from './sick-leave-request.model';
import { SickLeaveRequestService } from './sick-leave-request.service';

@Injectable()
export class SickLeaveRequestPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private sickLeaveRequestService: SickLeaveRequestService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.sickLeaveRequestService.find(id)
                    .subscribe((sickLeaveRequestResponse: HttpResponse<SickLeaveRequest>) => {
                        const sickLeaveRequest: SickLeaveRequest = sickLeaveRequestResponse.body;
                        this.ngbModalRef = this.sickLeaveRequestModalRef(component, sickLeaveRequest);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.sickLeaveRequestModalRef(component, new SickLeaveRequest());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    sickLeaveRequestModalRef(component: Component, sickLeaveRequest: SickLeaveRequest): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.sickLeaveRequest = sickLeaveRequest;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
