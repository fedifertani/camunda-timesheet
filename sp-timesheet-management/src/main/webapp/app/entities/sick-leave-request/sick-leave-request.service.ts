import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { SickLeaveRequest } from './sick-leave-request.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<SickLeaveRequest>;

@Injectable()
export class SickLeaveRequestService {

    private resourceUrl =  SERVER_API_URL + 'api/sick-leave-requests';
    private resourceSearchUrl = SERVER_API_URL + 'api/_search/sick-leave-requests';

    constructor(private http: HttpClient) { }

    create(sickLeaveRequest: SickLeaveRequest): Observable<EntityResponseType> {
        const copy = this.convert(sickLeaveRequest);
        return this.http.post<SickLeaveRequest>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(sickLeaveRequest: SickLeaveRequest): Observable<EntityResponseType> {
        const copy = this.convert(sickLeaveRequest);
        return this.http.put<SickLeaveRequest>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: string): Observable<EntityResponseType> {
        return this.http.get<SickLeaveRequest>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<SickLeaveRequest[]>> {
        const options = createRequestOption(req);
        return this.http.get<SickLeaveRequest[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SickLeaveRequest[]>) => this.convertArrayResponse(res));
    }

    delete(id: string): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    search(req?: any): Observable<HttpResponse<SickLeaveRequest[]>> {
        const options = createRequestOption(req);
        return this.http.get<SickLeaveRequest[]>(this.resourceSearchUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<SickLeaveRequest[]>) => this.convertArrayResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: SickLeaveRequest = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<SickLeaveRequest[]>): HttpResponse<SickLeaveRequest[]> {
        const jsonResponse: SickLeaveRequest[] = res.body;
        const body: SickLeaveRequest[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to SickLeaveRequest.
     */
    private convertItemFromServer(sickLeaveRequest: SickLeaveRequest): SickLeaveRequest {
        const copy: SickLeaveRequest = Object.assign({}, sickLeaveRequest);
        return copy;
    }

    /**
     * Convert a SickLeaveRequest to a JSON which can be sent to the server.
     */
    private convert(sickLeaveRequest: SickLeaveRequest): SickLeaveRequest {
        const copy: SickLeaveRequest = Object.assign({}, sickLeaveRequest);
        return copy;
    }
}
