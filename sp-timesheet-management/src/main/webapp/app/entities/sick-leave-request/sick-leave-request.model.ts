import { BaseEntity } from './../../shared';

export class SickLeaveRequest implements BaseEntity {
    constructor(
        public id?: string,
        public media?: string,
    ) {
    }
}
