import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    SickLeaveRequestService,
    SickLeaveRequestPopupService,
    SickLeaveRequestComponent,
    SickLeaveRequestDetailComponent,
    SickLeaveRequestDialogComponent,
    SickLeaveRequestPopupComponent,
    SickLeaveRequestDeletePopupComponent,
    SickLeaveRequestDeleteDialogComponent,
    sickLeaveRequestRoute,
    sickLeaveRequestPopupRoute,
} from './';

const ENTITY_STATES = [
    ...sickLeaveRequestRoute,
    ...sickLeaveRequestPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        SickLeaveRequestComponent,
        SickLeaveRequestDetailComponent,
        SickLeaveRequestDialogComponent,
        SickLeaveRequestDeleteDialogComponent,
        SickLeaveRequestPopupComponent,
        SickLeaveRequestDeletePopupComponent,
    ],
    entryComponents: [
        SickLeaveRequestComponent,
        SickLeaveRequestDialogComponent,
        SickLeaveRequestPopupComponent,
        SickLeaveRequestDeleteDialogComponent,
        SickLeaveRequestDeletePopupComponent,
    ],
    providers: [
        SickLeaveRequestService,
        SickLeaveRequestPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetSickLeaveRequestModule {}
