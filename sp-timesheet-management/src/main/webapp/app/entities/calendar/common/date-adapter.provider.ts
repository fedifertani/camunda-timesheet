import { Injectable } from "@angular/core";
import { DateAdapter } from "../../../date-adapters/date-adapter";

@Injectable()
export abstract class MyDateAdapter extends DateAdapter {
}