import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResizableModule } from 'angular-resizable-element';
//import { DragAndDropModule } from 'angular-draggable-droppable';
import { TimesheetCalendarDayViewComponent } from './timesheet-calendar-day-view.component';
import { TimesheetCalendarDayViewHourSegmentComponent } from './calendar-day-view-hour-segment.component';
import { TimesheetCalendarDayViewEventComponent } from './calendar-day-view-event.component';
import { CalendarCommonModule } from '../common/calendar-common.module';

export {
  TimesheetCalendarDayViewComponent,
  CalendarDayViewBeforeRenderEvent
} from './timesheet-calendar-day-view.component';

@NgModule({
  imports: [
    CommonModule,
    ResizableModule,
   // DragAndDropModule,
    CalendarCommonModule
  ],
  declarations: [
    TimesheetCalendarDayViewComponent,
    TimesheetCalendarDayViewHourSegmentComponent,
    TimesheetCalendarDayViewEventComponent
  ],
  exports: [
    ResizableModule,
   // DragAndDropModule,
    TimesheetCalendarDayViewComponent,
    TimesheetCalendarDayViewHourSegmentComponent,
    TimesheetCalendarDayViewEventComponent
  ]
})
export class TimesheetCalendarDayModule {}