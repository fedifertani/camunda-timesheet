import { NgModule, ModuleWithProviders, Provider } from '@angular/core';
import {
  CalendarCommonModule,
  CalendarModuleConfig,
  CalendarEventTitleFormatter,
  CalendarDateFormatter
} from './common/calendar-common.module';
import { TimesheetCalendarDayViewComponent } from './timesheet-calendar-day-view/timesheet-calendar-day-view.component';
import { CalendarUtils } from './common/calendar-utils.provider';
import { TimesheetCalendarDayModule } from './timesheet-calendar-day-view/timesheet-calendar-day.module';

export * from './common/calendar-common.module';
export * from './timesheet-calendar-day-view/timesheet-calendar-day-view.component';


/**
 * The main module of this library. Example usage:
 *
 * ```typescript
 * import { CalenderModule } from 'angular-calendar';
 *
 * @NgModule({
 *   imports: [
 *     CalenderModule.forRoot()
 *   ]
 * })
 * class MyModule {}
 * ```
 *
 */
@NgModule({
  imports: [
    CalendarCommonModule,
    TimesheetCalendarDayModule
  ],
  exports: [
    CalendarCommonModule,
    TimesheetCalendarDayModule
  ]
})
export class CalendarModule {
  static forRoot(
    dateAdapter: Provider,
    config: CalendarModuleConfig = {}
  ): ModuleWithProviders {
    return {
      ngModule: CalendarModule,
      providers: [
        dateAdapter,
        config.eventTitleFormatter || CalendarEventTitleFormatter,
        config.dateFormatter || CalendarDateFormatter,
        config.utils || CalendarUtils
      ]
    };
  }
}