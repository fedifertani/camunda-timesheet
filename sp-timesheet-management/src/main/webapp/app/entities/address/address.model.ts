import { BaseEntity } from './../../shared';
import { Country } from '../country';
import { AddressType } from '../address-type';

export class Address implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public apartment?: string,
        public buildng?: string,
        public department?: string,
        public pobox?: string,
        public postaCode?: string,
        public streetName?: string,
        public streetNumber?: string,
        public town?: string,
        public country?: Country,
        public type?: AddressType,
    ) {
    }
}
