import { BaseEntity } from './../../shared';
import { DayOff } from '../day-off';

export class Country implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public createdDate?: any,
        public updatedDate?: any,
        public isoCode?: string,
        public name?: string,
        public dayOffs?: DayOff[],
    ) {
    }
}
