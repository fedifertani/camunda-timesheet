import { BaseEntity } from './../../shared';

export class Currency implements BaseEntity {
    constructor(
        public id?: string,
        public code?: string,
        public isoCode?: string,
        public value?: string,
        public createdDate?: any,
        public updatedDate?: any,
    ) {
    }
}
