import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { TimesheetVacationRequestModule } from './vacation-request/vacation-request.module';
import { TimesheetVacationTypeModule } from './vacation-type/vacation-type.module';
import { TimesheetSickLeaveRequestModule } from './sick-leave-request/sick-leave-request.module';
import { TimesheetProjectModule } from './project/project.module';
import { TimesheetCurrencyModule } from './currency/currency.module';
import { TimesheetUserPositionModule } from './user-position/user-position.module';
import { TimesheetProjectMemberShipModule } from './project-member-ship/project-member-ship.module';
import { TimesheetModuleModule } from './module/module.module';
import { TimesheetTimeEntryModule } from './time-entry/time-entry.module';
import { TimesheetCustomerModule } from './customer/customer.module';
import { TimesheetCustomerContactModule } from './customer-contact/customer-contact.module';
import { TimesheetCustomerContactTypeModule } from './customer-contact-type/customer-contact-type.module';
import { TimesheetAddressModule } from './address/address.module';
import { TimesheetAddressTypeModule } from './address-type/address-type.module';
import { TimesheetCountryModule } from './country/country.module';
import { TimesheetDayOffModule } from './day-off/day-off.module';
import { TimesheetMissionModule } from './mission/mission.module';
import { TimesheetInvoiceModule } from './invoice/invoice.module';
import { TimesheetVacationTaskModule } from './vacation-task/vacation-task.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        TimesheetVacationRequestModule,
        TimesheetVacationTypeModule,
        TimesheetSickLeaveRequestModule,
        TimesheetProjectModule,
        TimesheetCurrencyModule,
        TimesheetUserPositionModule,
        TimesheetProjectMemberShipModule,
        TimesheetModuleModule,
        TimesheetTimeEntryModule,
        TimesheetCustomerModule,
        TimesheetCustomerContactModule,
        TimesheetCustomerContactTypeModule,
        TimesheetAddressModule,
        TimesheetAddressTypeModule,
        TimesheetCountryModule,
        TimesheetDayOffModule,
        TimesheetMissionModule,
        TimesheetInvoiceModule,
        TimesheetVacationTaskModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetEntityModule {}
