import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TimesheetSharedModule } from '../../shared';
import {
    UserPositionService,
    UserPositionPopupService,
    UserPositionComponent,
    UserPositionDetailComponent,
    UserPositionDialogComponent,
    UserPositionPopupComponent,
    UserPositionDeletePopupComponent,
    UserPositionDeleteDialogComponent,
    userPositionRoute,
    userPositionPopupRoute,
} from './';

const ENTITY_STATES = [
    ...userPositionRoute,
    ...userPositionPopupRoute,
];

@NgModule({
    imports: [
        TimesheetSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserPositionComponent,
        UserPositionDetailComponent,
        UserPositionDialogComponent,
        UserPositionDeleteDialogComponent,
        UserPositionPopupComponent,
        UserPositionDeletePopupComponent,
    ],
    entryComponents: [
        UserPositionComponent,
        UserPositionDialogComponent,
        UserPositionPopupComponent,
        UserPositionDeleteDialogComponent,
        UserPositionDeletePopupComponent,
    ],
    providers: [
        UserPositionService,
        UserPositionPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TimesheetUserPositionModule {}
