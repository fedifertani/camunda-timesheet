import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { UserPosition } from './user-position.model';
import { UserPositionService } from './user-position.service';

@Injectable()
export class UserPositionPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private modalService: NgbModal,
        private router: Router,
        private userPositionService: UserPositionService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.userPositionService.find(id)
                    .subscribe((userPositionResponse: HttpResponse<UserPosition>) => {
                        const userPosition: UserPosition = userPositionResponse.body;
                        if (userPosition.createdDate) {
                            userPosition.createdDate = {
                                year: userPosition.createdDate.getFullYear(),
                                month: userPosition.createdDate.getMonth() + 1,
                                day: userPosition.createdDate.getDate()
                            };
                        }
                        if (userPosition.updatedDate) {
                            userPosition.updatedDate = {
                                year: userPosition.updatedDate.getFullYear(),
                                month: userPosition.updatedDate.getMonth() + 1,
                                day: userPosition.updatedDate.getDate()
                            };
                        }
                        this.ngbModalRef = this.userPositionModalRef(component, userPosition);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.userPositionModalRef(component, new UserPosition());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    userPositionModalRef(component: Component, userPosition: UserPosition): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.userPosition = userPosition;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
