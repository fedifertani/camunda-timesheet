import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { UserPositionComponent } from './user-position.component';
import { UserPositionDetailComponent } from './user-position-detail.component';
import { UserPositionPopupComponent } from './user-position-dialog.component';
import { UserPositionDeletePopupComponent } from './user-position-delete-dialog.component';

export const userPositionRoute: Routes = [
    {
        path: 'user-position',
        component: UserPositionComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.userPosition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'user-position/:id',
        component: UserPositionDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.userPosition.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const userPositionPopupRoute: Routes = [
    {
        path: 'user-position-new',
        component: UserPositionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.userPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-position/:id/edit',
        component: UserPositionPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.userPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'user-position/:id/delete',
        component: UserPositionDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'timesheetApp.userPosition.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
