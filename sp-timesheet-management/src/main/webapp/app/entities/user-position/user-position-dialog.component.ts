import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserPosition } from './user-position.model';
import { UserPositionPopupService } from './user-position-popup.service';
import { UserPositionService } from './user-position.service';
let moment = require('moment');

@Component({
    selector: 'jhi-user-position-dialog',
    templateUrl: './user-position-dialog.component.html'
})
export class UserPositionDialogComponent implements OnInit {

    userPosition: UserPosition;
    isSaving: boolean;
    createdDateDp: any;
    updatedDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private userPositionService: UserPositionService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.userPosition.id !== undefined) {
            this.userPosition.updatedDate=moment().toDate();
            this.subscribeToSaveResponse(
                this.userPositionService.update(this.userPosition));
        } else {
            this.userPosition.updatedDate=moment().toDate();
            this.userPosition.createdDate=moment().toDate();

            this.subscribeToSaveResponse(
                this.userPositionService.create(this.userPosition));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<UserPosition>>) {
        result.subscribe((res: HttpResponse<UserPosition>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: UserPosition) {
        this.eventManager.broadcast({ name: 'userPositionListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-user-position-popup',
    template: ''
})
export class UserPositionPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userPositionPopupService: UserPositionPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.userPositionPopupService
                    .open(UserPositionDialogComponent as Component, params['id']);
            } else {
                this.userPositionPopupService
                    .open(UserPositionDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
