export interface IUserCamunda {
    id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    password?: string;
}

export class UserCamunda implements IUserCamunda {
    constructor(
       public id?: string,
       public firstName?: string,
       public lastName?: string,
       public  email?: string,
       public  password?: string
    ) {}
}