import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL, CAMUNDA_API_URL } from '../../app.constants';
import { UserCamunda } from './UserCamunda.model';

@Injectable()
export class Register {

    constructor(private http: HttpClient) {}

    save(account: any): Observable<any> {
        return this.http.post(SERVER_API_URL + 'api/register', account);
    }
    saveCamunda(camundaAccountProfile: UserCamunda): Observable<any> {
        const  httpOptions = {
            headers: new HttpHeaders({ 
              'Access-Control-Allow-Origin':' *',
              'Content-Type': 'application/json',
              'Access-Control-Allow-Methods' : '*'
            })
          };
        return this.http.post(CAMUNDA_API_URL + 'user', camundaAccountProfile,httpOptions);
    }
}
