import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { UserModalService } from './user-modal.service';
import { JhiLanguageHelper, User, UserService } from '../../shared';
import { UserCamunda } from '../../account/register/UserCamunda.model';
import { Register } from '../../account';
import { UserPosition, UserPositionService } from '../../entities/user-position';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-user-mgmt-dialog',
    templateUrl: './user-management-dialog.component.html'
})
export class UserMgmtDialogComponent implements OnInit {

    user: User;
    languages: any[];
    authorities: any[];
    isSaving: Boolean;
    camundaAccount: UserCamunda;
    registerAccount: any;
    userPositions: UserPosition[];
    constructor(
        public activeModal: NgbActiveModal,
        private languageHelper: JhiLanguageHelper,
        private userService: UserService,
        private userPositionService: UserPositionService,
        private eventManager: JhiEventManager
    ) {}

    ngOnInit() {
        this.userPositionService.query().subscribe(
            (res: HttpResponse<UserPosition[]>) => {
                this.userPositions = res.body;
            },
        
        );
        this.isSaving = false;
        this.authorities = [];
        this.userService.authorities().subscribe((authorities) => {
            this.authorities = authorities;
        });
        this.languageHelper.getAll().then((languages) => {
            this.languages = languages;
        });
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.user.id !== null) {
            this.user.userPosition=this.userPositions[0];
            this.userService.update(this.user).subscribe((response) => this.onSaveSuccess(response), () =>
             this.onSaveError());
        } else {          
            this.userService.create(this.user).subscribe(() => {           
                    this.camundaAccount=new UserCamunda(this.user.login,this.user.firstName,this.user.lastName,this.user.email,this.user.login);
 
                    this.userService.saveCamunda(this.camundaAccount).subscribe(
                    () => {                        
                        this.clear();
                        this.eventManager.broadcast({ name: 'userListModification', content: 'OK' });
                    },
                
                );
                   
              
               
            }, () => this.onSaveError());
    }
}

    private onSaveSuccess(result) {
        this.eventManager.broadcast({ name: 'userListModification', content: 'OK' });
        this.isSaving = false;
            this.activeModal.dismiss(result.body);
        
        }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-user-dialog',
    template: ''
})
export class UserDialogComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private userModalService: UserModalService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['login'] ) {
                this.userModalService.open(UserMgmtDialogComponent as Component, params['login']);
            } else {
                this.userModalService.open(UserMgmtDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
