/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { VacationRequestComponent } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request.component';
import { VacationRequestService } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request.service';
import { VacationRequest } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request.model';

describe('Component Tests', () => {

    describe('VacationRequest Management Component', () => {
        let comp: VacationRequestComponent;
        let fixture: ComponentFixture<VacationRequestComponent>;
        let service: VacationRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationRequestComponent],
                providers: [
                    VacationRequestService
                ]
            })
            .overrideTemplate(VacationRequestComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationRequestComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new VacationRequest('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.vacationRequests[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
