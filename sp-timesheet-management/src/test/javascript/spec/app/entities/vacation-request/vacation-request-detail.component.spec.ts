/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { VacationRequestDetailComponent } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request-detail.component';
import { VacationRequestService } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request.service';
import { VacationRequest } from '../../../../../../main/webapp/app/entities/vacation-request/vacation-request.model';

describe('Component Tests', () => {

    describe('VacationRequest Management Detail Component', () => {
        let comp: VacationRequestDetailComponent;
        let fixture: ComponentFixture<VacationRequestDetailComponent>;
        let service: VacationRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationRequestDetailComponent],
                providers: [
                    VacationRequestService
                ]
            })
            .overrideTemplate(VacationRequestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationRequestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new VacationRequest('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.vacationRequest).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
