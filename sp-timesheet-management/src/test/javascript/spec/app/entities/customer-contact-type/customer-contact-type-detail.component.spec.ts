/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { CustomerContactTypeDetailComponent } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type-detail.component';
import { CustomerContactTypeService } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.service';
import { CustomerContactType } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.model';

describe('Component Tests', () => {

    describe('CustomerContactType Management Detail Component', () => {
        let comp: CustomerContactTypeDetailComponent;
        let fixture: ComponentFixture<CustomerContactTypeDetailComponent>;
        let service: CustomerContactTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [CustomerContactTypeDetailComponent],
                providers: [
                    CustomerContactTypeService
                ]
            })
            .overrideTemplate(CustomerContactTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CustomerContactTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerContactTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CustomerContactType('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.customerContactType).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
