/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { CustomerContactTypeComponent } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.component';
import { CustomerContactTypeService } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.service';
import { CustomerContactType } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.model';

describe('Component Tests', () => {

    describe('CustomerContactType Management Component', () => {
        let comp: CustomerContactTypeComponent;
        let fixture: ComponentFixture<CustomerContactTypeComponent>;
        let service: CustomerContactTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [CustomerContactTypeComponent],
                providers: [
                    CustomerContactTypeService
                ]
            })
            .overrideTemplate(CustomerContactTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CustomerContactTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerContactTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CustomerContactType('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.customerContactTypes[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
