/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TimesheetTestModule } from '../../../test.module';
import { CustomerContactTypeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type-delete-dialog.component';
import { CustomerContactTypeService } from '../../../../../../main/webapp/app/entities/customer-contact-type/customer-contact-type.service';

describe('Component Tests', () => {

    describe('CustomerContactType Management Delete Component', () => {
        let comp: CustomerContactTypeDeleteDialogComponent;
        let fixture: ComponentFixture<CustomerContactTypeDeleteDialogComponent>;
        let service: CustomerContactTypeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [CustomerContactTypeDeleteDialogComponent],
                providers: [
                    CustomerContactTypeService
                ]
            })
            .overrideTemplate(CustomerContactTypeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CustomerContactTypeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerContactTypeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
