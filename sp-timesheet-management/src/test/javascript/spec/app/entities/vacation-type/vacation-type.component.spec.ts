/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { VacationTypeComponent } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type.component';
import { VacationTypeService } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type.service';
import { VacationType } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type.model';

describe('Component Tests', () => {

    describe('VacationType Management Component', () => {
        let comp: VacationTypeComponent;
        let fixture: ComponentFixture<VacationTypeComponent>;
        let service: VacationTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationTypeComponent],
                providers: [
                    VacationTypeService
                ]
            })
            .overrideTemplate(VacationTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new VacationType('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.vacationTypes[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
