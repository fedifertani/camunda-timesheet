/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { VacationTypeDetailComponent } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type-detail.component';
import { VacationTypeService } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type.service';
import { VacationType } from '../../../../../../main/webapp/app/entities/vacation-type/vacation-type.model';

describe('Component Tests', () => {

    describe('VacationType Management Detail Component', () => {
        let comp: VacationTypeDetailComponent;
        let fixture: ComponentFixture<VacationTypeDetailComponent>;
        let service: VacationTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationTypeDetailComponent],
                providers: [
                    VacationTypeService
                ]
            })
            .overrideTemplate(VacationTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new VacationType('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.vacationType).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
