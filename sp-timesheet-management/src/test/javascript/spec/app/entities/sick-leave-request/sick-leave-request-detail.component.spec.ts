/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { SickLeaveRequestDetailComponent } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request-detail.component';
import { SickLeaveRequestService } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.service';
import { SickLeaveRequest } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.model';

describe('Component Tests', () => {

    describe('SickLeaveRequest Management Detail Component', () => {
        let comp: SickLeaveRequestDetailComponent;
        let fixture: ComponentFixture<SickLeaveRequestDetailComponent>;
        let service: SickLeaveRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [SickLeaveRequestDetailComponent],
                providers: [
                    SickLeaveRequestService
                ]
            })
            .overrideTemplate(SickLeaveRequestDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SickLeaveRequestDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SickLeaveRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new SickLeaveRequest('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.sickLeaveRequest).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
