/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TimesheetTestModule } from '../../../test.module';
import { SickLeaveRequestDialogComponent } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request-dialog.component';
import { SickLeaveRequestService } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.service';
import { SickLeaveRequest } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.model';

describe('Component Tests', () => {

    describe('SickLeaveRequest Management Dialog Component', () => {
        let comp: SickLeaveRequestDialogComponent;
        let fixture: ComponentFixture<SickLeaveRequestDialogComponent>;
        let service: SickLeaveRequestService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [SickLeaveRequestDialogComponent],
                providers: [
                    SickLeaveRequestService
                ]
            })
            .overrideTemplate(SickLeaveRequestDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SickLeaveRequestDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SickLeaveRequestService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SickLeaveRequest('123');
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.sickLeaveRequest = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'sickLeaveRequestListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new SickLeaveRequest();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.sickLeaveRequest = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'sickLeaveRequestListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
