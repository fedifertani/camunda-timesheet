/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TimesheetTestModule } from '../../../test.module';
import { SickLeaveRequestDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request-delete-dialog.component';
import { SickLeaveRequestService } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.service';

describe('Component Tests', () => {

    describe('SickLeaveRequest Management Delete Component', () => {
        let comp: SickLeaveRequestDeleteDialogComponent;
        let fixture: ComponentFixture<SickLeaveRequestDeleteDialogComponent>;
        let service: SickLeaveRequestService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [SickLeaveRequestDeleteDialogComponent],
                providers: [
                    SickLeaveRequestService
                ]
            })
            .overrideTemplate(SickLeaveRequestDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SickLeaveRequestDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SickLeaveRequestService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
