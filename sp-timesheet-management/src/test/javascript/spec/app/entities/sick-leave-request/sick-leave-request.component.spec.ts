/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { SickLeaveRequestComponent } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.component';
import { SickLeaveRequestService } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.service';
import { SickLeaveRequest } from '../../../../../../main/webapp/app/entities/sick-leave-request/sick-leave-request.model';

describe('Component Tests', () => {

    describe('SickLeaveRequest Management Component', () => {
        let comp: SickLeaveRequestComponent;
        let fixture: ComponentFixture<SickLeaveRequestComponent>;
        let service: SickLeaveRequestService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [SickLeaveRequestComponent],
                providers: [
                    SickLeaveRequestService
                ]
            })
            .overrideTemplate(SickLeaveRequestComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(SickLeaveRequestComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(SickLeaveRequestService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new SickLeaveRequest('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.sickLeaveRequests[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
