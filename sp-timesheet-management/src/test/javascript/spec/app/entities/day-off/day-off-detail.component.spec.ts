/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { DayOffDetailComponent } from '../../../../../../main/webapp/app/entities/day-off/day-off-detail.component';
import { DayOffService } from '../../../../../../main/webapp/app/entities/day-off/day-off.service';
import { DayOff } from '../../../../../../main/webapp/app/entities/day-off/day-off.model';

describe('Component Tests', () => {

    describe('DayOff Management Detail Component', () => {
        let comp: DayOffDetailComponent;
        let fixture: ComponentFixture<DayOffDetailComponent>;
        let service: DayOffService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [DayOffDetailComponent],
                providers: [
                    DayOffService
                ]
            })
            .overrideTemplate(DayOffDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DayOffDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DayOffService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new DayOff('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.dayOff).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
