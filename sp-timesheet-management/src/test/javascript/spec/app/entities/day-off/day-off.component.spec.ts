/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { DayOffComponent } from '../../../../../../main/webapp/app/entities/day-off/day-off.component';
import { DayOffService } from '../../../../../../main/webapp/app/entities/day-off/day-off.service';
import { DayOff } from '../../../../../../main/webapp/app/entities/day-off/day-off.model';

describe('Component Tests', () => {

    describe('DayOff Management Component', () => {
        let comp: DayOffComponent;
        let fixture: ComponentFixture<DayOffComponent>;
        let service: DayOffService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [DayOffComponent],
                providers: [
                    DayOffService
                ]
            })
            .overrideTemplate(DayOffComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(DayOffComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(DayOffService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new DayOff('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.dayOffs[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
