/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { UserPositionDetailComponent } from '../../../../../../main/webapp/app/entities/user-position/user-position-detail.component';
import { UserPositionService } from '../../../../../../main/webapp/app/entities/user-position/user-position.service';
import { UserPosition } from '../../../../../../main/webapp/app/entities/user-position/user-position.model';

describe('Component Tests', () => {

    describe('UserPosition Management Detail Component', () => {
        let comp: UserPositionDetailComponent;
        let fixture: ComponentFixture<UserPositionDetailComponent>;
        let service: UserPositionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [UserPositionDetailComponent],
                providers: [
                    UserPositionService
                ]
            })
            .overrideTemplate(UserPositionDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserPositionDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserPositionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new UserPosition('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.userPosition).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
