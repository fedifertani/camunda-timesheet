/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { UserPositionComponent } from '../../../../../../main/webapp/app/entities/user-position/user-position.component';
import { UserPositionService } from '../../../../../../main/webapp/app/entities/user-position/user-position.service';
import { UserPosition } from '../../../../../../main/webapp/app/entities/user-position/user-position.model';

describe('Component Tests', () => {

    describe('UserPosition Management Component', () => {
        let comp: UserPositionComponent;
        let fixture: ComponentFixture<UserPositionComponent>;
        let service: UserPositionService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [UserPositionComponent],
                providers: [
                    UserPositionService
                ]
            })
            .overrideTemplate(UserPositionComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(UserPositionComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(UserPositionService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new UserPosition('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.userPositions[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
