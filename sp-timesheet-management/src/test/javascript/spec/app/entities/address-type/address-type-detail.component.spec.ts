/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { AddressTypeDetailComponent } from '../../../../../../main/webapp/app/entities/address-type/address-type-detail.component';
import { AddressTypeService } from '../../../../../../main/webapp/app/entities/address-type/address-type.service';
import { AddressType } from '../../../../../../main/webapp/app/entities/address-type/address-type.model';

describe('Component Tests', () => {

    describe('AddressType Management Detail Component', () => {
        let comp: AddressTypeDetailComponent;
        let fixture: ComponentFixture<AddressTypeDetailComponent>;
        let service: AddressTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [AddressTypeDetailComponent],
                providers: [
                    AddressTypeService
                ]
            })
            .overrideTemplate(AddressTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new AddressType('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.addressType).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
