/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { AddressTypeComponent } from '../../../../../../main/webapp/app/entities/address-type/address-type.component';
import { AddressTypeService } from '../../../../../../main/webapp/app/entities/address-type/address-type.service';
import { AddressType } from '../../../../../../main/webapp/app/entities/address-type/address-type.model';

describe('Component Tests', () => {

    describe('AddressType Management Component', () => {
        let comp: AddressTypeComponent;
        let fixture: ComponentFixture<AddressTypeComponent>;
        let service: AddressTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [AddressTypeComponent],
                providers: [
                    AddressTypeService
                ]
            })
            .overrideTemplate(AddressTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(AddressTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(AddressTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new AddressType('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.addressTypes[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
