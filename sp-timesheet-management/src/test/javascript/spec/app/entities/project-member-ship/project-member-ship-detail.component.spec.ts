/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { ProjectMemberShipDetailComponent } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship-detail.component';
import { ProjectMemberShipService } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.service';
import { ProjectMemberShip } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.model';

describe('Component Tests', () => {

    describe('ProjectMemberShip Management Detail Component', () => {
        let comp: ProjectMemberShipDetailComponent;
        let fixture: ComponentFixture<ProjectMemberShipDetailComponent>;
        let service: ProjectMemberShipService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [ProjectMemberShipDetailComponent],
                providers: [
                    ProjectMemberShipService
                ]
            })
            .overrideTemplate(ProjectMemberShipDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProjectMemberShipDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProjectMemberShipService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new ProjectMemberShip('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.projectMemberShip).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
