/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { TimesheetTestModule } from '../../../test.module';
import { ProjectMemberShipDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship-delete-dialog.component';
import { ProjectMemberShipService } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.service';

describe('Component Tests', () => {

    describe('ProjectMemberShip Management Delete Component', () => {
        let comp: ProjectMemberShipDeleteDialogComponent;
        let fixture: ComponentFixture<ProjectMemberShipDeleteDialogComponent>;
        let service: ProjectMemberShipService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [ProjectMemberShipDeleteDialogComponent],
                providers: [
                    ProjectMemberShipService
                ]
            })
            .overrideTemplate(ProjectMemberShipDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProjectMemberShipDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProjectMemberShipService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete('123');
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith('123');
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
