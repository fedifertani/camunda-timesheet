/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { ProjectMemberShipComponent } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.component';
import { ProjectMemberShipService } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.service';
import { ProjectMemberShip } from '../../../../../../main/webapp/app/entities/project-member-ship/project-member-ship.model';

describe('Component Tests', () => {

    describe('ProjectMemberShip Management Component', () => {
        let comp: ProjectMemberShipComponent;
        let fixture: ComponentFixture<ProjectMemberShipComponent>;
        let service: ProjectMemberShipService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [ProjectMemberShipComponent],
                providers: [
                    ProjectMemberShipService
                ]
            })
            .overrideTemplate(ProjectMemberShipComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(ProjectMemberShipComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(ProjectMemberShipService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new ProjectMemberShip('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.projectMemberShips[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
