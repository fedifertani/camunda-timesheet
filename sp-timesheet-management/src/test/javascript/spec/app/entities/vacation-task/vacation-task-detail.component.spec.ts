/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { VacationTaskDetailComponent } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task-detail.component';
import { VacationTaskService } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task.service';
import { VacationTask } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task.model';

describe('Component Tests', () => {

    describe('VacationTask Management Detail Component', () => {
        let comp: VacationTaskDetailComponent;
        let fixture: ComponentFixture<VacationTaskDetailComponent>;
        let service: VacationTaskService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationTaskDetailComponent],
                providers: [
                    VacationTaskService
                ]
            })
            .overrideTemplate(VacationTaskDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationTaskDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationTaskService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new VacationTask('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.vacationTask).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
