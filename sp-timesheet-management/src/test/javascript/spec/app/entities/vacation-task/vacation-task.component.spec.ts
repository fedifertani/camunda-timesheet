/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { VacationTaskComponent } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task.component';
import { VacationTaskService } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task.service';
import { VacationTask } from '../../../../../../main/webapp/app/entities/vacation-task/vacation-task.model';

describe('Component Tests', () => {

    describe('VacationTask Management Component', () => {
        let comp: VacationTaskComponent;
        let fixture: ComponentFixture<VacationTaskComponent>;
        let service: VacationTaskService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [VacationTaskComponent],
                providers: [
                    VacationTaskService
                ]
            })
            .overrideTemplate(VacationTaskComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(VacationTaskComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(VacationTaskService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new VacationTask('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.vacationTasks[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
