/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { TimesheetTestModule } from '../../../test.module';
import { CustomerContactDetailComponent } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact-detail.component';
import { CustomerContactService } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact.service';
import { CustomerContact } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact.model';

describe('Component Tests', () => {

    describe('CustomerContact Management Detail Component', () => {
        let comp: CustomerContactDetailComponent;
        let fixture: ComponentFixture<CustomerContactDetailComponent>;
        let service: CustomerContactService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [CustomerContactDetailComponent],
                providers: [
                    CustomerContactService
                ]
            })
            .overrideTemplate(CustomerContactDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CustomerContactDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerContactService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new CustomerContact('123')
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith('123');
                expect(comp.customerContact).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
