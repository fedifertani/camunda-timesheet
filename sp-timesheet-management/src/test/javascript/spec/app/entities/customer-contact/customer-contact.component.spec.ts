/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TimesheetTestModule } from '../../../test.module';
import { CustomerContactComponent } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact.component';
import { CustomerContactService } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact.service';
import { CustomerContact } from '../../../../../../main/webapp/app/entities/customer-contact/customer-contact.model';

describe('Component Tests', () => {

    describe('CustomerContact Management Component', () => {
        let comp: CustomerContactComponent;
        let fixture: ComponentFixture<CustomerContactComponent>;
        let service: CustomerContactService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [TimesheetTestModule],
                declarations: [CustomerContactComponent],
                providers: [
                    CustomerContactService
                ]
            })
            .overrideTemplate(CustomerContactComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CustomerContactComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CustomerContactService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new CustomerContact('123')],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.customerContacts[0]).toEqual(jasmine.objectContaining({id: '123'}));
            });
        });
    });

});
