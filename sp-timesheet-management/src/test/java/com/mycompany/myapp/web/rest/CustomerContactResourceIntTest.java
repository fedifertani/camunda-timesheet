package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.CustomerContact;
import com.mycompany.myapp.domain.CustomerContactType;
import com.mycompany.myapp.repository.CustomerContactRepository;
import com.mycompany.myapp.repository.search.CustomerContactSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CustomerContactResource REST controller.
 *
 * @see CustomerContactResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class CustomerContactResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_1 = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_1 = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE_2 = "AAAAAAAAAA";
    private static final String UPDATED_PHONE_2 = "BBBBBBBBBB";

    private static final CustomerContactType DEFAULT_TYPE = new CustomerContactType();
    private static final CustomerContactType UPDATED_TYPE = new CustomerContactType();

    @Autowired
    private CustomerContactRepository customerContactRepository;

    @Autowired
    private CustomerContactSearchRepository customerContactSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restCustomerContactMockMvc;

    private CustomerContact customerContact;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CustomerContactResource customerContactResource = new CustomerContactResource(customerContactRepository, customerContactSearchRepository);
        this.restCustomerContactMockMvc = MockMvcBuilders.standaloneSetup(customerContactResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerContact createEntity() {
        CustomerContact customerContact = new CustomerContact()
            .code(DEFAULT_CODE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .phone1(DEFAULT_PHONE_1)
            .phone2(DEFAULT_PHONE_2)
            .type(DEFAULT_TYPE);
        return customerContact;
    }

    @Before
    public void initTest() {
        customerContactRepository.deleteAll();
        customerContactSearchRepository.deleteAll();
        customerContact = createEntity();
    }

    @Test
    public void createCustomerContact() throws Exception {
        int databaseSizeBeforeCreate = customerContactRepository.findAll().size();

        // Create the CustomerContact
        restCustomerContactMockMvc.perform(post("/api/customer-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContact)))
            .andExpect(status().isCreated());

        // Validate the CustomerContact in the database
        List<CustomerContact> customerContactList = customerContactRepository.findAll();
        assertThat(customerContactList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerContact testCustomerContact = customerContactList.get(customerContactList.size() - 1);
        assertThat(testCustomerContact.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCustomerContact.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCustomerContact.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testCustomerContact.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testCustomerContact.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testCustomerContact.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testCustomerContact.getPhone1()).isEqualTo(DEFAULT_PHONE_1);
        assertThat(testCustomerContact.getPhone2()).isEqualTo(DEFAULT_PHONE_2);
        assertThat(testCustomerContact.getType()).isEqualTo(DEFAULT_TYPE);

        // Validate the CustomerContact in Elasticsearch
        CustomerContact customerContactEs = customerContactSearchRepository.findOne(testCustomerContact.getId());
        assertThat(customerContactEs).isEqualToIgnoringGivenFields(testCustomerContact);
    }

    @Test
    public void createCustomerContactWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerContactRepository.findAll().size();

        // Create the CustomerContact with an existing ID
        customerContact.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerContactMockMvc.perform(post("/api/customer-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContact)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerContact in the database
        List<CustomerContact> customerContactList = customerContactRepository.findAll();
        assertThat(customerContactList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllCustomerContacts() throws Exception {
        // Initialize the database
        customerContactRepository.save(customerContact);

        // Get all the customerContactList
        restCustomerContactMockMvc.perform(get("/api/customer-contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerContact.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone1").value(hasItem(DEFAULT_PHONE_1.toString())))
            .andExpect(jsonPath("$.[*].phone2").value(hasItem(DEFAULT_PHONE_2.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    public void getCustomerContact() throws Exception {
        // Initialize the database
        customerContactRepository.save(customerContact);

        // Get the customerContact
        restCustomerContactMockMvc.perform(get("/api/customer-contacts/{id}", customerContact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customerContact.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME.toString()))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME.toString()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL.toString()))
            .andExpect(jsonPath("$.phone1").value(DEFAULT_PHONE_1.toString()))
            .andExpect(jsonPath("$.phone2").value(DEFAULT_PHONE_2.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()));
    }

    @Test
    public void getNonExistingCustomerContact() throws Exception {
        // Get the customerContact
        restCustomerContactMockMvc.perform(get("/api/customer-contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCustomerContact() throws Exception {
        // Initialize the database
        customerContactRepository.save(customerContact);
        customerContactSearchRepository.save(customerContact);
        int databaseSizeBeforeUpdate = customerContactRepository.findAll().size();

        // Update the customerContact
        CustomerContact updatedCustomerContact = customerContactRepository.findOne(customerContact.getId());
        updatedCustomerContact
            .code(UPDATED_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .phone1(UPDATED_PHONE_1)
            .phone2(UPDATED_PHONE_2)
            .type(UPDATED_TYPE);

        restCustomerContactMockMvc.perform(put("/api/customer-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCustomerContact)))
            .andExpect(status().isOk());

        // Validate the CustomerContact in the database
        List<CustomerContact> customerContactList = customerContactRepository.findAll();
        assertThat(customerContactList).hasSize(databaseSizeBeforeUpdate);
        CustomerContact testCustomerContact = customerContactList.get(customerContactList.size() - 1);
        assertThat(testCustomerContact.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCustomerContact.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCustomerContact.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testCustomerContact.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testCustomerContact.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testCustomerContact.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testCustomerContact.getPhone1()).isEqualTo(UPDATED_PHONE_1);
        assertThat(testCustomerContact.getPhone2()).isEqualTo(UPDATED_PHONE_2);
        assertThat(testCustomerContact.getType()).isEqualTo(UPDATED_TYPE);

        // Validate the CustomerContact in Elasticsearch
        CustomerContact customerContactEs = customerContactSearchRepository.findOne(testCustomerContact.getId());
        assertThat(customerContactEs).isEqualToIgnoringGivenFields(testCustomerContact);
    }

    @Test
    public void updateNonExistingCustomerContact() throws Exception {
        int databaseSizeBeforeUpdate = customerContactRepository.findAll().size();

        // Create the CustomerContact

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCustomerContactMockMvc.perform(put("/api/customer-contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContact)))
            .andExpect(status().isCreated());

        // Validate the CustomerContact in the database
        List<CustomerContact> customerContactList = customerContactRepository.findAll();
        assertThat(customerContactList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCustomerContact() throws Exception {
        // Initialize the database
        customerContactRepository.save(customerContact);
        customerContactSearchRepository.save(customerContact);
        int databaseSizeBeforeDelete = customerContactRepository.findAll().size();

        // Get the customerContact
        restCustomerContactMockMvc.perform(delete("/api/customer-contacts/{id}", customerContact.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean customerContactExistsInEs = customerContactSearchRepository.exists(customerContact.getId());
        assertThat(customerContactExistsInEs).isFalse();

        // Validate the database is empty
        List<CustomerContact> customerContactList = customerContactRepository.findAll();
        assertThat(customerContactList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchCustomerContact() throws Exception {
        // Initialize the database
        customerContactRepository.save(customerContact);
        customerContactSearchRepository.save(customerContact);

        // Search the customerContact
        restCustomerContactMockMvc.perform(get("/api/_search/customer-contacts?query=id:" + customerContact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerContact.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME.toString())))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME.toString())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL.toString())))
            .andExpect(jsonPath("$.[*].phone1").value(hasItem(DEFAULT_PHONE_1.toString())))
            .andExpect(jsonPath("$.[*].phone2").value(hasItem(DEFAULT_PHONE_2.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerContact.class);
        CustomerContact customerContact1 = new CustomerContact();
        customerContact1.setId("id1");
        CustomerContact customerContact2 = new CustomerContact();
        customerContact2.setId(customerContact1.getId());
        assertThat(customerContact1).isEqualTo(customerContact2);
        customerContact2.setId("id2");
        assertThat(customerContact1).isNotEqualTo(customerContact2);
        customerContact1.setId(null);
        assertThat(customerContact1).isNotEqualTo(customerContact2);
    }
}
