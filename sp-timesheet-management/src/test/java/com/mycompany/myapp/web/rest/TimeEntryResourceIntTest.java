package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.Module;
import com.mycompany.myapp.domain.Project;
import com.mycompany.myapp.domain.TimeEntry;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.repository.TimeEntryRepository;
import com.mycompany.myapp.repository.search.TimeEntrySearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TimeEntryResource REST controller.
 *
 * @see TimeEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class TimeEntryResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Float DEFAULT_DURATION = 1F;
    private static final Float UPDATED_DURATION = 2F;

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Module DEFAULT_MODULE = new Module();
    private static final Module UPDATED_MODULE = new Module();

    private static final Project DEFAULT_PROJECT = new Project();
    private static final Project UPDATED_PROJECT = new Project();

    private static final User DEFAULT_USER = new User();
    private static final User UPDATED_USER = new User();

    @Autowired
    private TimeEntryRepository timeEntryRepository;

    @Autowired
    private TimeEntrySearchRepository timeEntrySearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restTimeEntryMockMvc;

    private TimeEntry timeEntry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TimeEntryResource timeEntryResource = new TimeEntryResource(timeEntryRepository, timeEntrySearchRepository);
        this.restTimeEntryMockMvc = MockMvcBuilders.standaloneSetup(timeEntryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static TimeEntry createEntity() {
        TimeEntry timeEntry = new TimeEntry()
            .code(DEFAULT_CODE)
            .description(DEFAULT_DESCRIPTION)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .duration(DEFAULT_DURATION)
            .status(DEFAULT_STATUS)
            .module(DEFAULT_MODULE)
            .project(DEFAULT_PROJECT)
            .user(DEFAULT_USER);
        return timeEntry;
    }

    @Before
    public void initTest() {
        timeEntryRepository.deleteAll();
        timeEntrySearchRepository.deleteAll();
        timeEntry = createEntity();
    }

    @Test
    public void createTimeEntry() throws Exception {
        int databaseSizeBeforeCreate = timeEntryRepository.findAll().size();

        // Create the TimeEntry
        restTimeEntryMockMvc.perform(post("/api/time-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(timeEntry)))
            .andExpect(status().isCreated());

        // Validate the TimeEntry in the database
        List<TimeEntry> timeEntryList = timeEntryRepository.findAll();
        assertThat(timeEntryList).hasSize(databaseSizeBeforeCreate + 1);
        TimeEntry testTimeEntry = timeEntryList.get(timeEntryList.size() - 1);
        assertThat(testTimeEntry.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testTimeEntry.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testTimeEntry.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testTimeEntry.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testTimeEntry.getDuration()).isEqualTo(DEFAULT_DURATION);
        assertThat(testTimeEntry.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testTimeEntry.getModule()).isEqualTo(DEFAULT_MODULE);
        assertThat(testTimeEntry.getProject()).isEqualTo(DEFAULT_PROJECT);
        assertThat(testTimeEntry.getUser()).isEqualTo(DEFAULT_USER);

        // Validate the TimeEntry in Elasticsearch
        TimeEntry timeEntryEs = timeEntrySearchRepository.findOne(testTimeEntry.getId());
        assertThat(timeEntryEs).isEqualToIgnoringGivenFields(testTimeEntry);
    }

    @Test
    public void createTimeEntryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = timeEntryRepository.findAll().size();

        // Create the TimeEntry with an existing ID
        timeEntry.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restTimeEntryMockMvc.perform(post("/api/time-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(timeEntry)))
            .andExpect(status().isBadRequest());

        // Validate the TimeEntry in the database
        List<TimeEntry> timeEntryList = timeEntryRepository.findAll();
        assertThat(timeEntryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllTimeEntries() throws Exception {
        // Initialize the database
        timeEntryRepository.save(timeEntry);

        // Get all the timeEntryList
        restTimeEntryMockMvc.perform(get("/api/time-entries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(timeEntry.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].module").value(hasItem(DEFAULT_MODULE.toString())))
            .andExpect(jsonPath("$.[*].project").value(hasItem(DEFAULT_PROJECT.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    public void getTimeEntry() throws Exception {
        // Initialize the database
        timeEntryRepository.save(timeEntry);

        // Get the timeEntry
        restTimeEntryMockMvc.perform(get("/api/time-entries/{id}", timeEntry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(timeEntry.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.duration").value(DEFAULT_DURATION.doubleValue()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.module").value(DEFAULT_MODULE.toString()))
            .andExpect(jsonPath("$.project").value(DEFAULT_PROJECT.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    public void getNonExistingTimeEntry() throws Exception {
        // Get the timeEntry
        restTimeEntryMockMvc.perform(get("/api/time-entries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateTimeEntry() throws Exception {
        // Initialize the database
        timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(timeEntry);
        int databaseSizeBeforeUpdate = timeEntryRepository.findAll().size();

        // Update the timeEntry
        TimeEntry updatedTimeEntry = timeEntryRepository.findOne(timeEntry.getId());
        updatedTimeEntry
            .code(UPDATED_CODE)
            .description(UPDATED_DESCRIPTION)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .duration(UPDATED_DURATION)
            .status(UPDATED_STATUS)
            .module(UPDATED_MODULE)
            .project(UPDATED_PROJECT)
            .user(UPDATED_USER);

        restTimeEntryMockMvc.perform(put("/api/time-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTimeEntry)))
            .andExpect(status().isOk());

        // Validate the TimeEntry in the database
        List<TimeEntry> timeEntryList = timeEntryRepository.findAll();
        assertThat(timeEntryList).hasSize(databaseSizeBeforeUpdate);
        TimeEntry testTimeEntry = timeEntryList.get(timeEntryList.size() - 1);
        assertThat(testTimeEntry.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testTimeEntry.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testTimeEntry.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testTimeEntry.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testTimeEntry.getDuration()).isEqualTo(UPDATED_DURATION);
        assertThat(testTimeEntry.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testTimeEntry.getModule()).isEqualTo(UPDATED_MODULE);
        assertThat(testTimeEntry.getProject()).isEqualTo(UPDATED_PROJECT);
        assertThat(testTimeEntry.getUser()).isEqualTo(UPDATED_USER);

        // Validate the TimeEntry in Elasticsearch
        TimeEntry timeEntryEs = timeEntrySearchRepository.findOne(testTimeEntry.getId());
        assertThat(timeEntryEs).isEqualToIgnoringGivenFields(testTimeEntry);
    }

    @Test
    public void updateNonExistingTimeEntry() throws Exception {
        int databaseSizeBeforeUpdate = timeEntryRepository.findAll().size();

        // Create the TimeEntry

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTimeEntryMockMvc.perform(put("/api/time-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(timeEntry)))
            .andExpect(status().isCreated());

        // Validate the TimeEntry in the database
        List<TimeEntry> timeEntryList = timeEntryRepository.findAll();
        assertThat(timeEntryList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteTimeEntry() throws Exception {
        // Initialize the database
        timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(timeEntry);
        int databaseSizeBeforeDelete = timeEntryRepository.findAll().size();

        // Get the timeEntry
        restTimeEntryMockMvc.perform(delete("/api/time-entries/{id}", timeEntry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean timeEntryExistsInEs = timeEntrySearchRepository.exists(timeEntry.getId());
        assertThat(timeEntryExistsInEs).isFalse();

        // Validate the database is empty
        List<TimeEntry> timeEntryList = timeEntryRepository.findAll();
        assertThat(timeEntryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchTimeEntry() throws Exception {
        // Initialize the database
        timeEntryRepository.save(timeEntry);
        timeEntrySearchRepository.save(timeEntry);

        // Search the timeEntry
        restTimeEntryMockMvc.perform(get("/api/_search/time-entries?query=id:" + timeEntry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(timeEntry.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].duration").value(hasItem(DEFAULT_DURATION.doubleValue())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].module").value(hasItem(DEFAULT_MODULE.toString())))
            .andExpect(jsonPath("$.[*].project").value(hasItem(DEFAULT_PROJECT.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(TimeEntry.class);
        TimeEntry timeEntry1 = new TimeEntry();
        timeEntry1.setId("id1");
        TimeEntry timeEntry2 = new TimeEntry();
        timeEntry2.setId(timeEntry1.getId());
        assertThat(timeEntry1).isEqualTo(timeEntry2);
        timeEntry2.setId("id2");
        assertThat(timeEntry1).isNotEqualTo(timeEntry2);
        timeEntry1.setId(null);
        assertThat(timeEntry1).isNotEqualTo(timeEntry2);
    }
}
