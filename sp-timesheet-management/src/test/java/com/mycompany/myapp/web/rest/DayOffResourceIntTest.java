package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.DayOff;
import com.mycompany.myapp.repository.DayOffRepository;
import com.mycompany.myapp.repository.search.DayOffSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DayOffResource REST controller.
 *
 * @see DayOffResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class DayOffResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private DayOffRepository dayOffRepository;

    @Autowired
    private DayOffSearchRepository dayOffSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restDayOffMockMvc;

    private DayOff dayOff;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DayOffResource dayOffResource = new DayOffResource(dayOffRepository, dayOffSearchRepository);
        this.restDayOffMockMvc = MockMvcBuilders.standaloneSetup(dayOffResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayOff createEntity() {
        DayOff dayOff = new DayOff()
            .code(DEFAULT_CODE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .date(DEFAULT_DATE)
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        return dayOff;
    }

    @Before
    public void initTest() {
        dayOffRepository.deleteAll();
        dayOffSearchRepository.deleteAll();
        dayOff = createEntity();
    }

    @Test
    public void createDayOff() throws Exception {
        int databaseSizeBeforeCreate = dayOffRepository.findAll().size();

        // Create the DayOff
        restDayOffMockMvc.perform(post("/api/day-offs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayOff)))
            .andExpect(status().isCreated());

        // Validate the DayOff in the database
        List<DayOff> dayOffList = dayOffRepository.findAll();
        assertThat(dayOffList).hasSize(databaseSizeBeforeCreate + 1);
        DayOff testDayOff = dayOffList.get(dayOffList.size() - 1);
        assertThat(testDayOff.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testDayOff.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testDayOff.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testDayOff.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testDayOff.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDayOff.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);

        // Validate the DayOff in Elasticsearch
        DayOff dayOffEs = dayOffSearchRepository.findOne(testDayOff.getId());
        assertThat(dayOffEs).isEqualToIgnoringGivenFields(testDayOff);
    }

    @Test
    public void createDayOffWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dayOffRepository.findAll().size();

        // Create the DayOff with an existing ID
        dayOff.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayOffMockMvc.perform(post("/api/day-offs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayOff)))
            .andExpect(status().isBadRequest());

        // Validate the DayOff in the database
        List<DayOff> dayOffList = dayOffRepository.findAll();
        assertThat(dayOffList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllDayOffs() throws Exception {
        // Initialize the database
        dayOffRepository.save(dayOff);

        // Get all the dayOffList
        restDayOffMockMvc.perform(get("/api/day-offs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayOff.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void getDayOff() throws Exception {
        // Initialize the database
        dayOffRepository.save(dayOff);

        // Get the dayOff
        restDayOffMockMvc.perform(get("/api/day-offs/{id}", dayOff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dayOff.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }

    @Test
    public void getNonExistingDayOff() throws Exception {
        // Get the dayOff
        restDayOffMockMvc.perform(get("/api/day-offs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateDayOff() throws Exception {
        // Initialize the database
        dayOffRepository.save(dayOff);
        dayOffSearchRepository.save(dayOff);
        int databaseSizeBeforeUpdate = dayOffRepository.findAll().size();

        // Update the dayOff
        DayOff updatedDayOff = dayOffRepository.findOne(dayOff.getId());
        updatedDayOff
            .code(UPDATED_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .date(UPDATED_DATE)
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restDayOffMockMvc.perform(put("/api/day-offs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDayOff)))
            .andExpect(status().isOk());

        // Validate the DayOff in the database
        List<DayOff> dayOffList = dayOffRepository.findAll();
        assertThat(dayOffList).hasSize(databaseSizeBeforeUpdate);
        DayOff testDayOff = dayOffList.get(dayOffList.size() - 1);
        assertThat(testDayOff.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testDayOff.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testDayOff.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testDayOff.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testDayOff.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDayOff.getDescription()).isEqualTo(UPDATED_DESCRIPTION);

        // Validate the DayOff in Elasticsearch
        DayOff dayOffEs = dayOffSearchRepository.findOne(testDayOff.getId());
        assertThat(dayOffEs).isEqualToIgnoringGivenFields(testDayOff);
    }

    @Test
    public void updateNonExistingDayOff() throws Exception {
        int databaseSizeBeforeUpdate = dayOffRepository.findAll().size();

        // Create the DayOff

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDayOffMockMvc.perform(put("/api/day-offs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayOff)))
            .andExpect(status().isCreated());

        // Validate the DayOff in the database
        List<DayOff> dayOffList = dayOffRepository.findAll();
        assertThat(dayOffList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteDayOff() throws Exception {
        // Initialize the database
        dayOffRepository.save(dayOff);
        dayOffSearchRepository.save(dayOff);
        int databaseSizeBeforeDelete = dayOffRepository.findAll().size();

        // Get the dayOff
        restDayOffMockMvc.perform(delete("/api/day-offs/{id}", dayOff.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean dayOffExistsInEs = dayOffSearchRepository.exists(dayOff.getId());
        assertThat(dayOffExistsInEs).isFalse();

        // Validate the database is empty
        List<DayOff> dayOffList = dayOffRepository.findAll();
        assertThat(dayOffList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchDayOff() throws Exception {
        // Initialize the database
        dayOffRepository.save(dayOff);
        dayOffSearchRepository.save(dayOff);

        // Search the dayOff
        restDayOffMockMvc.perform(get("/api/_search/day-offs?query=id:" + dayOff.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayOff.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayOff.class);
        DayOff dayOff1 = new DayOff();
        dayOff1.setId("id1");
        DayOff dayOff2 = new DayOff();
        dayOff2.setId(dayOff1.getId());
        assertThat(dayOff1).isEqualTo(dayOff2);
        dayOff2.setId("id2");
        assertThat(dayOff1).isNotEqualTo(dayOff2);
        dayOff1.setId(null);
        assertThat(dayOff1).isNotEqualTo(dayOff2);
    }
}
