package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.SickLeaveRequest;
import com.mycompany.myapp.repository.SickLeaveRequestRepository;
import com.mycompany.myapp.repository.search.SickLeaveRequestSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the SickLeaveRequestResource REST controller.
 *
 * @see SickLeaveRequestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class SickLeaveRequestResourceIntTest {

    private static final String DEFAULT_MEDIA = "AAAAAAAAAA";
    private static final String UPDATED_MEDIA = "BBBBBBBBBB";

    @Autowired
    private SickLeaveRequestRepository sickLeaveRequestRepository;

    @Autowired
    private SickLeaveRequestSearchRepository sickLeaveRequestSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restSickLeaveRequestMockMvc;

    private SickLeaveRequest sickLeaveRequest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final SickLeaveRequestResource sickLeaveRequestResource = new SickLeaveRequestResource(sickLeaveRequestRepository, sickLeaveRequestSearchRepository);
        this.restSickLeaveRequestMockMvc = MockMvcBuilders.standaloneSetup(sickLeaveRequestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SickLeaveRequest createEntity() {
        SickLeaveRequest sickLeaveRequest = new SickLeaveRequest()
            .media(DEFAULT_MEDIA);
        return sickLeaveRequest;
    }

    @Before
    public void initTest() {
        sickLeaveRequestRepository.deleteAll();
        sickLeaveRequestSearchRepository.deleteAll();
        sickLeaveRequest = createEntity();
    }

    @Test
    public void createSickLeaveRequest() throws Exception {
        int databaseSizeBeforeCreate = sickLeaveRequestRepository.findAll().size();

        // Create the SickLeaveRequest
        restSickLeaveRequestMockMvc.perform(post("/api/sick-leave-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sickLeaveRequest)))
            .andExpect(status().isCreated());

        // Validate the SickLeaveRequest in the database
        List<SickLeaveRequest> sickLeaveRequestList = sickLeaveRequestRepository.findAll();
        assertThat(sickLeaveRequestList).hasSize(databaseSizeBeforeCreate + 1);
        SickLeaveRequest testSickLeaveRequest = sickLeaveRequestList.get(sickLeaveRequestList.size() - 1);
        assertThat(testSickLeaveRequest.getMedia()).isEqualTo(DEFAULT_MEDIA);

        // Validate the SickLeaveRequest in Elasticsearch
        SickLeaveRequest sickLeaveRequestEs = sickLeaveRequestSearchRepository.findOne(testSickLeaveRequest.getId());
        assertThat(sickLeaveRequestEs).isEqualToIgnoringGivenFields(testSickLeaveRequest);
    }

    @Test
    public void createSickLeaveRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = sickLeaveRequestRepository.findAll().size();

        // Create the SickLeaveRequest with an existing ID
        sickLeaveRequest.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restSickLeaveRequestMockMvc.perform(post("/api/sick-leave-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sickLeaveRequest)))
            .andExpect(status().isBadRequest());

        // Validate the SickLeaveRequest in the database
        List<SickLeaveRequest> sickLeaveRequestList = sickLeaveRequestRepository.findAll();
        assertThat(sickLeaveRequestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllSickLeaveRequests() throws Exception {
        // Initialize the database
        sickLeaveRequestRepository.save(sickLeaveRequest);

        // Get all the sickLeaveRequestList
        restSickLeaveRequestMockMvc.perform(get("/api/sick-leave-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sickLeaveRequest.getId())))
            .andExpect(jsonPath("$.[*].media").value(hasItem(DEFAULT_MEDIA.toString())));
    }

    @Test
    public void getSickLeaveRequest() throws Exception {
        // Initialize the database
        sickLeaveRequestRepository.save(sickLeaveRequest);

        // Get the sickLeaveRequest
        restSickLeaveRequestMockMvc.perform(get("/api/sick-leave-requests/{id}", sickLeaveRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(sickLeaveRequest.getId()))
            .andExpect(jsonPath("$.media").value(DEFAULT_MEDIA.toString()));
    }

    @Test
    public void getNonExistingSickLeaveRequest() throws Exception {
        // Get the sickLeaveRequest
        restSickLeaveRequestMockMvc.perform(get("/api/sick-leave-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSickLeaveRequest() throws Exception {
        // Initialize the database
        sickLeaveRequestRepository.save(sickLeaveRequest);
        sickLeaveRequestSearchRepository.save(sickLeaveRequest);
        int databaseSizeBeforeUpdate = sickLeaveRequestRepository.findAll().size();

        // Update the sickLeaveRequest
        SickLeaveRequest updatedSickLeaveRequest = sickLeaveRequestRepository.findOne(sickLeaveRequest.getId());
        updatedSickLeaveRequest
            .media(UPDATED_MEDIA);

        restSickLeaveRequestMockMvc.perform(put("/api/sick-leave-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedSickLeaveRequest)))
            .andExpect(status().isOk());

        // Validate the SickLeaveRequest in the database
        List<SickLeaveRequest> sickLeaveRequestList = sickLeaveRequestRepository.findAll();
        assertThat(sickLeaveRequestList).hasSize(databaseSizeBeforeUpdate);
        SickLeaveRequest testSickLeaveRequest = sickLeaveRequestList.get(sickLeaveRequestList.size() - 1);
        assertThat(testSickLeaveRequest.getMedia()).isEqualTo(UPDATED_MEDIA);

        // Validate the SickLeaveRequest in Elasticsearch
        SickLeaveRequest sickLeaveRequestEs = sickLeaveRequestSearchRepository.findOne(testSickLeaveRequest.getId());
        assertThat(sickLeaveRequestEs).isEqualToIgnoringGivenFields(testSickLeaveRequest);
    }

    @Test
    public void updateNonExistingSickLeaveRequest() throws Exception {
        int databaseSizeBeforeUpdate = sickLeaveRequestRepository.findAll().size();

        // Create the SickLeaveRequest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restSickLeaveRequestMockMvc.perform(put("/api/sick-leave-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(sickLeaveRequest)))
            .andExpect(status().isCreated());

        // Validate the SickLeaveRequest in the database
        List<SickLeaveRequest> sickLeaveRequestList = sickLeaveRequestRepository.findAll();
        assertThat(sickLeaveRequestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteSickLeaveRequest() throws Exception {
        // Initialize the database
        sickLeaveRequestRepository.save(sickLeaveRequest);
        sickLeaveRequestSearchRepository.save(sickLeaveRequest);
        int databaseSizeBeforeDelete = sickLeaveRequestRepository.findAll().size();

        // Get the sickLeaveRequest
        restSickLeaveRequestMockMvc.perform(delete("/api/sick-leave-requests/{id}", sickLeaveRequest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean sickLeaveRequestExistsInEs = sickLeaveRequestSearchRepository.exists(sickLeaveRequest.getId());
        assertThat(sickLeaveRequestExistsInEs).isFalse();

        // Validate the database is empty
        List<SickLeaveRequest> sickLeaveRequestList = sickLeaveRequestRepository.findAll();
        assertThat(sickLeaveRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchSickLeaveRequest() throws Exception {
        // Initialize the database
        sickLeaveRequestRepository.save(sickLeaveRequest);
        sickLeaveRequestSearchRepository.save(sickLeaveRequest);

        // Search the sickLeaveRequest
        restSickLeaveRequestMockMvc.perform(get("/api/_search/sick-leave-requests?query=id:" + sickLeaveRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(sickLeaveRequest.getId())))
            .andExpect(jsonPath("$.[*].media").value(hasItem(DEFAULT_MEDIA.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SickLeaveRequest.class);
        SickLeaveRequest sickLeaveRequest1 = new SickLeaveRequest();
        sickLeaveRequest1.setId("id1");
        SickLeaveRequest sickLeaveRequest2 = new SickLeaveRequest();
        sickLeaveRequest2.setId(sickLeaveRequest1.getId());
        assertThat(sickLeaveRequest1).isEqualTo(sickLeaveRequest2);
        sickLeaveRequest2.setId("id2");
        assertThat(sickLeaveRequest1).isNotEqualTo(sickLeaveRequest2);
        sickLeaveRequest1.setId(null);
        assertThat(sickLeaveRequest1).isNotEqualTo(sickLeaveRequest2);
    }
}
