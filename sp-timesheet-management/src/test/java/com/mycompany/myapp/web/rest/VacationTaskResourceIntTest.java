package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.VacationTask;
import com.mycompany.myapp.repository.VacationTaskRepository;
import com.mycompany.myapp.repository.search.VacationTaskSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VacationTaskResource REST controller.
 *
 * @see VacationTaskResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class VacationTaskResourceIntTest {

    private static final String DEFAULT_TASK_ID = "AAAAAAAAAA";
    private static final String UPDATED_TASK_ID = "BBBBBBBBBB";

    private static final String DEFAULT_VACATION_REQUEST_ID = "AAAAAAAAAA";
    private static final String UPDATED_VACATION_REQUEST_ID = "BBBBBBBBBB";

    @Autowired
    private VacationTaskRepository vacationTaskRepository;

    @Autowired
    private VacationTaskSearchRepository vacationTaskSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restVacationTaskMockMvc;

    private VacationTask vacationTask;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VacationTaskResource vacationTaskResource = new VacationTaskResource(vacationTaskRepository, vacationTaskSearchRepository);
        this.restVacationTaskMockMvc = MockMvcBuilders.standaloneSetup(vacationTaskResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VacationTask createEntity() {
        VacationTask vacationTask = new VacationTask()
            .taskId(DEFAULT_TASK_ID)
            .vacationRequestId(DEFAULT_VACATION_REQUEST_ID);
        return vacationTask;
    }

    @Before
    public void initTest() {
        vacationTaskRepository.deleteAll();
        vacationTaskSearchRepository.deleteAll();
        vacationTask = createEntity();
    }

    @Test
    public void createVacationTask() throws Exception {
        int databaseSizeBeforeCreate = vacationTaskRepository.findAll().size();

        // Create the VacationTask
        restVacationTaskMockMvc.perform(post("/api/vacation-tasks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationTask)))
            .andExpect(status().isCreated());

        // Validate the VacationTask in the database
        List<VacationTask> vacationTaskList = vacationTaskRepository.findAll();
        assertThat(vacationTaskList).hasSize(databaseSizeBeforeCreate + 1);
        VacationTask testVacationTask = vacationTaskList.get(vacationTaskList.size() - 1);
        assertThat(testVacationTask.getTaskId()).isEqualTo(DEFAULT_TASK_ID);
        assertThat(testVacationTask.getVacationRequestId()).isEqualTo(DEFAULT_VACATION_REQUEST_ID);

        // Validate the VacationTask in Elasticsearch
        VacationTask vacationTaskEs = vacationTaskSearchRepository.findOne(testVacationTask.getId());
        assertThat(vacationTaskEs).isEqualToIgnoringGivenFields(testVacationTask);
    }

    @Test
    public void createVacationTaskWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacationTaskRepository.findAll().size();

        // Create the VacationTask with an existing ID
        vacationTask.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacationTaskMockMvc.perform(post("/api/vacation-tasks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationTask)))
            .andExpect(status().isBadRequest());

        // Validate the VacationTask in the database
        List<VacationTask> vacationTaskList = vacationTaskRepository.findAll();
        assertThat(vacationTaskList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllVacationTasks() throws Exception {
        // Initialize the database
        vacationTaskRepository.save(vacationTask);

        // Get all the vacationTaskList
        restVacationTaskMockMvc.perform(get("/api/vacation-tasks?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationTask.getId())))
            .andExpect(jsonPath("$.[*].taskId").value(hasItem(DEFAULT_TASK_ID.toString())))
            .andExpect(jsonPath("$.[*].vacationRequestId").value(hasItem(DEFAULT_VACATION_REQUEST_ID.toString())));
    }

    @Test
    public void getVacationTask() throws Exception {
        // Initialize the database
        vacationTaskRepository.save(vacationTask);

        // Get the vacationTask
        restVacationTaskMockMvc.perform(get("/api/vacation-tasks/{id}", vacationTask.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacationTask.getId()))
            .andExpect(jsonPath("$.taskId").value(DEFAULT_TASK_ID.toString()))
            .andExpect(jsonPath("$.vacationRequestId").value(DEFAULT_VACATION_REQUEST_ID.toString()));
    }

    @Test
    public void getNonExistingVacationTask() throws Exception {
        // Get the vacationTask
        restVacationTaskMockMvc.perform(get("/api/vacation-tasks/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVacationTask() throws Exception {
        // Initialize the database
        vacationTaskRepository.save(vacationTask);
        vacationTaskSearchRepository.save(vacationTask);
        int databaseSizeBeforeUpdate = vacationTaskRepository.findAll().size();

        // Update the vacationTask
        VacationTask updatedVacationTask = vacationTaskRepository.findOne(vacationTask.getId());
        updatedVacationTask
            .taskId(UPDATED_TASK_ID)
            .vacationRequestId(UPDATED_VACATION_REQUEST_ID);

        restVacationTaskMockMvc.perform(put("/api/vacation-tasks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVacationTask)))
            .andExpect(status().isOk());

        // Validate the VacationTask in the database
        List<VacationTask> vacationTaskList = vacationTaskRepository.findAll();
        assertThat(vacationTaskList).hasSize(databaseSizeBeforeUpdate);
        VacationTask testVacationTask = vacationTaskList.get(vacationTaskList.size() - 1);
        assertThat(testVacationTask.getTaskId()).isEqualTo(UPDATED_TASK_ID);
        assertThat(testVacationTask.getVacationRequestId()).isEqualTo(UPDATED_VACATION_REQUEST_ID);

        // Validate the VacationTask in Elasticsearch
        VacationTask vacationTaskEs = vacationTaskSearchRepository.findOne(testVacationTask.getId());
        assertThat(vacationTaskEs).isEqualToIgnoringGivenFields(testVacationTask);
    }

    @Test
    public void updateNonExistingVacationTask() throws Exception {
        int databaseSizeBeforeUpdate = vacationTaskRepository.findAll().size();

        // Create the VacationTask

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVacationTaskMockMvc.perform(put("/api/vacation-tasks")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationTask)))
            .andExpect(status().isCreated());

        // Validate the VacationTask in the database
        List<VacationTask> vacationTaskList = vacationTaskRepository.findAll();
        assertThat(vacationTaskList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteVacationTask() throws Exception {
        // Initialize the database
        vacationTaskRepository.save(vacationTask);
        vacationTaskSearchRepository.save(vacationTask);
        int databaseSizeBeforeDelete = vacationTaskRepository.findAll().size();

        // Get the vacationTask
        restVacationTaskMockMvc.perform(delete("/api/vacation-tasks/{id}", vacationTask.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean vacationTaskExistsInEs = vacationTaskSearchRepository.exists(vacationTask.getId());
        assertThat(vacationTaskExistsInEs).isFalse();

        // Validate the database is empty
        List<VacationTask> vacationTaskList = vacationTaskRepository.findAll();
        assertThat(vacationTaskList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchVacationTask() throws Exception {
        // Initialize the database
        vacationTaskRepository.save(vacationTask);
        vacationTaskSearchRepository.save(vacationTask);

        // Search the vacationTask
        restVacationTaskMockMvc.perform(get("/api/_search/vacation-tasks?query=id:" + vacationTask.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationTask.getId())))
            .andExpect(jsonPath("$.[*].taskId").value(hasItem(DEFAULT_TASK_ID.toString())))
            .andExpect(jsonPath("$.[*].vacationRequestId").value(hasItem(DEFAULT_VACATION_REQUEST_ID.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VacationTask.class);
        VacationTask vacationTask1 = new VacationTask();
        vacationTask1.setId("id1");
        VacationTask vacationTask2 = new VacationTask();
        vacationTask2.setId(vacationTask1.getId());
        assertThat(vacationTask1).isEqualTo(vacationTask2);
        vacationTask2.setId("id2");
        assertThat(vacationTask1).isNotEqualTo(vacationTask2);
        vacationTask1.setId(null);
        assertThat(vacationTask1).isNotEqualTo(vacationTask2);
    }
}
