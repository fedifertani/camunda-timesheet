package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.VacationRequest;
import com.mycompany.myapp.domain.VacationType;
import com.mycompany.myapp.repository.VacationRequestRepository;
import com.mycompany.myapp.repository.search.VacationRequestSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VacationRequestResource REST controller.
 *
 * @see VacationRequestResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class VacationRequestResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_START_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_START_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_END_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_END_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_REASON = "AAAAAAAAAA";
    private static final String UPDATED_REASON = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final VacationType DEFAULT_TYPE = new VacationType();
    private static final VacationType UPDATED_TYPE = new VacationType();

    private static final User DEFAULT_USER = new User();
    private static final User UPDATED_USER = new User();

    @Autowired
    private VacationRequestRepository vacationRequestRepository;

    @Autowired
    private VacationRequestSearchRepository vacationRequestSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restVacationRequestMockMvc;

    private VacationRequest vacationRequest;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VacationRequestResource vacationRequestResource = new VacationRequestResource(vacationRequestRepository, vacationRequestSearchRepository);
        this.restVacationRequestMockMvc = MockMvcBuilders.standaloneSetup(vacationRequestResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VacationRequest createEntity() {
        VacationRequest vacationRequest = new VacationRequest()
            .code(DEFAULT_CODE)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .reason(DEFAULT_REASON)
            .status(DEFAULT_STATUS)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .type(DEFAULT_TYPE)
            .user(DEFAULT_USER);
        return vacationRequest;
    }

    @Before
    public void initTest() {
        vacationRequestRepository.deleteAll();
        vacationRequestSearchRepository.deleteAll();
        vacationRequest = createEntity();
    }

    @Test
    public void createVacationRequest() throws Exception {
        int databaseSizeBeforeCreate = vacationRequestRepository.findAll().size();

        // Create the VacationRequest
        restVacationRequestMockMvc.perform(post("/api/vacation-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationRequest)))
            .andExpect(status().isCreated());

        // Validate the VacationRequest in the database
        List<VacationRequest> vacationRequestList = vacationRequestRepository.findAll();
        assertThat(vacationRequestList).hasSize(databaseSizeBeforeCreate + 1);
        VacationRequest testVacationRequest = vacationRequestList.get(vacationRequestList.size() - 1);
        assertThat(testVacationRequest.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVacationRequest.getStartDate()).isEqualTo(DEFAULT_START_DATE);
        assertThat(testVacationRequest.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testVacationRequest.getReason()).isEqualTo(DEFAULT_REASON);
        assertThat(testVacationRequest.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testVacationRequest.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testVacationRequest.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testVacationRequest.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testVacationRequest.getUser()).isEqualTo(DEFAULT_USER);

        // Validate the VacationRequest in Elasticsearch
        VacationRequest vacationRequestEs = vacationRequestSearchRepository.findOne(testVacationRequest.getId());
        assertThat(vacationRequestEs).isEqualToIgnoringGivenFields(testVacationRequest);
    }

    @Test
    public void createVacationRequestWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacationRequestRepository.findAll().size();

        // Create the VacationRequest with an existing ID
        vacationRequest.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacationRequestMockMvc.perform(post("/api/vacation-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationRequest)))
            .andExpect(status().isBadRequest());

        // Validate the VacationRequest in the database
        List<VacationRequest> vacationRequestList = vacationRequestRepository.findAll();
        assertThat(vacationRequestList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllVacationRequests() throws Exception {
        // Initialize the database
        vacationRequestRepository.save(vacationRequest);

        // Get all the vacationRequestList
        restVacationRequestMockMvc.perform(get("/api/vacation-requests?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationRequest.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    public void getVacationRequest() throws Exception {
        // Initialize the database
        vacationRequestRepository.save(vacationRequest);

        // Get the vacationRequest
        restVacationRequestMockMvc.perform(get("/api/vacation-requests/{id}", vacationRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacationRequest.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.reason").value(DEFAULT_REASON.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()));
    }

    @Test
    public void getNonExistingVacationRequest() throws Exception {
        // Get the vacationRequest
        restVacationRequestMockMvc.perform(get("/api/vacation-requests/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVacationRequest() throws Exception {
        // Initialize the database
        vacationRequestRepository.save(vacationRequest);
        vacationRequestSearchRepository.save(vacationRequest);
        int databaseSizeBeforeUpdate = vacationRequestRepository.findAll().size();

        // Update the vacationRequest
        VacationRequest updatedVacationRequest = vacationRequestRepository.findOne(vacationRequest.getId());
        updatedVacationRequest
            .code(UPDATED_CODE)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .reason(UPDATED_REASON)
            .status(UPDATED_STATUS)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .type(UPDATED_TYPE)
            .user(UPDATED_USER);

        restVacationRequestMockMvc.perform(put("/api/vacation-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVacationRequest)))
            .andExpect(status().isOk());

        // Validate the VacationRequest in the database
        List<VacationRequest> vacationRequestList = vacationRequestRepository.findAll();
        assertThat(vacationRequestList).hasSize(databaseSizeBeforeUpdate);
        VacationRequest testVacationRequest = vacationRequestList.get(vacationRequestList.size() - 1);
        assertThat(testVacationRequest.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVacationRequest.getStartDate()).isEqualTo(UPDATED_START_DATE);
        assertThat(testVacationRequest.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testVacationRequest.getReason()).isEqualTo(UPDATED_REASON);
        assertThat(testVacationRequest.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testVacationRequest.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testVacationRequest.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testVacationRequest.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testVacationRequest.getUser()).isEqualTo(UPDATED_USER);

        // Validate the VacationRequest in Elasticsearch
        VacationRequest vacationRequestEs = vacationRequestSearchRepository.findOne(testVacationRequest.getId());
        assertThat(vacationRequestEs).isEqualToIgnoringGivenFields(testVacationRequest);
    }

    @Test
    public void updateNonExistingVacationRequest() throws Exception {
        int databaseSizeBeforeUpdate = vacationRequestRepository.findAll().size();

        // Create the VacationRequest

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVacationRequestMockMvc.perform(put("/api/vacation-requests")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationRequest)))
            .andExpect(status().isCreated());

        // Validate the VacationRequest in the database
        List<VacationRequest> vacationRequestList = vacationRequestRepository.findAll();
        assertThat(vacationRequestList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteVacationRequest() throws Exception {
        // Initialize the database
        vacationRequestRepository.save(vacationRequest);
        vacationRequestSearchRepository.save(vacationRequest);
        int databaseSizeBeforeDelete = vacationRequestRepository.findAll().size();

        // Get the vacationRequest
        restVacationRequestMockMvc.perform(delete("/api/vacation-requests/{id}", vacationRequest.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean vacationRequestExistsInEs = vacationRequestSearchRepository.exists(vacationRequest.getId());
        assertThat(vacationRequestExistsInEs).isFalse();

        // Validate the database is empty
        List<VacationRequest> vacationRequestList = vacationRequestRepository.findAll();
        assertThat(vacationRequestList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchVacationRequest() throws Exception {
        // Initialize the database
        vacationRequestRepository.save(vacationRequest);
        vacationRequestSearchRepository.save(vacationRequest);

        // Search the vacationRequest
        restVacationRequestMockMvc.perform(get("/api/_search/vacation-requests?query=id:" + vacationRequest.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationRequest.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].reason").value(hasItem(DEFAULT_REASON.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VacationRequest.class);
        VacationRequest vacationRequest1 = new VacationRequest();
        vacationRequest1.setId("id1");
        VacationRequest vacationRequest2 = new VacationRequest();
        vacationRequest2.setId(vacationRequest1.getId());
        assertThat(vacationRequest1).isEqualTo(vacationRequest2);
        vacationRequest2.setId("id2");
        assertThat(vacationRequest1).isNotEqualTo(vacationRequest2);
        vacationRequest1.setId(null);
        assertThat(vacationRequest1).isNotEqualTo(vacationRequest2);
    }
}
