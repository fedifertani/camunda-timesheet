package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.UserPosition;
import com.mycompany.myapp.repository.UserPositionRepository;
import com.mycompany.myapp.repository.search.UserPositionSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the UserPositionResource REST controller.
 *
 * @see UserPositionResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class UserPositionResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private UserPositionRepository userPositionRepository;

    @Autowired
    private UserPositionSearchRepository userPositionSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restUserPositionMockMvc;

    private UserPosition userPosition;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final UserPositionResource userPositionResource = new UserPositionResource(userPositionRepository, userPositionSearchRepository);
        this.restUserPositionMockMvc = MockMvcBuilders.standaloneSetup(userPositionResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static UserPosition createEntity() {
        UserPosition userPosition = new UserPosition()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return userPosition;
    }

    @Before
    public void initTest() {
        userPositionRepository.deleteAll();
        userPositionSearchRepository.deleteAll();
        userPosition = createEntity();
    }

    @Test
    public void createUserPosition() throws Exception {
        int databaseSizeBeforeCreate = userPositionRepository.findAll().size();

        // Create the UserPosition
        restUserPositionMockMvc.perform(post("/api/user-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPosition)))
            .andExpect(status().isCreated());

        // Validate the UserPosition in the database
        List<UserPosition> userPositionList = userPositionRepository.findAll();
        assertThat(userPositionList).hasSize(databaseSizeBeforeCreate + 1);
        UserPosition testUserPosition = userPositionList.get(userPositionList.size() - 1);
        assertThat(testUserPosition.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testUserPosition.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testUserPosition.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testUserPosition.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the UserPosition in Elasticsearch
        UserPosition userPositionEs = userPositionSearchRepository.findOne(testUserPosition.getId());
        assertThat(userPositionEs).isEqualToIgnoringGivenFields(testUserPosition);
    }

    @Test
    public void createUserPositionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = userPositionRepository.findAll().size();

        // Create the UserPosition with an existing ID
        userPosition.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restUserPositionMockMvc.perform(post("/api/user-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPosition)))
            .andExpect(status().isBadRequest());

        // Validate the UserPosition in the database
        List<UserPosition> userPositionList = userPositionRepository.findAll();
        assertThat(userPositionList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllUserPositions() throws Exception {
        // Initialize the database
        userPositionRepository.save(userPosition);

        // Get all the userPositionList
        restUserPositionMockMvc.perform(get("/api/user-positions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPosition.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void getUserPosition() throws Exception {
        // Initialize the database
        userPositionRepository.save(userPosition);

        // Get the userPosition
        restUserPositionMockMvc.perform(get("/api/user-positions/{id}", userPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(userPosition.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    public void getNonExistingUserPosition() throws Exception {
        // Get the userPosition
        restUserPositionMockMvc.perform(get("/api/user-positions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateUserPosition() throws Exception {
        // Initialize the database
        userPositionRepository.save(userPosition);
        userPositionSearchRepository.save(userPosition);
        int databaseSizeBeforeUpdate = userPositionRepository.findAll().size();

        // Update the userPosition
        UserPosition updatedUserPosition = userPositionRepository.findOne(userPosition.getId());
        updatedUserPosition
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restUserPositionMockMvc.perform(put("/api/user-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedUserPosition)))
            .andExpect(status().isOk());

        // Validate the UserPosition in the database
        List<UserPosition> userPositionList = userPositionRepository.findAll();
        assertThat(userPositionList).hasSize(databaseSizeBeforeUpdate);
        UserPosition testUserPosition = userPositionList.get(userPositionList.size() - 1);
        assertThat(testUserPosition.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testUserPosition.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testUserPosition.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testUserPosition.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the UserPosition in Elasticsearch
        UserPosition userPositionEs = userPositionSearchRepository.findOne(testUserPosition.getId());
        assertThat(userPositionEs).isEqualToIgnoringGivenFields(testUserPosition);
    }

    @Test
    public void updateNonExistingUserPosition() throws Exception {
        int databaseSizeBeforeUpdate = userPositionRepository.findAll().size();

        // Create the UserPosition

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restUserPositionMockMvc.perform(put("/api/user-positions")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(userPosition)))
            .andExpect(status().isCreated());

        // Validate the UserPosition in the database
        List<UserPosition> userPositionList = userPositionRepository.findAll();
        assertThat(userPositionList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteUserPosition() throws Exception {
        // Initialize the database
        userPositionRepository.save(userPosition);
        userPositionSearchRepository.save(userPosition);
        int databaseSizeBeforeDelete = userPositionRepository.findAll().size();

        // Get the userPosition
        restUserPositionMockMvc.perform(delete("/api/user-positions/{id}", userPosition.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean userPositionExistsInEs = userPositionSearchRepository.exists(userPosition.getId());
        assertThat(userPositionExistsInEs).isFalse();

        // Validate the database is empty
        List<UserPosition> userPositionList = userPositionRepository.findAll();
        assertThat(userPositionList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchUserPosition() throws Exception {
        // Initialize the database
        userPositionRepository.save(userPosition);
        userPositionSearchRepository.save(userPosition);

        // Search the userPosition
        restUserPositionMockMvc.perform(get("/api/_search/user-positions?query=id:" + userPosition.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(userPosition.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(UserPosition.class);
        UserPosition userPosition1 = new UserPosition();
        userPosition1.setId("id1");
        UserPosition userPosition2 = new UserPosition();
        userPosition2.setId(userPosition1.getId());
        assertThat(userPosition1).isEqualTo(userPosition2);
        userPosition2.setId("id2");
        assertThat(userPosition1).isNotEqualTo(userPosition2);
        userPosition1.setId(null);
        assertThat(userPosition1).isNotEqualTo(userPosition2);
    }
}
