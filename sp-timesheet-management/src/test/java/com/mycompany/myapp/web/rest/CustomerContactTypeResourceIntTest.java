package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.CustomerContactType;
import com.mycompany.myapp.repository.CustomerContactTypeRepository;
import com.mycompany.myapp.repository.search.CustomerContactTypeSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CustomerContactTypeResource REST controller.
 *
 * @see CustomerContactTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class CustomerContactTypeResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private CustomerContactTypeRepository customerContactTypeRepository;

    @Autowired
    private CustomerContactTypeSearchRepository customerContactTypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restCustomerContactTypeMockMvc;

    private CustomerContactType customerContactType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CustomerContactTypeResource customerContactTypeResource = new CustomerContactTypeResource(customerContactTypeRepository, customerContactTypeSearchRepository);
        this.restCustomerContactTypeMockMvc = MockMvcBuilders.standaloneSetup(customerContactTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CustomerContactType createEntity() {
        CustomerContactType customerContactType = new CustomerContactType()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return customerContactType;
    }

    @Before
    public void initTest() {
        customerContactTypeRepository.deleteAll();
        customerContactTypeSearchRepository.deleteAll();
        customerContactType = createEntity();
    }

    @Test
    public void createCustomerContactType() throws Exception {
        int databaseSizeBeforeCreate = customerContactTypeRepository.findAll().size();

        // Create the CustomerContactType
        restCustomerContactTypeMockMvc.perform(post("/api/customer-contact-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContactType)))
            .andExpect(status().isCreated());

        // Validate the CustomerContactType in the database
        List<CustomerContactType> customerContactTypeList = customerContactTypeRepository.findAll();
        assertThat(customerContactTypeList).hasSize(databaseSizeBeforeCreate + 1);
        CustomerContactType testCustomerContactType = customerContactTypeList.get(customerContactTypeList.size() - 1);
        assertThat(testCustomerContactType.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testCustomerContactType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCustomerContactType.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testCustomerContactType.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the CustomerContactType in Elasticsearch
        CustomerContactType customerContactTypeEs = customerContactTypeSearchRepository.findOne(testCustomerContactType.getId());
        assertThat(customerContactTypeEs).isEqualToIgnoringGivenFields(testCustomerContactType);
    }

    @Test
    public void createCustomerContactTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = customerContactTypeRepository.findAll().size();

        // Create the CustomerContactType with an existing ID
        customerContactType.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restCustomerContactTypeMockMvc.perform(post("/api/customer-contact-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContactType)))
            .andExpect(status().isBadRequest());

        // Validate the CustomerContactType in the database
        List<CustomerContactType> customerContactTypeList = customerContactTypeRepository.findAll();
        assertThat(customerContactTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllCustomerContactTypes() throws Exception {
        // Initialize the database
        customerContactTypeRepository.save(customerContactType);

        // Get all the customerContactTypeList
        restCustomerContactTypeMockMvc.perform(get("/api/customer-contact-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerContactType.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void getCustomerContactType() throws Exception {
        // Initialize the database
        customerContactTypeRepository.save(customerContactType);

        // Get the customerContactType
        restCustomerContactTypeMockMvc.perform(get("/api/customer-contact-types/{id}", customerContactType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(customerContactType.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    public void getNonExistingCustomerContactType() throws Exception {
        // Get the customerContactType
        restCustomerContactTypeMockMvc.perform(get("/api/customer-contact-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCustomerContactType() throws Exception {
        // Initialize the database
        customerContactTypeRepository.save(customerContactType);
        customerContactTypeSearchRepository.save(customerContactType);
        int databaseSizeBeforeUpdate = customerContactTypeRepository.findAll().size();

        // Update the customerContactType
        CustomerContactType updatedCustomerContactType = customerContactTypeRepository.findOne(customerContactType.getId());
        updatedCustomerContactType
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restCustomerContactTypeMockMvc.perform(put("/api/customer-contact-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCustomerContactType)))
            .andExpect(status().isOk());

        // Validate the CustomerContactType in the database
        List<CustomerContactType> customerContactTypeList = customerContactTypeRepository.findAll();
        assertThat(customerContactTypeList).hasSize(databaseSizeBeforeUpdate);
        CustomerContactType testCustomerContactType = customerContactTypeList.get(customerContactTypeList.size() - 1);
        assertThat(testCustomerContactType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testCustomerContactType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCustomerContactType.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testCustomerContactType.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the CustomerContactType in Elasticsearch
        CustomerContactType customerContactTypeEs = customerContactTypeSearchRepository.findOne(testCustomerContactType.getId());
        assertThat(customerContactTypeEs).isEqualToIgnoringGivenFields(testCustomerContactType);
    }

    @Test
    public void updateNonExistingCustomerContactType() throws Exception {
        int databaseSizeBeforeUpdate = customerContactTypeRepository.findAll().size();

        // Create the CustomerContactType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCustomerContactTypeMockMvc.perform(put("/api/customer-contact-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(customerContactType)))
            .andExpect(status().isCreated());

        // Validate the CustomerContactType in the database
        List<CustomerContactType> customerContactTypeList = customerContactTypeRepository.findAll();
        assertThat(customerContactTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCustomerContactType() throws Exception {
        // Initialize the database
        customerContactTypeRepository.save(customerContactType);
        customerContactTypeSearchRepository.save(customerContactType);
        int databaseSizeBeforeDelete = customerContactTypeRepository.findAll().size();

        // Get the customerContactType
        restCustomerContactTypeMockMvc.perform(delete("/api/customer-contact-types/{id}", customerContactType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean customerContactTypeExistsInEs = customerContactTypeSearchRepository.exists(customerContactType.getId());
        assertThat(customerContactTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<CustomerContactType> customerContactTypeList = customerContactTypeRepository.findAll();
        assertThat(customerContactTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchCustomerContactType() throws Exception {
        // Initialize the database
        customerContactTypeRepository.save(customerContactType);
        customerContactTypeSearchRepository.save(customerContactType);

        // Search the customerContactType
        restCustomerContactTypeMockMvc.perform(get("/api/_search/customer-contact-types?query=id:" + customerContactType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(customerContactType.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CustomerContactType.class);
        CustomerContactType customerContactType1 = new CustomerContactType();
        customerContactType1.setId("id1");
        CustomerContactType customerContactType2 = new CustomerContactType();
        customerContactType2.setId(customerContactType1.getId());
        assertThat(customerContactType1).isEqualTo(customerContactType2);
        customerContactType2.setId("id2");
        assertThat(customerContactType1).isNotEqualTo(customerContactType2);
        customerContactType1.setId(null);
        assertThat(customerContactType1).isNotEqualTo(customerContactType2);
    }
}
