package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.Project;
import com.mycompany.myapp.domain.ProjectMemberShip;
import com.mycompany.myapp.domain.User;
import com.mycompany.myapp.domain.UserPosition;
import com.mycompany.myapp.repository.ProjectMemberShipRepository;
import com.mycompany.myapp.repository.search.ProjectMemberShipSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ProjectMemberShipResource REST controller.
 *
 * @see ProjectMemberShipResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class ProjectMemberShipResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_RATE = new BigDecimal(1);
    private static final BigDecimal UPDATED_RATE = new BigDecimal(2);

    private static final User DEFAULT_USER = new User();
    private static final User UPDATED_USER = new User();

    private static final UserPosition DEFAULT_USER_POSITION = new UserPosition();
    private static final UserPosition UPDATED_USER_POSITION = new UserPosition();

    private static final Project DEFAULT_PROJECT = new Project();
    private static final Project UPDATED_PROJECT = new Project();

    @Autowired
    private ProjectMemberShipRepository projectMemberShipRepository;

    @Autowired
    private ProjectMemberShipSearchRepository projectMemberShipSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restProjectMemberShipMockMvc;

    private ProjectMemberShip projectMemberShip;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ProjectMemberShipResource projectMemberShipResource = new ProjectMemberShipResource(projectMemberShipRepository, projectMemberShipSearchRepository);
        this.restProjectMemberShipMockMvc = MockMvcBuilders.standaloneSetup(projectMemberShipResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProjectMemberShip createEntity() {
        ProjectMemberShip projectMemberShip = new ProjectMemberShip()
            .code(DEFAULT_CODE)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE)
            .rate(DEFAULT_RATE)
            .user(DEFAULT_USER)
            .userPosition(DEFAULT_USER_POSITION)
            .project(DEFAULT_PROJECT);
        return projectMemberShip;
    }

    @Before
    public void initTest() {
        projectMemberShipRepository.deleteAll();
        projectMemberShipSearchRepository.deleteAll();
        projectMemberShip = createEntity();
    }

    @Test
    public void createProjectMemberShip() throws Exception {
        int databaseSizeBeforeCreate = projectMemberShipRepository.findAll().size();

        // Create the ProjectMemberShip
        restProjectMemberShipMockMvc.perform(post("/api/project-member-ships")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projectMemberShip)))
            .andExpect(status().isCreated());

        // Validate the ProjectMemberShip in the database
        List<ProjectMemberShip> projectMemberShipList = projectMemberShipRepository.findAll();
        assertThat(projectMemberShipList).hasSize(databaseSizeBeforeCreate + 1);
        ProjectMemberShip testProjectMemberShip = projectMemberShipList.get(projectMemberShipList.size() - 1);
        assertThat(testProjectMemberShip.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testProjectMemberShip.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testProjectMemberShip.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);
        assertThat(testProjectMemberShip.getRate()).isEqualTo(DEFAULT_RATE);
        assertThat(testProjectMemberShip.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testProjectMemberShip.getUserPosition()).isEqualTo(DEFAULT_USER_POSITION);
        assertThat(testProjectMemberShip.getProject()).isEqualTo(DEFAULT_PROJECT);

        // Validate the ProjectMemberShip in Elasticsearch
        ProjectMemberShip projectMemberShipEs = projectMemberShipSearchRepository.findOne(testProjectMemberShip.getId());
        assertThat(projectMemberShipEs).isEqualToIgnoringGivenFields(testProjectMemberShip);
    }

    @Test
    public void createProjectMemberShipWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = projectMemberShipRepository.findAll().size();

        // Create the ProjectMemberShip with an existing ID
        projectMemberShip.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restProjectMemberShipMockMvc.perform(post("/api/project-member-ships")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projectMemberShip)))
            .andExpect(status().isBadRequest());

        // Validate the ProjectMemberShip in the database
        List<ProjectMemberShip> projectMemberShipList = projectMemberShipRepository.findAll();
        assertThat(projectMemberShipList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllProjectMemberShips() throws Exception {
        // Initialize the database
        projectMemberShipRepository.save(projectMemberShip);

        // Get all the projectMemberShipList
        restProjectMemberShipMockMvc.perform(get("/api/project-member-ships?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projectMemberShip.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE.intValue())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].userPosition").value(hasItem(DEFAULT_USER_POSITION.toString())))
            .andExpect(jsonPath("$.[*].project").value(hasItem(DEFAULT_PROJECT.toString())));
    }

    @Test
    public void getProjectMemberShip() throws Exception {
        // Initialize the database
        projectMemberShipRepository.save(projectMemberShip);

        // Get the projectMemberShip
        restProjectMemberShipMockMvc.perform(get("/api/project-member-ships/{id}", projectMemberShip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(projectMemberShip.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()))
            .andExpect(jsonPath("$.rate").value(DEFAULT_RATE.intValue()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.userPosition").value(DEFAULT_USER_POSITION.toString()))
            .andExpect(jsonPath("$.project").value(DEFAULT_PROJECT.toString()));
    }

    @Test
    public void getNonExistingProjectMemberShip() throws Exception {
        // Get the projectMemberShip
        restProjectMemberShipMockMvc.perform(get("/api/project-member-ships/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateProjectMemberShip() throws Exception {
        // Initialize the database
        projectMemberShipRepository.save(projectMemberShip);
        projectMemberShipSearchRepository.save(projectMemberShip);
        int databaseSizeBeforeUpdate = projectMemberShipRepository.findAll().size();

        // Update the projectMemberShip
        ProjectMemberShip updatedProjectMemberShip = projectMemberShipRepository.findOne(projectMemberShip.getId());
        updatedProjectMemberShip
            .code(UPDATED_CODE)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE)
            .rate(UPDATED_RATE)
            .user(UPDATED_USER)
            .userPosition(UPDATED_USER_POSITION)
            .project(UPDATED_PROJECT);

        restProjectMemberShipMockMvc.perform(put("/api/project-member-ships")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedProjectMemberShip)))
            .andExpect(status().isOk());

        // Validate the ProjectMemberShip in the database
        List<ProjectMemberShip> projectMemberShipList = projectMemberShipRepository.findAll();
        assertThat(projectMemberShipList).hasSize(databaseSizeBeforeUpdate);
        ProjectMemberShip testProjectMemberShip = projectMemberShipList.get(projectMemberShipList.size() - 1);
        assertThat(testProjectMemberShip.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testProjectMemberShip.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testProjectMemberShip.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);
        assertThat(testProjectMemberShip.getRate()).isEqualTo(UPDATED_RATE);
        assertThat(testProjectMemberShip.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testProjectMemberShip.getUserPosition()).isEqualTo(UPDATED_USER_POSITION);
        assertThat(testProjectMemberShip.getProject()).isEqualTo(UPDATED_PROJECT);

        // Validate the ProjectMemberShip in Elasticsearch
        ProjectMemberShip projectMemberShipEs = projectMemberShipSearchRepository.findOne(testProjectMemberShip.getId());
        assertThat(projectMemberShipEs).isEqualToIgnoringGivenFields(testProjectMemberShip);
    }

    @Test
    public void updateNonExistingProjectMemberShip() throws Exception {
        int databaseSizeBeforeUpdate = projectMemberShipRepository.findAll().size();

        // Create the ProjectMemberShip

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restProjectMemberShipMockMvc.perform(put("/api/project-member-ships")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(projectMemberShip)))
            .andExpect(status().isCreated());

        // Validate the ProjectMemberShip in the database
        List<ProjectMemberShip> projectMemberShipList = projectMemberShipRepository.findAll();
        assertThat(projectMemberShipList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteProjectMemberShip() throws Exception {
        // Initialize the database
        projectMemberShipRepository.save(projectMemberShip);
        projectMemberShipSearchRepository.save(projectMemberShip);
        int databaseSizeBeforeDelete = projectMemberShipRepository.findAll().size();

        // Get the projectMemberShip
        restProjectMemberShipMockMvc.perform(delete("/api/project-member-ships/{id}", projectMemberShip.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean projectMemberShipExistsInEs = projectMemberShipSearchRepository.exists(projectMemberShip.getId());
        assertThat(projectMemberShipExistsInEs).isFalse();

        // Validate the database is empty
        List<ProjectMemberShip> projectMemberShipList = projectMemberShipRepository.findAll();
        assertThat(projectMemberShipList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchProjectMemberShip() throws Exception {
        // Initialize the database
        projectMemberShipRepository.save(projectMemberShip);
        projectMemberShipSearchRepository.save(projectMemberShip);

        // Search the projectMemberShip
        restProjectMemberShipMockMvc.perform(get("/api/_search/project-member-ships?query=id:" + projectMemberShip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(projectMemberShip.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].rate").value(hasItem(DEFAULT_RATE.intValue())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].userPosition").value(hasItem(DEFAULT_USER_POSITION.toString())))
            .andExpect(jsonPath("$.[*].project").value(hasItem(DEFAULT_PROJECT.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ProjectMemberShip.class);
        ProjectMemberShip projectMemberShip1 = new ProjectMemberShip();
        projectMemberShip1.setId("id1");
        ProjectMemberShip projectMemberShip2 = new ProjectMemberShip();
        projectMemberShip2.setId(projectMemberShip1.getId());
        assertThat(projectMemberShip1).isEqualTo(projectMemberShip2);
        projectMemberShip2.setId("id2");
        assertThat(projectMemberShip1).isNotEqualTo(projectMemberShip2);
        projectMemberShip1.setId(null);
        assertThat(projectMemberShip1).isNotEqualTo(projectMemberShip2);
    }
}
