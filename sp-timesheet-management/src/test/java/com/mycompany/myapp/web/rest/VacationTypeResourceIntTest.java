package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.TimesheetApp;

import com.mycompany.myapp.domain.VacationType;
import com.mycompany.myapp.repository.VacationTypeRepository;
import com.mycompany.myapp.repository.search.VacationTypeSearchRepository;
import com.mycompany.myapp.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.mycompany.myapp.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the VacationTypeResource REST controller.
 *
 * @see VacationTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = TimesheetApp.class)
public class VacationTypeResourceIntTest {

    private static final String DEFAULT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CREATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CREATED_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_UPDATED_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPDATED_DATE = LocalDate.now(ZoneId.systemDefault());

    @Autowired
    private VacationTypeRepository vacationTypeRepository;

    @Autowired
    private VacationTypeSearchRepository vacationTypeSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restVacationTypeMockMvc;

    private VacationType vacationType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final VacationTypeResource vacationTypeResource = new VacationTypeResource(vacationTypeRepository, vacationTypeSearchRepository);
        this.restVacationTypeMockMvc = MockMvcBuilders.standaloneSetup(vacationTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static VacationType createEntity() {
        VacationType vacationType = new VacationType()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME)
            .createdDate(DEFAULT_CREATED_DATE)
            .updatedDate(DEFAULT_UPDATED_DATE);
        return vacationType;
    }

    @Before
    public void initTest() {
        vacationTypeRepository.deleteAll();
        vacationTypeSearchRepository.deleteAll();
        vacationType = createEntity();
    }

    @Test
    public void createVacationType() throws Exception {
        int databaseSizeBeforeCreate = vacationTypeRepository.findAll().size();

        // Create the VacationType
        restVacationTypeMockMvc.perform(post("/api/vacation-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationType)))
            .andExpect(status().isCreated());

        // Validate the VacationType in the database
        List<VacationType> vacationTypeList = vacationTypeRepository.findAll();
        assertThat(vacationTypeList).hasSize(databaseSizeBeforeCreate + 1);
        VacationType testVacationType = vacationTypeList.get(vacationTypeList.size() - 1);
        assertThat(testVacationType.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testVacationType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVacationType.getCreatedDate()).isEqualTo(DEFAULT_CREATED_DATE);
        assertThat(testVacationType.getUpdatedDate()).isEqualTo(DEFAULT_UPDATED_DATE);

        // Validate the VacationType in Elasticsearch
        VacationType vacationTypeEs = vacationTypeSearchRepository.findOne(testVacationType.getId());
        assertThat(vacationTypeEs).isEqualToIgnoringGivenFields(testVacationType);
    }

    @Test
    public void createVacationTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = vacationTypeRepository.findAll().size();

        // Create the VacationType with an existing ID
        vacationType.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVacationTypeMockMvc.perform(post("/api/vacation-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationType)))
            .andExpect(status().isBadRequest());

        // Validate the VacationType in the database
        List<VacationType> vacationTypeList = vacationTypeRepository.findAll();
        assertThat(vacationTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllVacationTypes() throws Exception {
        // Initialize the database
        vacationTypeRepository.save(vacationType);

        // Get all the vacationTypeList
        restVacationTypeMockMvc.perform(get("/api/vacation-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationType.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void getVacationType() throws Exception {
        // Initialize the database
        vacationTypeRepository.save(vacationType);

        // Get the vacationType
        restVacationTypeMockMvc.perform(get("/api/vacation-types/{id}", vacationType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(vacationType.getId()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.createdDate").value(DEFAULT_CREATED_DATE.toString()))
            .andExpect(jsonPath("$.updatedDate").value(DEFAULT_UPDATED_DATE.toString()));
    }

    @Test
    public void getNonExistingVacationType() throws Exception {
        // Get the vacationType
        restVacationTypeMockMvc.perform(get("/api/vacation-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVacationType() throws Exception {
        // Initialize the database
        vacationTypeRepository.save(vacationType);
        vacationTypeSearchRepository.save(vacationType);
        int databaseSizeBeforeUpdate = vacationTypeRepository.findAll().size();

        // Update the vacationType
        VacationType updatedVacationType = vacationTypeRepository.findOne(vacationType.getId());
        updatedVacationType
            .code(UPDATED_CODE)
            .name(UPDATED_NAME)
            .createdDate(UPDATED_CREATED_DATE)
            .updatedDate(UPDATED_UPDATED_DATE);

        restVacationTypeMockMvc.perform(put("/api/vacation-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedVacationType)))
            .andExpect(status().isOk());

        // Validate the VacationType in the database
        List<VacationType> vacationTypeList = vacationTypeRepository.findAll();
        assertThat(vacationTypeList).hasSize(databaseSizeBeforeUpdate);
        VacationType testVacationType = vacationTypeList.get(vacationTypeList.size() - 1);
        assertThat(testVacationType.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testVacationType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVacationType.getCreatedDate()).isEqualTo(UPDATED_CREATED_DATE);
        assertThat(testVacationType.getUpdatedDate()).isEqualTo(UPDATED_UPDATED_DATE);

        // Validate the VacationType in Elasticsearch
        VacationType vacationTypeEs = vacationTypeSearchRepository.findOne(testVacationType.getId());
        assertThat(vacationTypeEs).isEqualToIgnoringGivenFields(testVacationType);
    }

    @Test
    public void updateNonExistingVacationType() throws Exception {
        int databaseSizeBeforeUpdate = vacationTypeRepository.findAll().size();

        // Create the VacationType

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restVacationTypeMockMvc.perform(put("/api/vacation-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(vacationType)))
            .andExpect(status().isCreated());

        // Validate the VacationType in the database
        List<VacationType> vacationTypeList = vacationTypeRepository.findAll();
        assertThat(vacationTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteVacationType() throws Exception {
        // Initialize the database
        vacationTypeRepository.save(vacationType);
        vacationTypeSearchRepository.save(vacationType);
        int databaseSizeBeforeDelete = vacationTypeRepository.findAll().size();

        // Get the vacationType
        restVacationTypeMockMvc.perform(delete("/api/vacation-types/{id}", vacationType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean vacationTypeExistsInEs = vacationTypeSearchRepository.exists(vacationType.getId());
        assertThat(vacationTypeExistsInEs).isFalse();

        // Validate the database is empty
        List<VacationType> vacationTypeList = vacationTypeRepository.findAll();
        assertThat(vacationTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void searchVacationType() throws Exception {
        // Initialize the database
        vacationTypeRepository.save(vacationType);
        vacationTypeSearchRepository.save(vacationType);

        // Search the vacationType
        restVacationTypeMockMvc.perform(get("/api/_search/vacation-types?query=id:" + vacationType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(vacationType.getId())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].createdDate").value(hasItem(DEFAULT_CREATED_DATE.toString())))
            .andExpect(jsonPath("$.[*].updatedDate").value(hasItem(DEFAULT_UPDATED_DATE.toString())));
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(VacationType.class);
        VacationType vacationType1 = new VacationType();
        vacationType1.setId("id1");
        VacationType vacationType2 = new VacationType();
        vacationType2.setId(vacationType1.getId());
        assertThat(vacationType1).isEqualTo(vacationType2);
        vacationType2.setId("id2");
        assertThat(vacationType1).isNotEqualTo(vacationType2);
        vacationType1.setId(null);
        assertThat(vacationType1).isNotEqualTo(vacationType2);
    }
}
