package fr.ippon.jhipster.bpm.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.google.common.collect.ImmutableMap;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;
import fr.ippon.jhipster.bpm.config.YAMLConfig;
import fr.ippon.jhipster.bpm.domain.*;
import org.camunda.bpm.engine.FormService;
import org.camunda.bpm.engine.IdentityService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.TaskService;
import org.camunda.bpm.engine.impl.persistence.entity.UserEntity;
import org.camunda.bpm.engine.runtime.SignalEventReceivedBuilder;
import org.camunda.bpm.engine.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;


@RestController
@RequestMapping("/api/bpmn")
public class BPMResource {

    private static final String VACATION_REQUEST_ID = "VacationRequestId";
    private static final String USER_ID = "UserId";
    private static final String WEEK_NUMBER = "WeekNumber";
    private static final String YEAR = "Year";
    private static final String USER_RH = "UserRH";
    private static final String ASSIGNEE="assignee";
    private static final String APPROVED="approved";

    private final RuntimeService runtimeService;
    private final IdentityService identityService;
    private final TaskService taskService;
    private final FormService formService;
    private static final Logger LOGGER = Logger.getLogger( BPMResource.class.getName() );
    private YAMLConfig myConfig;


    public BPMResource(RuntimeService runtimeService,IdentityService identityService,TaskService taskService,FormService formService) {
        this.runtimeService = runtimeService;
        this.formService=formService;
        this.taskService=taskService;
        this.identityService=identityService;
    }


   @PostMapping("/holidaySignal")
    @Timed
    public ResponseEntity<Void> sendHolidaySignal(@RequestBody HolidaySignal signal) {
        SignalEventReceivedBuilder signalEvent = runtimeService.createSignalEvent(signal.getSignal());
        signalEvent.setVariables(ImmutableMap.of(USER_RH, signal.getUserRH()));
        signalEvent.setVariables(ImmutableMap.of(VACATION_REQUEST_ID, signal.getVacationRequestId()));
        signalEvent.send();
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/timesheetSignal")
    @Timed
    public ResponseEntity<Void> sendSignal(@RequestBody TimesheetSignal signal) {
        SignalEventReceivedBuilder signalEvent = runtimeService.createSignalEvent(signal.getSignal());
        signalEvent.setVariables(ImmutableMap.of(USER_RH, signal.getUserRH()));
        signalEvent.setVariables(ImmutableMap.of(WEEK_NUMBER, signal.getWeekNumer()));
        signalEvent.setVariables(ImmutableMap.of(YEAR, signal.getYear()));
        signalEvent.setVariables(ImmutableMap.of(USER_ID, signal.getUserId()));

        signalEvent.send();
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/user")
    @Timed
    public ResponseEntity<Void> create(@RequestBody UserEntity user) {
        identityService.saveUser(user);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/holidayTasks/{userId}")
    @Timed
    public List<HolidayTask> getHolidayTasks(@PathVariable("userId") String userId) {
        List<Task> entities=taskService.createTaskQuery().taskAssignee(userId).list();
        List<HolidayTask> holidayTasks=new ArrayList<HolidayTask>();
        for(int i=0;i<entities.size();i++)
        {
            if(entities.get(i).getProcessDefinitionId().contains("Process_Holiday"))
            {
                String taskId=entities.get(i).getId();
                String vacationRequestId= (String) taskService.getVariable(taskId,VACATION_REQUEST_ID);
                HolidayTask holidayTask=new HolidayTask(taskId,vacationRequestId);
                holidayTasks.add(holidayTask);
            }

        }
        return holidayTasks;
        }

    @GetMapping("/timesheetTasks/{userId}")
    @Timed
    public List<TimesheetTask> getTimesheetTasks(@PathVariable("userId") String userId) {
        List<Task> entities=taskService.createTaskQuery().taskAssignee(userId).list();
        List<TimesheetTask> timesheetTasks=new ArrayList<TimesheetTask>();
        for(int i=0;i<entities.size();i++)
        {
            if(entities.get(i).getProcessDefinitionId().contains("Process_timesheet"))
            {
                String taskId=entities.get(i).getId();
                String user= (String) taskService.getVariable(taskId,USER_ID);
                String weekNumer= (String) taskService.getVariable(taskId,WEEK_NUMBER);
                String year= (String) taskService.getVariable(taskId,YEAR);

                TimesheetTask timesheetTask=new TimesheetTask(taskId,user,weekNumer,year);
                timesheetTasks.add(timesheetTask);

            }

        }
        return timesheetTasks;
    }

    @PostMapping("/submitform/{taskId}/{isapproved}")
    public ResponseEntity<String> submitForm(@RequestBody FormTask formTask,@PathVariable("isapproved") String isapproved,@PathVariable("taskId") String taskId) {

        OkHttpClient client = new OkHttpClient();

        LOGGER.info("FROM BPMResource : is approved ---->"+formTask.getAprroved());
        MediaType mediaType = MediaType.parse("application/json");
        com.squareup.okhttp.RequestBody body = com.squareup.okhttp.RequestBody.create(mediaType,
            "{\"variables\":   {\"assignee\" : " +
                "{\"value\" : \""+formTask.getAssignee()+"\", \"type\": \"String\"}, " +
                "    \"isapproved\" : {\"value\" : "+isapproved+", \"type\": \"String\"} " +
                "   }, \"businessKey\" : \"myBusinessKey\"}");
        Request request = new Request.Builder()
            .url("http://localhost:8085/rest/engine/default/task/"+taskId+"/submit-form")
            .post(body)
            .addHeader("Content-Type", "application/json")

            .build();

        try {
            Response response = client.newCall(request).execute();
            return ResponseEntity.ok().body(response.body().string());

        }
        catch (IOException ex)
        {
            return ResponseEntity.badRequest().build();

        }
    }
}
