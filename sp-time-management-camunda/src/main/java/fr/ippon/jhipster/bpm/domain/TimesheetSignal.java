package fr.ippon.jhipster.bpm.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

import java.io.Serializable;

public class TimesheetSignal implements Serializable{

    private String signal;

    private String userId;
    private String weekNumer;
    private String year;
    private String userRH;

    public TimesheetSignal(String signal, String userId, String weekNumer, String year) {
        this.signal = signal;
        this.userId = userId;
        this.weekNumer = weekNumer;
        this.year = year;
    }



    /**
     * @return the userRH
     */
    public String getUserRH() {
        return userRH;
    }

    /**
     * @param userRH the userRH to set
     */
    public void setUserRH(String userRH) {
        this.userRH = userRH;
    }


    /**
     * @return the signal
     */
    public String getSignal() {
        return signal;
    }

    /**
     * @param signal the signal to set
     */
    public void setSignal(String signal) {
        this.signal = signal;
    }




    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeekNumer() {
        return weekNumer;
    }

    public void setWeekNumer(String weekNumer) {
        this.weekNumer = weekNumer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    }
