package fr.ippon.jhipster.bpm.tasks;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.Gson;
import com.squareup.okhttp.*;
import fr.ippon.jhipster.bpm.config.YAMLConfig;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Logger;

public class AuthentificationManager {


    OkHttpClient client;
    MediaType mediaType;
    RequestBody body;
    Request request;
    Response response;
    Gson gson;
    String URL;
    @Autowired
    private YAMLConfig myConfig;


    public String authentificate() throws IOException {
        JWTToken token;

        URL = "http://localhost:8080";

        client = new OkHttpClient();

        mediaType = MediaType.parse("application/json");
        body = RequestBody.create(mediaType, "{\"username\":\"admin\",\n\"password\":\"admin\"}");
        request = new Request.Builder()
            .url(URL + "/api/authenticate")
            .post(body)
            .addHeader("Content-Type", "application/json")
            .build();

        response = client.newCall(request).execute();

        gson = new Gson();

        token = gson.fromJson(response.body().string(), JWTToken.class);
        return token.getIdToken();

    }

    static class JWTToken implements Serializable {

        private String id_token;

        JWTToken(String idToken) {
            this.id_token = idToken;
        }

        @JsonProperty("id_token")
        String getIdToken() {
            return id_token;
        }

        void setIdToken(String idToken) {
            this.id_token = idToken;
        }
    }
}
