package fr.ippon.jhipster.bpm.domain;

public class HolidayTask {

    private String taskId;
    private String vacationRequestId;

    public HolidayTask(String taskId, String vacationRequestId) {
        this.taskId = taskId;
        this.vacationRequestId = vacationRequestId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getVacationRequestId() {
        return vacationRequestId;
    }

    public void setVacationRequestId(String vacationRequestId) {
        this.vacationRequestId = vacationRequestId;
    }
}
