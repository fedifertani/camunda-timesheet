package fr.ippon.jhipster.bpm.tasks;

import java.io.IOException;
import java.util.logging.Logger;

import com.squareup.okhttp.*;
import fr.ippon.jhipster.bpm.config.YAMLConfig;
import org.camunda.bpm.engine.delegate.DelegateExecution;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequestStatusUpdater {


    OkHttpClient client;
    MediaType mediaType;
    RequestBody body;
    Request request;
    Response response;
    String status;
    String token;
    @Autowired
    private YAMLConfig myConfig;
    String URL;
    private static final Logger LOGGER = Logger.getLogger(RequestStatusUpdater.class.getName());

    public void updateVacationRequestStatus(DelegateExecution execution) {

        try {
            setStatus(execution.getVariable("isapproved").toString());
            LOGGER.info("token : " + token);
            sendStatusToUpdateAPI(execution);

        } catch (IOException ex) {
            LOGGER.warning(ex.getMessage());
        }


    }

    private void setStatus(String status) {
        if (status.equals("true")) {
            this.status = "approved";
        }
        if (status.equals("false")) {
            this.status = "rejected";
        }
    }


    private void sendStatusToUpdateAPI(DelegateExecution execution) throws IOException {
        LOGGER.info("code : " + execution.getVariable("VacationRequestId"));
        LOGGER.info("status : " + status);

        URL = "http://" + myConfig.getPlatformServerHost() + ":" + myConfig.getPlatformServerPort();

        token = new AuthentificationManager().authentificate();
        LOGGER.info("token : " + token);

        client = new OkHttpClient();
        body = RequestBody.create(mediaType, "{}");
        body = RequestBody.create(mediaType, "{\"status\":\""+status+"\"}");
        mediaType = MediaType.parse("application/json");

        request = new Request.Builder()
            .url(URL + "/api/vacation-request/"+execution.getVariable("VacationRequestId")+"/status")
            .put(body)
            .addHeader("Content-Type", "application/json")
            .addHeader("Authorization", "Bearer " + token)
            .build();

        response = client.newCall(request).execute();
        LOGGER.info("get by code  : " + response.body().string());

    }


}
