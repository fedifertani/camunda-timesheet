package fr.ippon.jhipster.bpm.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

import java.io.Serializable;

@JsonIgnoreType
public class Signal implements Serializable{

    private String signal;
    @JsonIgnore
    private String taskId;
    @JsonIgnore
    private String userId;
    @JsonIgnore
    private String weekNumer;
    @JsonIgnore
    private String year;
    @JsonIgnore
    private String userRH;
    @JsonIgnore
    private String vacationRequestId;

    public Signal(String signal, String taskId, String userId, String weekNumer, String year,String vacationRequestId) {
        this.signal = signal;
        this.vacationRequestId=vacationRequestId;
        this.taskId = taskId;
        this.userId = userId;
        this.weekNumer = weekNumer;
        this.year = year;
    }


    /**
     * @return the vacationRequestId
     */
    public String getVacationRequestId() {
        return vacationRequestId;
    }


    /**
     * @param vacationRequestId the vacationRequestId to set
     */
    public void setVacationRequestId(String vacationRequestId) {
        this.vacationRequestId = vacationRequestId;
    }

    /**
     * @return the userRH
     */
    public String getUserRH() {
        return userRH;
    }

    /**
     * @param userRH the userRH to set
     */
    public void setUserRH(String userRH) {
        this.userRH = userRH;
    }


    /**
     * @return the signal
     */
    public String getSignal() {
        return signal;
    }

    /**
     * @param signal the signal to set
     */
    public void setSignal(String signal) {
        this.signal = signal;
    }


    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeekNumer() {
        return weekNumer;
    }

    public void setWeekNumer(String weekNumer) {
        this.weekNumer = weekNumer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    }
