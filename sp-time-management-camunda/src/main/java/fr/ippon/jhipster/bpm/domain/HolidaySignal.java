package fr.ippon.jhipster.bpm.domain;
import java.io.Serializable;

public class HolidaySignal implements Serializable{

    private String signal;
    private String userRH;
    private String vacationRequestId;

    /**
     * @return the signal
     */
    public String getSignal() {
        return signal;
    }

    /**
     * @param signal the signal to set
     */
    public void setSignal(String signal) {
        this.signal = signal;
    }

    /**
     * @return the userRH
     */
    public String getUserRH() {
        return userRH;
    }

    /**
     * @param userRH the userRH to set
     */
    public void setUserRH(String userRH) {
        this.userRH = userRH;
    }

      /**
     * @return the vacationRequestId
     */
    public String getVacationRequestId() {
        return vacationRequestId;
    }


    /**
     * @param vacationRequestId the vacationRequestId to set
     */
    public void setVacationRequestId(String vacationRequestId) {
        this.vacationRequestId = vacationRequestId;
    }

    }
