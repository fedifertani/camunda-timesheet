package fr.ippon.jhipster.bpm.domain;

public class TimesheetTask {

    private String taskId;
    private String userId;
    private String weekNumer;
    private String year;

    public TimesheetTask(String taskId, String userId, String weekNumer, String year) {
        this.taskId = taskId;
        this.userId = userId;
        this.weekNumer = weekNumer;
        this.year = year;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getWeekNumer() {
        return weekNumer;
    }

    public void setWeekNumer(String weekNumer) {
        this.weekNumer = weekNumer;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
