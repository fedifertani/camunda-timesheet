package fr.ippon.jhipster.bpm.domain;
import java.io.Serializable;

public class VacationUpdate implements Serializable{

    private String code;
    private String status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
