package fr.ippon.jhipster.bpm.domain;
import java.io.Serializable;

public class FormTask implements Serializable{

    private String assignee;
    private String isapproved;


    public String getAssignee() {
        return assignee;
    }

    public void setAssignee(String assignee) {
        this.assignee = assignee;
    }

    public String getAprroved() {
        return isapproved;
    }

    public void setAprroved(String aprroved) {
        this.isapproved = aprroved;
    }
}
