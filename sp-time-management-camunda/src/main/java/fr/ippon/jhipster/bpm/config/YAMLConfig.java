package fr.ippon.jhipster.bpm.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties
public class YAMLConfig {

    private String platformServerHost;
    private String platformServerPort;


    public String getPlatformServerHost() {
        return platformServerHost;
    }

    public void setPlatformServerHost(String platformServerHost) {
        this.platformServerHost = platformServerHost;
    }

    public String getPlatformServerPort() {
        return platformServerPort;
    }

    public void setPlatformServerPort(String platformServerPort) {
        this.platformServerPort = platformServerPort;
    }
}
